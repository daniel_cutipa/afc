@extends('layouts.app')
@section('htmlheader_title')
Division/Create
@endsection
@section('content')

<h3>
    <a href="/division">Divisiones</a>
    Nueva Division
</h3>
<form method="POST" action="/division">
    {{ csrf_field() }}
    <div class="form-group {{ $errors->has('division') ? 'has-error' : '' }}">
        <label for="division" >Nombre Division</label>
        <input id="division" class="form-control" type="text" name="division" required autofocus>
        @if ($errors->has('division'))
            <span class="help-block">
                <strong>{{ $errors->first('division') }}</strong>
            </span>
        @endif
    </div>
    <button class="btn btn-primary" type="submit">
        Guardar<i class="fa fa-send"></i>
    </button>
</form>
@endsection