@extends('layouts.app')
@section('htmlheader_title')
Division
@endsection
@section('content')
<a href="/division/create" class="btn btn-primary" role="button">Nueva Division</a>
<hr>
<table class="table table-striped table-sm">
    <thead>
        <tr>
            <th>Nombre Division</th>
          	<th>acciones</th>
        </tr>
    </thead>
    <tbody>
    	@foreach($divisiones as $division)
        <tr>
            <td>{{ $division->division }}</td>
            <td>
            	<form action="/division/{{ $division->id_division }}" method="POST">
            		<input type="hidden" name="_method" value="DELETE">
            		<input type="hidden" name="_token" value="{{ csrf_token() }}">
                    
            		<button type="submit" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="eliminar">
						<i class="fa fa-trash fa-lg"></i>
					</button>

					<a href="{{ url('/division/'.$division->id_division.'/edit') }}" class="btn btn-primary btn-sm" role="button" data-toggle="tooltip" data-placement="right" title="editar">
                    	<i class="fa fa-pencil fa-lg"></i>
                	</a>
            	</form>
            </td>
        </tr>
		@endforeach
    </tbody>
</table>
@endsection