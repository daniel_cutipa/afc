@extends('layouts.app')
@section('htmlheader_title')
Division/Edit
@endsection
@section('content')
<h3>
    <a href="/division">Divisiones</a>
    Modificar Division
</h3>
<form method="POST" action="/division/{{ $division->id_division }}">
    <input name="_method" type="hidden" value="PUT">
    {{ csrf_field() }}
	<div class="form-group {{ $errors->has('division') ? 'has-error' : '' }}">
        <label for="division" >Nombre Division</label>
        <input id="division" class="form-control" type="text" name="division" value="{{ $division->division}}" required autofocus>
        @if ($errors->has('division'))
	        <span class="help-block">
	            <strong>{{ $errors->first('division') }}</strong>
	        </span>
	    @endif
    </div>
    <button class="btn btn-primary" type="submit">
    	Guardar<i class="fa fa-send"></i>
    </button>
</form>

@endsection