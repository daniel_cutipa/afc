@extends('layouts.app')
@section('htmlheader_title')
Cancha/Edit
@endsection
@section('content')
<h3>
    <a href="/cancha">Canchas</a>
    Modificar Cancha
</h3>
<form method="POST" action="/cancha/{{ $cancha->id_cancha }}">
    <input name="_method" type="hidden" value="PUT">
    {{ csrf_field() }}
	<div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
        <label for="Nombre" >Nombre </label>
        <input id="Nombre" class="form-control" type="text" name="nombre" value="{{ $cancha->nombre }}" required autofocus>
        @if ($errors->has('nombre'))
            <span class="help-block">
                <strong>{{ $errors->first('nombre') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('ubicacion') ? 'has-error' : '' }}">
        <label for="ubicacion" >ubicacion </label>
        <input id="ubicacion" class="form-control" type="text" name="ubicacion" value="{{ $cancha->ubicacion }}">
        @if ($errors->has('ubicacion'))
            <span class="help-block">
                <strong>{{ $errors->first('ubicacion') }}</strong>
            </span>
        @endif
    </div>

	<div class="form-group {{ $errors->has('tipo_cancha') ? 'has-error' : '' }}">
        <label for="tipo_cancha" >tipo cancha </label>
        <input id="tipo_cancha" class="form-control" type="text" name="tipo_cancha" value="{{ $cancha->tipo_cancha }}">
        @if ($errors->has('tipo_cancha'))
            <span class="help-block">
                <strong>{{ $errors->first('tipo_cancha') }}</strong>
            </span>
        @endif
    </div>
    
    <button class="btn btn-primary" type="submit">
        Guardar<i class="fa fa-send"></i>
    </button>
</form>

@endsection