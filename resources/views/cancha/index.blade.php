@extends('layouts.app')
@section('htmlheader_title')
Cancha
@endsection
@section('content')
<a href="/cancha/create" class="btn btn-primary" role="button">Nueva Cancha</a>
<hr>
<table class="table table-striped table-sm">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Ubicacion</th>
            <th>Tipo de Cancha</th>
            
        </tr>
    </thead>
    <tbody>
    	@foreach($canchas as $cancha)
        <tr>
            <td>{{ $cancha->nombre }}</td>
            <td>{{ $cancha->ubicacion }}</td>
            <td>{{ $cancha->tipo_cancha }}</td>
            
            <td>
            	<form action="/cancha/{{ $cancha->id_cancha }}" method="POST">
            		<input type="hidden" name="_method" value="DELETE">
            		<input type="hidden" name="_token" value="{{ csrf_token() }}">
                    
            		<button type="submit" class="btn btn-danger btn-sm">
						<i class="fa fa-trash fa-lg"></i>
					</button>

					<a href="{{ url('/cancha/'.$cancha->id_cancha.'/edit') }}" class="btn btn-primary btn-sm">
                    	<i class="fa fa-pencil fa-lg"></i>
                	</a>
            	</form>
            </td>
        </tr>
		@endforeach
    </tbody>
</table>
@endsection