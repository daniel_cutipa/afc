@extends('layouts.app')
@section('htmlheader_title')
Cancha/Create
@endsection
@section('content')

<h3>
    <a href="/cancha">Canchas</a>
    Nueva Cancha
</h3>
<form method="POST" action="/cancha">
    {{ csrf_field() }}
    <div class="form-group {{ $errors->has('cancha') ? 'has-error' : '' }}">
        <label for="nombre" >Nombre</label>
        <input id="nombre" class="form-control" type="text" name="nombre" required autofocus>
        @if ($errors->has('nombre'))
            <span class="help-block">
                <strong>{{ $errors->first('nombre') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('cancha') ? 'has-error' : '' }}">
        <label for="ubicacion" >ubicacion</label>
        <input id="ubicacion" class="form-control" type="text" name="ubicacion">
        @if ($errors->has('ubicacion'))
            <span class="help-block">
                <strong>{{ $errors->first('ubicacion') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('cancha') ? 'has-error' : '' }}">
        <label for="tipo_cancha" >tipo_cancha</label>
        <input id="tipo_cancha" class="form-control" type="text" name="tipo_cancha">
        @if ($errors->has('tipo_cancha'))
            <span class="help-block">
                <strong>{{ $errors->first('tipo_cancha') }}</strong>
            </span>
        @endif
    </div>
    
    <button class="btn btn-primary" type="submit">
        Guardar<i class="fa fa-send"></i>
    </button>
</form>
@endsection