@extends('layouts.app')
@section('htmlheader_title')
Samcion/Create
@endsection
@section('content')
{{ csrf_field() }}
<div class="row">
    <div class="col-lg-12">
        <h1>Lista de Temporadas</h1>
        <table class="table table-striped table-sm" >
            <thead>
                <tr>
                    <th>Categoria</th>
                    <th>Gestion</th>
                    <th>Torneo</th>
                    <th>Fecha Inicio</th>
                    <th>Fecha Fin</th>
                    <th>acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($temporadas as $temporada)
                <tr data-id="{{ $temporada->id_temporada }}">
                    <td>{{ $temporada->categoria->categoria }}</td>
                    <td>{{ $temporada->gestion }}</td>
                    <td>{{ $temporada->torneo }}</td>
                    <td>{{ $temporada->fecha_inicio }}</td>
                    <td>{{ $temporada->fecha_fin }}</td>
                    <td>
                        <a href="#" class="btn btn-success btn-sm list_jugadores">
                            <i class="fa fa-list-ul fa-lg"> Jugadores</i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>


<div class="row">
    <div class="col-lg-6">
        <h1>Lista de Jugadores</h1>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
                <input type="text" class="form-control" id="txt_search">
                <div class="input-group-append">
                    <button type="button" class="btn btn-primary" id="buscar">Buscar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <table class="table table-striped table-sm" >
            <thead>
                <tr>
                    <th>Matricula</th>
                    <th>Jugador</th>
                    <th>Club</th>
                    <th>Tarjetas</th>
                    <th>Estado</th>
                    <th>acciones</th>
                </tr>
            </thead>
            <tbody id="table_jugadores">

            </tbody>
        </table>

    </div>
</div>
<!-- modal agregar sancion -->
<div id="modal_sancion" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Agregar Sancion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="descripcion">Descripcion:</label>
                            <textarea class="form-control" id="descripcion"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="fecha_inicio">Fecha Inicio</label>
                            <input type="date" class="form-control" id="fecha_inicio">
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="fecha_fin">Fecha Fin</label>
                            <input type="date" class="form-control" id="fecha_fin">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="sancion_economica">Sancion Economica</label>
                            <input type="number" class="form-control" id="sancion_economica">
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="numero_partidos">Numero de Partidos</label>
                            <input type="number" class="form-control" id="numero_partidos">
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="id_jugador_sancion">
            <div class="modal-footer">
                <button type="button" class="btn btn-success add_modal_sancion" data-dismiss="modal">
                    <i class="fa fa-check fa-lg"></i> Guardar
                </button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">
                    <i class="fa fa-remove fa-lg"></i> Cerrar
                </button>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<script>
    var id_temporada = null;
    $( ".list_jugadores" ).click(function() {
        // var res = str.substr(1, 4);
        id_temporada = $($(this).parents("tr")).data('id');
        poner_en_tabla('getJugadores/' + id_temporada);
    });
</script>
<script>
    $(document).on('click', '.modal_sancion_add', function(e) {
        e.preventDefault();
        $('#id_jugador_sancion').val($($(this).parents("tr")).data('id')),
        $('#descripcion').val('');
        $('#fecha_inicio').val('');
        $('#fecha_fin').val('');
        $('#sancion_economica').val('');
        $('#numero_partidos').val('');
        $('#modal_sancion').modal('show');
    });
</script>
<script>
    $('.modal-footer').on('click', '.add_modal_sancion', function() {
        // verificar que uno por lo menos este con campo lleno
        $.ajax({
            type: 'POST',
            url: '/sancion',
            data: {
                '_token': $('input[name=_token]').val(),
                'id_jugador': $('#id_jugador_sancion').val(),
                'descripcion': $('#descripcion').val(),
                'fecha_inicio': $('#fecha_inicio').val(),
                'fecha_fin': $('#fecha_fin').val(),
                'sancion_economica': $('#sancion_economica').val(),
                'numero_partidos': $('#numero_partidos').val(),
            },
            success : function(data) {
                toastr.success(data.message, 'Success Alert', {timeOut: 5000});
            },
            error : function(xhr, status) {
                toastr.error('Disculpe, existio un problema!', 'Error Alert', {timeOut: 5000});
            },
        });
                            
    });
</script>
<script>
    $(document).on('click', '#buscar', function(e) {
        e.preventDefault();
        if ($('#txt_search').val() != '') {
            poner_en_tabla('searchJugadores/'+id_temporada+'/'+$('#txt_search').val());
        }
        else{
            poner_en_tabla('getJugadores/' + id_temporada);
        }
    });
</script>
<script>
    function poner_en_tabla(url){
        $.get(url, function(data){
            $('#table_jugadores').empty();
            $.each(data.jugadores, function(index, jugador){
                $('#table_jugadores').append("\
                    <tr data-id='"+jugador.id_jugador+"'>\
                        <td>"+jugador.matricula+"</td>\
                        <td>"+jugador.nombre+"</td>\
                        <td>"+jugador.club+"</td>\
                        <td id='tarjeta'></td>\
                        <td>"+(jugador.en_espera==1?"<div class='badge badge-warning rounded'>suspendido</div>":'')+"</td>\
                        <td>\
                            <a href='#' class='btn btn-success btn-sm modal_sancion_add'>\
                                <i class='fa fa-plus fa-lg'> Sanción</i>\
                            </a>\
                        </td>\
                    </tr>"
                );
                for (var i = 0; i < jugador.amarilla; i++) {
                    $('#tarjeta').append("<div class='badge badge-warning'>&nbsp;</div>&nbsp;");
                }
                for (var i = 0; i < jugador.roja; i++) {
                    $('#tarjeta').append("<div class='badge badge-danger'>&nbsp;</div>&nbsp;");
                }
                $('#tarjeta').removeAttr('id');
            });
        });
    }
</script>
@endsection
