@extends('layouts.app')
@section('htmlheader_title')
Sancion/Edit
@endsection
@section('content')
<h3>
    <a href="/suspension">Suspension</a>
    Modificar suspension
</h3>
<form method="POST" action="/suspension/{{ $suspension->id_suspension }}">
    <input name="_method" type="hidden" value="PUT">
    {{ csrf_field() }}

    <div class="form-group {{ $errors->has('id_observacion') ? 'has-error' : '' }}">
        <label for="id_observacion" >id observacion</label>
        <input id="id_observacion" class="form-control" type="text" name="id_observacion" value="{{ $suspension->id_observacion }}" required autofocus>
        @if ($errors->has('id_observacion'))
            <span class="help-block">
                <strong>{{ $errors->first('id_observacion') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('descripcion') ? 'has-error' : '' }}">
        <label for="descripcion" >Descripcion</label>
        <input id="descripcion" class="form-control" type="text" name="descripcion" value="{{ $suspension->descripcion }}" required autofocus>
        @if ($errors->has('descripcion'))
            <span class="help-block">
                <strong>{{ $errors->first('descripcion') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('tiempo') ? 'has-error' : '' }}">
        <label for="tiempo" >Tiempo</label>
        <input id="tiempo" class="form-control" type="text" name="tiempo" value="{{ $suspension->tiempo }}" required autofocus>
        @if ($errors->has('tiempo'))
            <span class="help-block">
                <strong>{{ $errors->first('tiempo') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('fecha_inicio') ? 'has-error' : '' }}">
        <label for="fecha_inicio" >Fecha Inicio</label>
        <input id="fecha_inicio" class="form-control" type="date" name="fecha_inicio" value="{{ $suspension->fecha_inicio }}" required autofocus>
        @if ($errors->has('fecha_inicio'))
            <span class="help-block">
                <strong>{{ $errors->first('fecha_inicio') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('fecha_fin') ? 'has-error' : '' }}">
        <label for="fecha_fin" >Fecha Fin</label>
        <input id="fecha_fin" class="form-control" type="date" name="fecha_fin" value="{{ $suspension->fecha_fin }}" required autofocus>
        @if ($errors->has('fecha_fin'))
            <span class="help-block">
                <strong>{{ $errors->first('fecha_fin') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('sancion_economica') ? 'has-error' : '' }}">
        <label for="sancion_economica" >Fecha Fin</label>
        <input id="sancion_economica" class="form-control" type="text" name="sancion_economica" value="{{ $suspension->sancion_economica }}" required autofocus>
        @if ($errors->has('sancion_economica'))
            <span class="help-block">
                <strong>{{ $errors->first('sancion_economica') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('numero_partidos') ? 'has-error' : '' }}">
        <label for="numero_partidos" >Numero Partidos</label>
        <input id="numero_partidos" class="form-control" type="text" name="numero_partidos" value="{{ $suspension->numero_partidos }}" required autofocus>
        @if ($errors->has('numero_partidos'))
            <span class="help-block">
                <strong>{{ $errors->first('numero_partidos') }}</strong>
            </span>
        @endif
    </div>
    <button class="btn btn-primary" type="submit">
        Guardar<i class="fa fa-send"></i>
    </button>
</form>

@endsection
