@extends('layouts.app')
@section('htmlheader_title')
Sancion
@endsection
@section('content')
<a href="/sancion/create" class="btn btn-primary" role="button">Nueva Sancion</a>
<hr>
<table class="table table-striped table-sm">
    <thead>
        <tr>
            <th>Jugador</th>
            <th>Descripcion</th>
            <th>Fecha Inicio</th>
            <th>Fecha Fin</th>
            <th>Sancion Economica</th>
            <th>Numero Partidos</th>
            <th>acciones</th>
        </tr>
    </thead>
    <tbody>
    	@foreach($sanciones as $sancion)
        <tr data-id="{{ $sancion->id_sancion }}">
            <td>{{ $sancion->jugador->ap_paterno . ' ' . $sancion->jugador->ap_materno . ' ' . $sancion->jugador->nombre}}</td>
            <td>{{ $sancion->descripcion }}</td>
            <td>{{ $sancion->fecha_inicio }}</td>
            <td>{{ $sancion->fecha_fin }}</td>
            <td>{{ $sancion->sancion_economica }}</td>
            <td>{{ $sancion->numero_partidos }}</td>
            <td>
            	<form action="/sancion/{{ $sancion->id_sancion }}" method="POST" onSubmit="if(!confirm('¿Estas seguro de eliminar?')){return false;}">
            		<input type="hidden" name="_method" value="DELETE">
            		<input type="hidden" name="_token" value="{{ csrf_token() }}">

            		<button type="submit" class="btn btn-danger btn-sm">
						<i class="fa fa-trash fa-lg"></i>
					</button>

					<a href="{{ url('/sancion/'.$sancion->id_sancion.'/edit') }}" class="btn btn-primary btn-sm modal_sancion_edit">
                    	<i class="fa fa-edit fa-lg"></i>
                	</a>
            	</form>
            </td>
        </tr>
		@endforeach
    </tbody>
</table>

<!-- modal editar sancion -->
<div id="modal_sancion" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Editar Sancion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="descripcion">Descripcion:</label>
                            <textarea class="form-control" id="descripcion"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="fecha_inicio">Fecha Inicio</label>
                            <input type="date" class="form-control" id="fecha_inicio">
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="fecha_fin">Fecha Fin</label>
                            <input type="date" class="form-control" id="fecha_fin">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="sancion_economica">Sancion Economica</label>
                            <input type="number" class="form-control" id="sancion_economica">
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="numero_partidos">Numero de Partidos</label>
                            <input type="number" class="form-control" id="numero_partidos">
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="id_sancion">
            <div class="modal-footer">
                <button type="button" class="btn btn-success edit_modal_sancion" data-dismiss="modal">
                    <i class="fa fa-check fa-lg"></i> Guardar
                </button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">
                    <i class="fa fa-remove fa-lg"></i> Cerrar
                </button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    $(document).on('click', '.modal_sancion_edit', function(e) {
        e.preventDefault();
        $.get('sancion/'+$($(this).parents("tr")).data('id')+'/edit', function(data){
            $('#id_sancion').val(data.sancion.id_sancion),
            $('#descripcion').val(data.sancion.descripcion);
            $('#fecha_inicio').val(data.sancion.fecha_inicio);
            $('#fecha_fin').val(data.sancion.fecha_fin);
            $('#sancion_economica').val(data.sancion.sancion_economica);
            $('#numero_partidos').val(data.sancion.numero_partidos);
            $('#modal_sancion').modal('show');
        });
    });
</script>
<script>
    $('.modal-footer').on('click', '.edit_modal_sancion', function() {
        // verificar que uno por lo menos este con campo lleno
        $.ajax({
            type: 'PUT',
            url: '/sancion/'+$('#id_sancion').val(),
            data: {
                '_token': $('input[name=_token]').val(),
                'descripcion': $('#descripcion').val(),
                'fecha_inicio': $('#fecha_inicio').val(),
                'fecha_fin': $('#fecha_fin').val(),
                'sancion_economica': $('#sancion_economica').val(),
                'numero_partidos': $('#numero_partidos').val(),
            },
            success : function(data) {
                toastr.success(data.message, 'Success Alert', {timeOut: 5000});
                location.reload(true);
            },
            error : function(xhr, status) {
                toastr.error('Disculpe, existio un problema!', 'Error Alert', {timeOut: 5000});
            },
        });
    });
</script>
@endsection
