@extends('layouts.app')
@section('htmlheader_title')
Usuarios
@endsection
@section('content')
<a href="/user/create" class="btn btn-primary" role="button">Nuevo Usuario</a>
<hr>
<table class="table table-striped table-sm">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Email</th>
            <th>Rol</th>
            <th>acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($users as $user)
        <tr>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->rol==1?'Administrador':($user->rol==2?'Planillero':'Secretaria') }}</td>
            <td>
                <form action="/user/{{ $user->id }}" method="POST">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="eliminar">
                    <i class="fa fa-trash fa-lg"></i>
                    </button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection