@extends('layouts.app')
@section('htmlheader_title')
Temporada/Fixture
@endsection
@section('content')
{{ csrf_field() }}
<div class="row">
    <div class="col-lg">
        <label for=""><b>Gestion:</b> {{ $temporada->gestion }}</label>
    </div>
    <div class="col-lg">
        <label for=""><b>Torneo:</b> {{ $temporada->torneo }}</label>
    </div>
    <div class="col-lg">
        <label for=""><b>Fecha de inicio:</b> {{ $temporada->fecha_inicio }}</label>
    </div>
    <div class="col-lg">
        <label for=""><b>Fecha fin:</b> {{ $temporada->fecha_fin }}</label>
    </div>
    <div class="col-lg noPrint">
        <button type="button" class="btn btn-light" onclick="window.print();">
            <i class="fa fa-print fa-lg"></i>imprimir
        </button>
    </div>
</div>

<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" id="nav-pri-fase-tab" data-toggle="tab" href="#nav-pri-fase" role="tab" aria-controls="nav-pri-fase" aria-selected="true">Primera Fase</a>
        <a class="nav-item nav-link" id="nav-seg-fase-tab" data-toggle="tab" href="#nav-seg-fase" role="tab" aria-controls="nav-seg-fase" aria-selected="false">
            {{ $fase_s != null? $fase_s->fase : '' }}
        </a>
        <a class="nav-item nav-link" id="nav-final-fase-tab" data-toggle="tab" href="#nav-final-fase" role="tab" aria-controls="nav-final-fase" aria-selected="false">
            {{ $fase_f != null? $fase_f->fase : '' }}
        </a>
    </div>
</nav>
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-pri-fase" role="tabpanel" aria-labelledby="nav-pri-fase-tab">
        <div class="p-3 mb-2 bg-danger text-center text-white">
            <h1>{{ $fase_p->fase }}</h1>
        </div>
        @foreach($grupos_p as $g => $grupo)
        <!-- div para el titulo de fase -->
        <div class="p-3 mb-2 bg-warning text-center text-dark">
            <h2>{{ $grupo->grupo }}</h2>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <table class="table table-bordered table-dark">
                    <thead>
                        <tr>
                            <th class="text-center">NOMBRE DEL CLUB</th>
                            <th></th>
                            <th class="text-center">NOMBRE DEL CLUB</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($fechas_p[$g] as $f => $fecha)
                        <tr class="bg-info">
                            <th colspan="3" class="text-center">
                                Fecha {{ $f + 1 }} : {{ $fecha->fecha }}
                            </th>
                        </tr>
                            @foreach($fecha->partidos as $key3 => $partido)
                                @php($cancha = "sin definir")
                                @foreach($canchas as $aux)
                                    @if($aux->id_cancha == $partido->id_cancha)
                                        @php($cancha = $aux->nombre)
                                    @endif
                                @endforeach
                            <tr>
                                <td class="text-center">
                                    {{ $partido->equipo1 }}<br>
                                    <img src="/img/{{ $partido->logo1 }}" height="100" class="rounded noPrint"></td>
                                <td>
                                    <div class="row">
                                        <div class="col">
                                            Hora: <b>{{$partido->hora==null?"sin definir":$partido->hora}}</b>
                                        </div>
                                        <div class="col">
                                            Cancha: <b>{{ $cancha }}</b>
                                        </div>
                                    </div>
                                    <div class="row noPrint">
                                        <div class="col text-center">
                                            <h2>
                                            {{ $partido->goles_equi1==null?0:$partido->goles_equi1 }}
                                             - 
                                            {{ $partido->goles_equi2==null?0:$partido->goles_equi2 }}
                                            </h2>
                                        </div>
                                    </div>
                                    <div class="noPrint text-center">
                                        <div class="row">
                                            <div class="col">
                                                <a href="planilla/{{ $partido->id_partido }}" class="btn btn-success btn-sm" target="_blank">
                                                    <i class="fa fa-futbol-o fa-lg"> Planilla</i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="row {{ $partido->jugado == 1?'':'fade' }}">
                                            <div class="col">
                                                <div class="badge badge-danger rounded">partido completado</div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    {{ $partido->equipo2 }}<br>
                                    <img src="/img/{{ $partido->logo2 }}" height="100" class="rounded noPrint"></td>
                            </tr>
                            @endforeach
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @endforeach
    </div>
    <div class="tab-pane fade" id="nav-seg-fase" role="tabpanel" aria-labelledby="nav-seg-fase-tab">
        @if($fase_s != null)
            <div class="p-3 mb-2 bg-danger text-center text-white">
                <h1>{{ $fase_s->fase }}</h1>
            </div>
            @foreach($grupos_s as $g => $grupo)
            <!-- div para el titulo de fase -->
            <div class="p-3 mb-2 bg-warning text-center text-dark">
                <h2>{{ $grupo->grupo }}</h2>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-bordered table-dark">
                        <thead>
                            <tr>
                                <th class="text-center">NOMBRE DEL CLUB</th>
                                <th></th>
                                <th class="text-center">NOMBRE DEL CLUB</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($fechas_s[$g] as $f => $fecha)
                            <tr class="bg-info">
                                <th colspan="3" class="text-center">
                                    Fecha {{ $f + 1 }} : {{ $fecha->fecha }}
                                </th>
                            </tr>
                                @foreach($fecha->partidos as $key3 => $partido)
                                    @php($cancha = "sin definir")
                                    @foreach($canchas as $aux)
                                        @if($aux->id_cancha == $partido->id_cancha)
                                            @php($cancha = $aux->nombre)
                                        @endif
                                    @endforeach
                                <tr>
                                    <td class="text-center">
                                        {{ $partido->equipo1 }}<br>
                                        <img src="/img/{{ $partido->logo1 }}" height="100" class="rounded noPrint"></td>
                                    <td>
                                        <div class="row">
                                            <div class="col">
                                                Hora: <b>{{$partido->hora==null?"sin definir":$partido->hora}}</b>
                                            </div>
                                            <div class="col">
                                                Cancha: <b>{{ $cancha }}</b>
                                            </div>
                                        </div>
                                        <div class="row noPrint">
                                            <div class="col text-center">
                                                <h2>
                                                {{ $partido->goles_equi1==null?0:$partido->goles_equi1 }}
                                                 - 
                                                {{ $partido->goles_equi2==null?0:$partido->goles_equi2 }}
                                                </h2>
                                            </div>
                                        </div>
                                        <div class="noPrint text-center">
                                            <div class="row">
                                                <div class="col">
                                                    <a href="planilla/{{ $partido->id_partido }}" class="btn btn-success btn-sm" target="_blank">
                                                        <i class="fa fa-futbol-o fa-lg"> Planilla</i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="row {{ $partido->jugado == 1?'':'fade' }}">
                                                <div class="col">
                                                    <div class="badge badge-danger rounded">partido completado</div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        {{ $partido->equipo2 }}<br>
                                        <img src="/img/{{ $partido->logo2 }}" height="100" class="rounded noPrint"></td>
                                </tr>
                                @endforeach
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @endforeach
        @endif
    </div>
    <div class="tab-pane fade" id="nav-final-fase" role="tabpanel" aria-labelledby="nav-final-fase-tab">
        @if($fase_f != null)
            <div class="p-3 mb-2 bg-danger text-center text-white">
                <h1>{{ $fase_f->fase }}</h1>
            </div>
            @foreach($grupos_f as $g => $grupo)
            <!-- div para el titulo de fase -->
            <div class="p-3 mb-2 bg-warning text-center text-dark">
                <h2>{{ $grupo->grupo }}</h2>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-bordered table-dark">
                        <thead>
                            <tr>
                                <th class="text-center">NOMBRE DEL CLUB</th>
                                <th></th>
                                <th class="text-center">NOMBRE DEL CLUB</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($fechas_f[$g] as $f => $fecha)
                            <tr class="bg-info">
                                <th colspan="3" class="text-center">
                                    Fecha {{ $f + 1 }} : {{ $fecha->fecha }} 
                                    <h3>{{ $f==0?' 3er y 4to lugar':($f==1?' 1er y 2do lugar':'') }}</h3>
                                </th>
                            </tr>
                                @foreach($fecha->partidos as $key3 => $partido)
                                    @php($cancha = "sin definir")
                                    @foreach($canchas as $aux)
                                        @if($aux->id_cancha == $partido->id_cancha)
                                            @php($cancha = $aux->nombre)
                                        @endif
                                    @endforeach
                                <tr>
                                    <td class="text-center">
                                        {{ $partido->equipo1 }}<br>
                                        <img src="/img/{{ $partido->logo1 }}" height="100" class="rounded noPrint"></td>
                                    <td>
                                        <div class="row">
                                            <div class="col">
                                                Hora: <b>{{$partido->hora==null?"sin definir":$partido->hora}}</b>
                                            </div>
                                            <div class="col">
                                                Cancha: <b>{{ $cancha }}</b>
                                            </div>
                                        </div>
                                        <div class="row noPrint">
                                            <div class="col text-center">
                                                <h2>
                                                {{ $partido->goles_equi1==null?0:$partido->goles_equi1 }}
                                                 - 
                                                {{ $partido->goles_equi2==null?0:$partido->goles_equi2 }}
                                                </h2>
                                            </div>
                                        </div>
                                        <div class="noPrint text-center">
                                            <div class="row">
                                                <div class="col">
                                                    <a href="planilla/{{ $partido->id_partido }}" class="btn btn-success btn-sm" target="_blank">
                                                        <i class="fa fa-futbol-o fa-lg"> Planilla</i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="row {{ $partido->jugado == 1?'':'fade' }}">
                                                <div class="col">
                                                    <div class="badge badge-danger rounded">partido completado</div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        {{ $partido->equipo2 }}<br>
                                        <img src="/img/{{ $partido->logo2 }}" height="100" class="rounded noPrint"></td>
                                </tr>
                                @endforeach
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @endforeach
        @endif
    </div>
</div>

        

        

@endsection