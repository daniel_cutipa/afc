@extends('layouts.app')
@section('htmlheader_title')
Temporada/Planilla
@endsection
@section('content')
{{ csrf_field() }}

<button type="button" class="btn btn-outline-primary rounded" data-toggle="modal" data-target="#modal_informe_arbitro">
    Agregar Informe del Arbitro
</button>
<button type="button" class="btn btn-outline-primary rounded" data-toggle="modal" data-target="#modal_informe_veedor">
    Agregar Informe del Veedor
</button>
<button type="button" class="btn btn-outline-primary rounded" data-toggle="modal" data-target="#modal_informe_delegado">
    Agregar Informe del Delegado
</button>
<button type="button" class="btn btn-outline-primary rounded" data-toggle="modal" data-target="#modal_informe_planillero">
    Agregar Informe del planillero
</button>
<button type="button" class="btn btn-success rounded" id="partido_completo" {{ $jugado == 1?'hidden':''}}>
    PARTIDO completado
</button>
<hr>
<!-- modal informe del arbitro -->
<div id="modal_informe_arbitro" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Informe Arbitro</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="nombre_arbitro">Nombre del Arbitro:</label>
                            <input type="text" class="form-control" id="nombre_arbitro">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="informe_arbitro">Informe del Arbitro:</label>
                            <textarea id="informe_arbitro" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success add_modal_arbitro" data-dismiss="modal">
                    <i class="fa fa-check fa-lg"></i> Guardar
                </button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">
                    <i class="fa fa-remove fa-lg"></i> Cerrar
                </button>
            </div>
        </div>
    </div>
</div>
<!-- modal informe del veedor -->
<div id="modal_informe_veedor" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Informe Veedor</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="nombre_veedor">Nombre del Veedor:</label>
                            <input type="text" class="form-control" id="nombre_veedor">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="conducta_publico">Conducta del Publico:</label>
                            <textarea id="conducta_publico" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="conducta_jugadores">Conducta de los Jugadores:</label>
                            <textarea id="conducta_jugadores" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="calificacion_arbitro">Calificacion del Arbitro:</label>
                            <textarea id="calificacion_arbitro" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success add_modal_veedor" data-dismiss="modal">
                    <i class="fa fa-check fa-lg"></i> Guardar
                </button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">
                    <i class="fa fa-remove fa-lg"></i> Cerrar
                </button>
            </div>
        </div>
    </div>
</div>
<!-- modal informe del delegado -->
<div id="modal_informe_delegado" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Informe Delegado</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="nombre_delegado">Nombre del Delegado:</label>
                            <input type="text" class="form-control" id="nombre_delegado">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="informe_delegado">Informe del Delegado:</label>
                            <textarea id="informe_delegado" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success add_modal_delegado" data-dismiss="modal">
                    <i class="fa fa-check fa-lg"></i> Guardar
                </button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">
                    <i class="fa fa-remove fa-lg"></i> Cerrar
                </button>
            </div>
        </div>
    </div>
</div>
<!-- modal informe del planillero -->
<div id="modal_informe_planillero" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Informe Planillero</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="nombre_planillero">Nombre del Planillero:</label>
                            <input type="text" class="form-control" id="nombre_planillero">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="informe_planillero">Informe del Planillero:</label>
                            <textarea id="informe_planillero" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success add_modal_planillero" data-dismiss="modal">
                    <i class="fa fa-check fa-lg"></i> Guardar
                </button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">
                    <i class="fa fa-remove fa-lg"></i> Cerrar
                </button>
            </div>
        </div>
    </div>
</div>
<!-- modal agregar goles -->
<div id="modal_goles" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Goles</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="goles">Cantidad de Goles:</label>
                            <input type="number" class="form-control" id="goles" min="0">
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="id_jugador_goles">
            <div class="modal-footer">
                <button type="button" class="btn btn-success add_modal_goles" data-dismiss="modal">
                    <i class="fa fa-check fa-lg"></i> Guardar
                </button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">
                    <i class="fa fa-remove fa-lg"></i> Cerrar
                </button>
            </div>
        </div>
    </div>
</div>
<!-- modal agregar faltas -->
<div id="modal_faltas" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Faltas</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="observacion">Observacion:</label>
                            <textarea id="observacion" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="tarjeta">Tarjeta:</label>
                            <select id="tarjeta" class="form-control">
                                <option value="2">tarjeta roja</option>
                                <option value="3">tarjeta amarilla</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="id_jugador_faltas">
            <div class="modal-footer">
                <button type="button" class="btn btn-success add_modal_faltas" data-dismiss="modal">
                    <i class="fa fa-check fa-lg"></i> Guardar
                </button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">
                    <i class="fa fa-remove fa-lg"></i> Cerrar
                </button>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col text-left"><h3>Club: {{ $club1[0]->nombre }}</h3></div>
    <div class="col text-center"><img src="/img/{{$club1[0]->logo}}" height="100" class='rounded'></div>
    @can('admin')
    <div class="col-1">
        <input type="number" class="form-control form-control-lg txt_ptos" min="0" max="3" data-id="{{$club1[0]->id_equipo}}" value="{{$partido[0]->ptos_equi1}}" data-toggle="tooltip" data-placement="top" title="Solo USAR en caso de WalkOver">
    </div>
    <div class="col-1">
        <input type="number" class="form-control form-control-lg txt_ptos" min="0" max="3" data-id="{{$club2[0]->id_equipo}}" value="{{$partido[0]->ptos_equi2}}" data-toggle="tooltip" data-placement="top" title="Solo USAR en caso de WalkOver">
    </div>
    @endcan
    <div class="col text-center"><img src="/img/{{ $club2[0]->logo }}" height="100" class='rounded'></div>
    <div class="col text-right"><h3>Club: {{ $club2[0]->nombre }}</h3></div>
</div>
<!-- lista de jugadores en tablas -->
<div class="row">
	<div class="col">
        <!-- tabla para el equipo 1 -->
        <button type="button" class="btn btn-success btn-sm" onclick="javascript:jugadores({{ $club1[0]->id_equipo }})" data-toggle="tooltip" data-placement="top" title="Lista de jugadores del Club">
            <i class="fa fa-plus fa-lg"></i> JUGADORES
        </button>
		<table class="table table-hover table-dark table-sm" data-id="{{$club1[0]->id_equipo}}">
            <thead>
                <tr>
                    <th style="width: 9%">N°</th>
                	<th style="width: 11%">#</th>
                    <th>Nombre y Apellidos</th>
                    <th style="width: 11%">x</th>
                    <th>acciones</th>
                </tr>
            </thead>
            <tbody>
            @foreach($titulares1 as $jugador)
                <tr id="{{$jugador->id_jugador}}">
                    <td>{{$jugador->matricula}}</td>
                    <td><input type='number' class='form-control form-control-sm txt_num' value='{{$jugador->numero}}'></td>
                    <td>{{$jugador->nombre}}</td>
                    <td></td>
                    <td>
                        <a href='#' class='btn btn-success btn-xs add_goles' data-toggle="tooltip" data-placement="top" title="Agregar Goles">
                            <i class='fa fa-futbol-o fa-lg'></i>
                        </a>
                        <a href='#' class='btn btn-danger btn-xs add_faltas' data-toggle="tooltip" data-placement="top" title="Agregar Falta">
                            <i class='fa fa-tags fa-lg'></i>
                        </a>
                        <a href='#' class='btn btn-danger btn-xs delete_jugador' data-toggle="tooltip" data-placement="top" title="Eliminar de la lista">
                            <i class='fa fa-trash fa-lg'></i>
                        </a>
                    </td>
                </tr>
            @endforeach
                <tr class='text-center'>
                    <td colspan='5'>SUPLENTES</td>
                </tr>
            @foreach($suplentes1 as $jugador)
                <tr id="{{$jugador->id_jugador}}">
                    <td>{{$jugador->matricula}}</td>
                    <td><input type='number' class='form-control form-control-sm txt_num' value='{{$jugador->numero}}'></td>
                    <td>{{$jugador->nombre}}</td>
                    <td><input type='number' class='form-control form-control-sm txt_num_s' value='{{$jugador->numero_s}}'></td>
                    <td>
                        <a href='#' class='btn btn-success btn-xs add_goles' data-toggle="tooltip" data-placement="top" title="Agregar Goles">
                            <i class='fa fa-futbol-o fa-lg'></i>
                        </a>
                        <a href='#' class='btn btn-danger btn-xs add_faltas' data-toggle="tooltip" data-placement="top" title="Agregar Falta">
                            <i class='fa fa-tags fa-lg'></i>
                        </a>
                        <a href='#' class='btn btn-danger btn-xs delete_jugador' data-toggle="tooltip" data-placement="top" title="Eliminar de la lista">
                            <i class='fa fa-trash fa-lg'></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
	</div>
	<div class="col">
        <!-- tabla para el equipo 2 -->
        <button type="button" class="btn btn-success btn-sm" onclick="javascript:jugadores({{ $club2[0]->id_equipo }})" data-toggle="tooltip" data-placement="top" title="Lista de jugadores del Club">
            <i class="fa fa-plus fa-lg"></i> JUGADORES
        </button>
		<table class="table table-hover table-dark table-sm" data-id="{{$club2[0]->id_equipo}}">
            <thead>
                <tr>
                	<th style="width: 9%">N°</th>
                    <th style="width: 11%">#</th>
                    <th>Nombre y Apellidos</th>
                    <th style="width: 11%">x</th>
                    <th>acciones</th>
                </tr>
            </thead>
            <tbody>
            @foreach($titulares2 as $jugador)
                <tr id="{{$jugador->id_jugador}}">
                    <td>{{$jugador->matricula}}</td>
                    <td><input type='number' class='form-control form-control-sm txt_num' value='{{$jugador->numero}}'></td>
                    <td>{{$jugador->nombre}}</td>
                    <td></td>
                    <td>
                        <a href='#' class='btn btn-success btn-xs add_goles' data-toggle="tooltip" data-placement="top" title="Agregar Goles">
                            <i class='fa fa-futbol-o fa-lg'></i>
                        </a>
                        <a href='#' class='btn btn-danger btn-xs add_faltas' data-toggle="tooltip" data-placement="top" title="Agregar Falta">
                            <i class='fa fa-tags fa-lg'></i>
                        </a>
                        <a href='#' class='btn btn-danger btn-xs delete_jugador' data-toggle="tooltip" data-placement="top" title="Eliminar de la lista">
                            <i class='fa fa-trash fa-lg'></i>
                        </a>
                    </td>
                </tr>
            @endforeach
                <tr class='text-center'>
                    <td colspan='5'>SUPLENTES</td>
                </tr>
            @foreach($suplentes2 as $jugador)
                <tr id="{{$jugador->id_jugador}}">
                    <td>{{$jugador->matricula}}</td>
                    <td><input type='number' class='form-control form-control-sm txt_num' value='{{$jugador->numero}}'></td>
                    <td>{{$jugador->nombre}}</td>
                    <td><input type='number' class='form-control form-control-sm txt_num_s' value='{{$jugador->numero_s}}'></td>
                    <td>
                        <a href='#' class='btn btn-success btn-xs add_goles' data-toggle="tooltip" data-placement="top" title="Agregar Goles">
                            <i class='fa fa-futbol-o fa-lg'></i>
                        </a>
                        <a href='#' class='btn btn-danger btn-xs add_faltas' data-toggle="tooltip" data-placement="top" title="Agregar Falta">
                            <i class='fa fa-tags fa-lg'></i>
                        </a>
                        <a href='#' class='btn btn-danger btn-xs delete_jugador' data-toggle="tooltip" data-placement="top" title="Eliminar de la lista">
                            <i class='fa fa-trash fa-lg'></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
	</div>
</div>

<!-- modal agregar jugadores -->
<div id="modal_jugadores" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Agregar Jugadores</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <h2>Lista de Jugadores</h2>
                <input type="hidden" id="id_equipo">
                <table class="table table-hover table-sm">
                    <thead>
                        <tr>
                            <th style="width: 12%">Matricula</th>
                            <th>Nombre y Apellidos</th>
                            <th>Edad</th>
                            <th>Nacionalidad</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="table_jugadores">
                        
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success add_modal_jugadores" data-dismiss="modal">
                    <i class="fa fa-check fa-lg"></i> Seleccionar
                </button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">
                    <i class="fa fa-remove fa-lg"></i> Cerrar
                </button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<!-- script agrega goles de un jugador -->
<script>
    var id_equipo_obser = null;
    var btn_gol = null;
    $(document).on('click', '.add_goles', function(e) {
        e.preventDefault();
        btn_gol = $(this);
        $('#goles').val('');
        $('#id_jugador_goles').val($(this).parents('tr').attr('id'));
        id_equipo_obser = $(this).parents('table').data('id');
        $('#modal_goles').modal('show');
    });
    $('.modal-footer').on('click', '.add_modal_goles', function() {
    	if ($('#goles').val() < 1) {
    		toastr.error('Disculpe, solo valores positivos!', 'Error Alert', {timeOut: 5000});
    		return;
    	}
        $.ajax({
            type: 'POST',
            url: 'addObservacion',
            data: {
                '_token': $('input[name=_token]').val(),
                'id_jugador': $('#id_jugador_goles').val(),
                'id_partido': '{{$id_partido}}',
                'id_tipo_observacion': 1,// el 1 es gol
                'observacion': $('#goles').val(),
                'id_equipo': id_equipo_obser,
            },
            success : function(data) {
                // CONVERTIR EN NUMERO EL VALOR QUE TIENE 
                var goles = btn_gol.find('i').val() + $('#goles').val();
                btn_gol.find('i').append(' '+goles);
                toastr.success(data.message, 'Success Alert', {timeOut: 5000});
            },
            error : function(xhr, status) {
                toastr.error('Disculpe, existio un problema!', 'Error Alert', {timeOut: 5000});
            },
        });
        id_equipo_obser = null;
    });
</script>
<!-- script agrega faltas de un jugador -->
<script>
    $(document).on('click', '.add_faltas', function(e) {
        e.preventDefault();
        $('#observacion').val('');
        $('#id_jugador_faltas').val($(this).parents('tr').attr('id'));
        // $('.div_suspension').remove();
        // $('.div_suspension').removeAttr('hidden');
        $('#modal_faltas').modal('show');
    });
    // $("#checkbox").change(event => {
    // 	if (event.target.checked) {
    // 		$('.div_suspension').removeAttr('hidden');
    // 	}
    // 	else{
    // 		$('.div_suspension').attr('hidden', '');
    // 	}
    // });
    $('.modal-footer').on('click', '.add_modal_faltas', function() {
        $.ajax({
            type: 'POST',
            url: 'addObservacion',
            data: {
                '_token': $('input[name=_token]').val(),
                'id_jugador': $('#id_jugador_faltas').val(),
                'id_partido': '{{$id_partido}}',
                'id_tipo_observacion': $('#tarjeta').val(),
                'observacion': $('#observacion').val(),
                'id_equipo': null,
            },
            success : function(data) {
                toastr.success(data.message, 'Success Alert', {timeOut: 5000});
            },
            error : function(xhr, status) {
                toastr.error('Disculpe, existio un problema!', 'Error Alert', {timeOut: 5000});
            },
        });
    });
</script>
<!-- script elimina un jugador de la tabla -->
<script>
    $(document).on('click', '.delete_jugador', function(e) {
        e.preventDefault();
        // OCULTAR EL TOOLTIP ANTES DE ELIMINAR
        $(this).tooltip('dispose');
        var fila = $(this).parents('tr').attr('id');
        $.ajax({
            type: 'POST',
            url: 'deleteLogPartido',
            data: {
                '_token': $('input[name=_token]').val(),
                'id_partido': '{{$id_partido}}',
                'id_equipo': $(this).parents('table').data('id'),
                'id_jugador': $(this).parents('tr').attr('id'),
            },
            success : function(data) {
                $('#'+fila).remove();
            },
        });
    });
</script>
<!-- script agrega informe del arbitro -->
<script>
    $('.modal-footer').on('click', '.add_modal_arbitro', function() {
        $.ajax({
            type: 'POST',
            url: 'add_informe_arbitro',
            data: {
                '_token': $('input[name=_token]').val(),
                'id_partido': '{{$id_partido}}',
                'nombre': $('#nombre_arbitro').val(),
                'informe': $('#informe_arbitro').val(),
            },
            success : function(data) {
                toastr.success(data.message, 'Success Alert', {timeOut: 5000});
            },
            error : function(xhr, status) {
                toastr.error('Disculpe, existio un problema!', 'Error Alert', {timeOut: 5000});
            },
        });
    });
</script>
<!-- script agrega informe del delegado -->
<script>
    $('.modal-footer').on('click', '.add_modal_delegado', function() {
        $.ajax({
            type: 'POST',
            url: 'add_informe_delegado',
            data: {
                '_token': $('input[name=_token]').val(),
                'id_partido': '{{$id_partido}}',
                'nombre': $('#nombre_delegado').val(),
                'informe': $('#informe_delegado').val(),
            },
            success : function(data) {
                toastr.success(data.message, 'Success Alert', {timeOut: 5000});
            },
            error : function(xhr, status) {
                toastr.error('Disculpe, existio un problema!', 'Error Alert', {timeOut: 5000});
            },
        });
    });
</script>
<!-- script agrega informe del veedor -->
<script>
    $('.modal-footer').on('click', '.add_modal_veedor', function() {
        $.ajax({
            type: 'POST',
            url: 'add_informe_veedor',
            data: {
                '_token': $('input[name=_token]').val(),
                'id_partido': '{{$id_partido}}',
                'nombre': $('#nombre_veedor').val(),
                'conducta_publico': $('#conducta_publico').val(),
                'conducta_jugadores': $('#conducta_jugadores').val(),
                'calificacion_arbitro': $('#calificacion_arbitro').val(),
            },
            success : function(data) {
                toastr.success(data.message, 'Success Alert', {timeOut: 5000});
            },
            error : function(xhr, status) {
                toastr.error('Disculpe, existio un problema!', 'Error Alert', {timeOut: 5000});
            },
        });
    });
</script>
<!-- script agrega informe del planillero -->
<script>
    $('.modal-footer').on('click', '.add_modal_planillero', function() {
        $.ajax({
            type: 'POST',
            url: 'add_informe_planillero',
            data: {
                '_token': $('input[name=_token]').val(),
                'id_partido': '{{$id_partido}}',
                'nombre': $('#nombre_planillero').val(),
                'informe': $('#informe_planillero').val(),
            },
            success : function(data) {
                toastr.success(data.message, 'Success Alert', {timeOut: 5000});
            },
            error : function(xhr, status) {
                toastr.error('Disculpe, existio un problema!', 'Error Alert', {timeOut: 5000});
            },
        });
    });
</script>
<!-- script para completar el partido -->
<script>
    $('#partido_completo').on('click', function() {
        // HACER UNA PREGUNTA DE SI ESTA DEACUERDO 
        if (!confirm('¿Estas seguro de completar el partido?\n el partido figurara en la tabla de posiciones como partido jugado!!!')) {
            return false;
        }
        $.ajax({
            type: 'POST',
            url: 'updateJugado',
            data: {
                '_token': $('input[name=_token]').val(),
                'id_partido': '{{$id_partido}}',
            },
            success : function(data) {
                $('#partido_completo').attr('hidden', '');
                toastr.success(data.message, 'Success Alert', {timeOut: 5000});
            },
            error : function(xhr, status) {
                toastr.error('Disculpe, existio un problema!', 'Error Alert', {timeOut: 5000});
            },
        });
    });
</script>
<!-- script muestra jugadores en modal y agrega jugadores -->
<script>
    // carga el modal con datos para su tabla con jugadores
    var cant_titulares = 0;
    function jugadores(id_equipo){
        $('#table_jugadores').empty();
        $('#id_equipo').val(id_equipo);
        $.get('/temporada/planilla/getJugadores/'+{{$id_partido}}+'/'+id_equipo, function(data){
            // COSULTAR CUANTOS TITULARES EXISTEN EN LA 
            cant_titulares = data.cant_titulares;
            $.each(data.jugadores,function(index,result){
                $('#table_jugadores').append("\
                    <tr data-id='"+result.id_jugador+"'>\
                        <td>"+result.matricula+"</td>\
                        <td>"+result.nombre+"</td>\
                        <td>"+result.fecha_nacimiento+"</td>\
                        <td>"+result.nacionalidad+"</td>\
                        <td>\
                            <div class='row'>\
                                <div class='col'>\
                                    <div class='i-checks'>\
                                        <input id='ch_t"+index+"' type='checkbox' class='form-control-custom check_t'>\
                                        <label for='ch_t"+index+"'>titular</label>\
                                    </div>\
                                </div>\
                                <div class='col'>\
                                    <div class='i-checks'>\
                                        <input id='ch_s"+index+"' type='checkbox' class='form-control-custom check_s'>\
                                        <label for='ch_s"+index+"'>suplente</label>\
                                    </div>\
                                </div>\
                            </div>\
                        </td>\
                    </tr>\
                ");
            });
            $('#modal_jugadores').modal('show');
            toastr.info('seleccionar por lo menos 1 menor de edad', 'Alert', {timeOut: 6000});
        });
    }
    // cuando se cierra el modal
    $('.modal-footer').on('click', '.add_modal_jugadores', function() {
        var list_titulares = [];
        var list_suplentes = [];
        var tabla = $('#table_jugadores').find('tr');
        // este bucle saca a los titulares y suplentes checkeados
        $.each(tabla, function(index,tr){
            var id_jugador = $(tabla[index]).data('id');
            var matricula = tr.cells[0].childNodes[0].nodeValue;
            var nombre = tr.cells[1].childNodes[0].nodeValue;
            var row = [id_jugador, matricula, nombre];
            var check_t = ($(tr).find('.check_t'))[0].checked;
            var check_s = ($(tr).find('.check_s'))[0].checked;
            if (check_t){
                if (cant_titulares == 11) 
                    toastr.error('solo puedes ingresar 11 jugadores titulares', 'Error Alert', {timeOut: 5000});
                cant_titulares++;
                if (cant_titulares <= 11)
                    list_titulares.push(row);
            }
            else if(check_s)
                list_suplentes.push(row);
        });
        $('#table_jugadores').empty();
        // este bucle pone en la tabla a los titulares que an sido seleccionados
        toastr.info('estamos trabajando', 'Espere por favor', {timeOut: 6000});
        var count = 0;
        var extranjeros = 0;
        var menor_edad = 0;
        $.each(list_titulares, function(index,titular){
            // guarda el la tabla log_partido a los jugadores
            $.ajax({
                type: 'POST',
                url: 'addLogPartido',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id_partido': '{{$id_partido}}',
                    'id_equipo': $('#id_equipo').val(),
                    'id_jugador': titular[0],
                    'titular': 1,
                },
                success : function(data) {
                    extranjeros += data.nacionalidad==1?0:1;
                    count++;
                    if (extranjeros >= 4) 
                        toastr.warning('maximo 4 jugadores extranjeros\n corregir por favor!!!', 'Alert', {timeOut: 5000});
                    // POR AQUI CONTROLAR SI ES MENOR DE EDAD
                    if (count == (list_titulares.length + list_suplentes.length))
                        location.reload(true);
                },
            });
        });
        // este bucle pone en la tabla a los suplentes que an sido seleccionados
        $.each(list_suplentes, function(index,suplente){
            // guarda el la tabla log_partido a los jugadores
            $.ajax({
                type: 'POST',
                url: 'addLogPartido',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id_partido': '{{$id_partido}}',
                    'id_equipo': $('#id_equipo').val(),
                    'id_jugador': suplente[0],
                    'titular': 0,
                },
                success : function(data) {
                    count++;
                    if (count == (list_titulares.length + list_suplentes.length))
                        location.reload(true);
                },
            });
        });
    });
</script>
<!-- script para modificar los numero de los jugadores titulares y suplentes -->
<script>
    $('.txt_num').change( function() {
        $.ajax({
            type: 'POST',
            url: 'update_num',
            data: {
                '_token': $('input[name=_token]').val(),
                'id_partido': '{{$id_partido}}',
                'id_equipo': $(this).parents('table').data('id'),
                'id_jugador': $(this).parents('tr').attr('id'),
                'numero': $(this).val(),
            },
            success : function(data) {
                if (data.message != 1) {
                    toastr.error(data.message, 'Error Alert', {timeOut: 5000});
                }
            },
        });
    });
    $('.txt_num_s').change( function() {
        $.ajax({
            type: 'POST',
            url: 'update_num_s',
            data: {
                '_token': $('input[name=_token]').val(),
                'id_partido': '{{$id_partido}}',
                'id_equipo': $(this).parents('table').data('id'),
                'id_jugador': $(this).parents('tr').attr('id'),
                'numero_s': $(this).val(),
            },
            success : function(data) {
                if (data.message != 1) {
                    toastr.error(data.message, 'Error Alert', {timeOut: 5000});
                }
            },
        });
    });
</script>
<script>
    $('.txt_ptos').change( function() {
        $.ajax({
            type: 'POST',
            url: 'update_partido_ptos',
            data: {
                '_token': $('input[name=_token]').val(),
                'id_partido': '{{$id_partido}}',
                'id_equipo': $(this).data('id'),
                'ptos': $(this).val(),
            },
            success : function(data) {},
        });
    });
</script>

@endsection