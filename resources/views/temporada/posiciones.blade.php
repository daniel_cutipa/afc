@extends('layouts.app')
@section('htmlheader_title')
Temporada/Posiciones
@endsection
@section('content')
{{ csrf_field() }}
<div class="row">
    <div class="col-lg">
        <label for=""><b>Gestion:</b> {{ $fase_p->temporada->gestion }}</label>
    </div>
    <div class="col-lg">
        <label for=""><b>Torneo:</b> {{ $fase_p->temporada->torneo }}</label>
    </div>
    <div class="col-lg">
        <label for=""><b>Fecha de inicio:</b> {{ $fase_p->temporada->fecha_inicio }}</label>
    </div>
    <div class="col-lg">
        <label for=""><b>Fecha fin:</b> {{ $fase_p->temporada->fecha_fin }}</label>
    </div>
    @if($fase_s == NULL)
    <div class="col-lg noPrint" id="div_btn_segunda_fase">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_s_fase">
            Generar Fase
        </button>
    </div>
    @else
    <div class="col-lg noPrint" id="div_btn_final_fase">
        <button type="button" class="btn btn-primary" onclick="finalFase();">
            Generar Fase Final
        </button>
    </div>
    @endif
</div>

<div class="p-3 mb-2 bg-success text-center text-white">
    <h1>TABLA DE POSICIONES (Categoria {{ $fase_p->temporada->categoria->categoria }})</h1>
</div>

<div class="p-3 mb-2 bg-danger text-center text-white">
    <h1>{{ $fase_p->fase }}</h1>
</div>
@foreach($grupos_p as $g => $grupo)
<!-- div para el titulo de fase -->
<div class="p-3 mb-2 bg-warning text-center text-dark">
    <h2>{{ $grupo->grupo }}</h2>
</div>
<table class="table table-hover table-dark">
    <thead>
        <tr>
            <th>N°</th>
            <th>EQUIPOS</th>
            <th>PJ</th>
            <th>PG</th>
            <th>PE</th>
            <th>PP</th>
            <th>GF</th>
            <th>GC</th>
            <th>DIF</th>
            <th>PTOS</th>
        </tr>
    </thead>
    <tbody id="table_partidos">
        @foreach($partidos_p[$g] as $e => $equipo)
        <tr data-id="{{$equipo->id_equipo}}">
            <td>{{ $e + 1 }}</td>
            <td>{{ $equipo->nombre }}</td>
            <td>{{ $equipo->pj }}</td>
            <td>{{ $equipo->pg }}</td>
            <td>{{ $equipo->pe }}</td>
            <td>{{ $equipo->pp }}</td>
            <td>{{ $equipo->gf }}</td>
            <td>{{ $equipo->gc }}</td>
            <td>{{ $equipo->dif }}</td>
            <td>{{ $equipo->ptos }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endforeach

@if($fase_s != NULL)
    <div class="p-3 mb-2 bg-danger text-center text-white">
        <h1>{{ $fase_s->fase }}</h1>
    </div>
    @foreach($grupos_s as $g => $grupo)
    <!-- div para el titulo de fase -->
    <div class="p-3 mb-2 bg-warning text-center text-dark">
        <h2>{{ $grupo->grupo }}</h2>
    </div>
    <table class="table table-hover table-dark">
        <thead>
            <tr>
                <th>N°</th>
                <th>EQUIPOS</th>
                <th>PJ</th>
                <th>PG</th>
                <th>PE</th>
                <th>PP</th>
                <th>GF</th>
                <th>GC</th>
                <th>DIF</th>
                <th>PTOS</th>
            </tr>
        </thead>
        <tbody id="table_partidos_sf">
            @foreach($partidos_s[$g] as $e => $equipo)
            <tr data-id="{{$equipo->id_equipo}}">
                <td>{{ $e + 1 }}</td>
                <td>{{ $equipo->nombre }}</td>
                <td>{{ $equipo->pj }}</td>
                <td>{{ $equipo->pg }}</td>
                <td>{{ $equipo->pe }}</td>
                <td>{{ $equipo->pp }}</td>
                <td>{{ $equipo->gf }}</td>
                <td>{{ $equipo->gc }}</td>
                <td>{{ $equipo->dif }}</td>
                <td>{{ $equipo->ptos }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @endforeach

    @if($fase_f != NULL)
        <div class="p-3 mb-2 bg-danger text-center text-white">
            <h1>{{ $fase_f->fase }}</h1>
        </div>
        <!-- div para el titulo de fase -->
        <div class="p-3 mb-2 bg-warning text-center text-dark">
            <h2>{{ $grupos_f->grupo }}</h2>
        </div>
        <table class="table table-hover table-dark">
            <thead>
                <tr>
                    <th>Posicion</th>
                    <th>EQUIPOS</th>
                </tr>
            </thead>
            <tbody>
                @foreach($partidos_f as $e => $equipo)
                <tr>
                    <td>{{ $equipo->pos }}</td>
                    <td>{{ $equipo->nombre }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    @endif
@endif


<div id="modal_s_fase" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Seleccionar Fase</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="fase">Fase:</label>
                            <select name="fase" class="form-control" id="fase">
                                <option value="Segunda Fase">Segunda Fase</option>
                                <option value="Semi Final">Semi Final</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <div class="form-group">
                            <div class="i-checks">
                                <input id="ida_vuelta" type="checkbox" value="" class="form-control-custom">
                                <label for="ida_vuelta">Ida y Vuelta</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal" onclick="segundaFase();">
                    <i class="fa fa-check fa-lg"></i> Guardar
                </button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">
                    <i class="fa fa-remove fa-lg"></i> Cerrar
                </button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    function segundaFase(){
        if (!confirm('¿Estas seguro de Generar la Fase?\n Todos los partidos deben ser completados!!!')) {
            return false;
        }
        // console.log(document.getElementById('table_partidos').rows[1].cells[0].childNodes[0]);
        // alert(document.getElementById('table_partidos').rows[1].cells[0].childNodes[0].nodeValue);
        var tabla = $('#table_partidos').find('tr');
        var equi1 = $(tabla[0]).data('id');
        var equi2 = $(tabla[1]).data('id');
        var equi3 = $(tabla[2]).data('id');
        var equi4 = $(tabla[3]).data('id');
        $.ajax({
            type: 'POST',
            url: '/temporada/createSegundaFase',
            data: {
                '_token': $('input[name=_token]').val(),
                'id_temporada': '{{$fase_p->temporada->id_temporada}}',
                'equi1': equi1,
                'equi2': equi2,
                'equi3': equi3,
                'equi4': equi4,
                'fase': $('#fase').val(),
                'checked': $('#ida_vuelta').prop('checked'),
            },
            success : function(data) {
                toastr.success(data.message, 'Success Alert', {timeOut: 1000});
                // DESACTIVAR EL BOTON
                $('#div_btn_segunda_fase').attr('hidden', '');
            },
            error : function(xhr, status) {
                toastr.error('Disculpe, existio un problema!', 'Error Alert', {timeOut: 1000});
            },
        });
    }
    function finalFase(){
        if (!confirm('¿Estas seguro de Generar la Fase?\n Todos los partidos deben ser completados!!!')) {
            return false;
        }
        var tabla = $('#table_partidos_sf').find('tr');
        var equi1 = $(tabla[0]).data('id');
        var equi2 = $(tabla[1]).data('id');
        var equi3 = $(tabla[2]).data('id');
        var equi4 = $(tabla[3]).data('id');
        console.log(equi1,equi2,equi3,equi4);
        $.ajax({
            type: 'POST',
            url: '/temporada/createFinalFase',
            data: {
                '_token': $('input[name=_token]').val(),
                'id_temporada': '{{$fase_p->temporada->id_temporada}}',
                'equi1': equi1,
                'equi2': equi2,
                'equi3': equi3,
                'equi4': equi4,
            },
            success : function(data) {
                toastr.success(data.message, 'Success Alert', {timeOut: 1000});
                // DESACTIVAR EL BOTON
                $('#div_btn_final_fase').attr('hidden', '');
            },
            error : function(xhr, status) {
                toastr.error('Disculpe, existio un problema!', 'Error Alert', {timeOut: 1000});
            },
        });
    }
</script>
@endsection