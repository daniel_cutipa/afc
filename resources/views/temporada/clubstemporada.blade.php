@extends('layouts.app')
@section('htmlheader_title')
Temporada
@endsection
@section('content')
<!-- <a href="/club/create" class="btn btn-primary" role="button">Nuevo Club</a> -->
<hr>
<table class="table table-striped table-sm">
   <thead>
       <tr>
           <th>Nombre Club</th>
           <th>Jugadores</th>
       </tr>
   </thead>
   <tbody>
       @foreach($clubes as $club)
       <tr>
           <td>{{ $club->nombre }}</td>

           <td>
               <button class="btn btn-success" onclick="VerModalJugadores({{ $club->id_equipo }})">
                    <i class="fa fa-user fa-lg"></i>
                    Ver
               </button>
               <button class="btn btn-warning" onclick="AgregarModalJugadores({{ $club->id_equipo }}, {{ $club->id_club }} )">
                    <i class="fa fa-plus fa-lg"></i>
                    Agregar
               </button>
           </td>
       </tr>
        @endforeach
   </tbody>
</table>

<div id="addModal_jugador_equipo" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Seleccionar jugador</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="table-jugadores" class="table">
                        <thead>
                            <th>#</th>
                            <th>Accion</th>
                            <th>Nombre completo</th>
                            <th>Matricula</th>
                            <th>Edad</th>
                            <th>Tipo de Jugador</th>
                            <th>Nacionalida</th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success add_modal_temporada" data-dismiss="modal" onclick='guardarJugadorConEquipo()'>
                    <i class="fa fa-check fa-lg"></i> Guardar
                </button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">
                    <i class="fa fa-remove fa-lg"></i> Cerrar
                </button>
            </div>
        </div>
    </div>
</div>

<div id="mostrarModal_jugador_equipo" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Jugadores registrados</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="table-jugadoresmostrar" class="table">
                        <thead>
                            <th>#</th>
                            <th>Accion</th>
                            <th>Nombre completo</th>
                            <th>Matricula</th>
                            <th>Edad</th>
                            <th>Tipo de Jugador</th>
                            <th>Nacionalida</th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger mostrarmodal_jugador_equipo" data-dismiss="modal" onclick='BorrarJugadorConEquipo()'>
                    <i class="fa fa-remove fa-lg"></i> Seleccionados
                </button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">
                    <i class="fa fa-remove fa-lg"></i> Cerrar
                </button>
            </div>
        </div>
    </div>
</div>


@endsection

<script>
    var jugadores = [];
    var idEquipo = 0;

    function cargarJugadores(id_club){
        $('#table-jugadores tbody').html("");
        $.ajax({
            type: 'GET',
            url: 'libres/'+id_club,
            data: {},
            success : function(data) {
                console.log(data);

                let html = "";
                if (data.length == 0) {
                    html += "<tr> <td colspan='7' class='text-center'>No se encontraron datos</td> </tr>";
                }
                console.log(html);
                
                // toastr.success(data.message, 'Success Alert', {timeOut: 5000});
                data.forEach( (fila, index, array)=>{
                    let numeracion = parseInt(index)+1;
                    html += "<tr>\
                        <td>" + numeracion + "</td>\
                        <td> <button class='btn btn-success' seleccionado='false' onclick='captureJugador("+fila.id_jugador+", this)'>Seleccionar</button> </td>\
                        <td>" + fila.nombre + " " + fila.ap_paterno + " " + fila.ap_materno + "</td>\
                        <td>" + fila.matricula + "</td>\
                        <td>" + fila.fecha_nacimiento + "</td>\
                        <td>" + fila.tipo_jugador + "</td>\
                        <td>" + fila.nacionalidad + "</td>\
                    </tr>";
                });

                $('#table-jugadores tbody').prepend(html);
            },
            error : function(xhr, status) {
                toastr.error('Disculpe, existio un problema!', 'Error Alert', {timeOut: 5000});
            },
        });
    }

    function AgregarModalJugadores(id_equipo, id_club){
        jugadores = [];        
        cargarJugadores(id_club);
        idEquipo = id_equipo;
        $("#addModal_jugador_equipo").modal("show");
    }

    function captureJugador(id_jugador, botton){
        if ($(botton).attr('seleccionado') == 'false'){
            $(botton).css('background', 'blue');
            if ( validarJugadores(id_jugador) ){
                jugadores.push({
                    'id_jugador':id_jugador
                });
                console.log(jugadores);
            }
            $(botton).attr('seleccionado','true');
        }else{
            $(botton).css('background', 'green');
            removerJugador(id_jugador);
            $(botton).attr('seleccionado','false');
        }
    }

    /**
     * procedimiento  elimnar jugadores de la arreglo
     */
    function removerJugador(id_jugador){
        jugadores.forEach(function(elemento, index){
            if (id_jugador == elemento.id_jugador){
                jugadores.pop(index);
            } 
        });
    }

    function validarJugadores(id_jugador){
        let res = true;
        jugadores.forEach(function(elemento, index){
            if (id_jugador == elemento.id_jugador){
                res = false;
            } 
        });
        return res;
    }

    function guardarJugadorConEquipo(){
         $.ajax({
            type: 'POST',
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            url: 'agregarequipojugador/',
            data: {
                "_token": "{{ csrf_token() }}",
                'jugadores':jugadores,
                'id_equipo': idEquipo
            },
            success : function(data) {
                console.log(data);
                toastr.success(data.message, 'Success Alert', {timeOut: 5000});
            },
            error : function(xhr, status) {
                toastr.error('Disculpe, existio un problema!', 'Error Alert', {timeOut: 5000});
            },
        });
    }
    function cargarJugadoresmostrar(id_equipo){
        $('#table-jugadoresmostrar tbody').html("");
        $.ajax({
            type: 'GET',
            url: 'equipo/'+id_equipo,
            data: {},
            success : function(data) {
                console.log(data);

                let html = "";
                if (data.length == 0) {
                    html += "<tr> <td colspan='7' class='text-center'>No se encontraron datos</td> </tr>";
                }
                console.log(html);
                
                // toastr.success(data.message, 'Success Alert', {timeOut: 5000});
                data.forEach( (fila, index, array)=>{
                    let numeracion = parseInt(index)+1;
                    html += "<tr>\
                        <td>" + numeracion + "</td>\
                        <td> <button class='btn btn-success' seleccionadoborrar='false' onclick='captureJugadorborrar("+fila.id_jugador+", this)'>Borrar</button> </td>\
                        <td>" + fila.nombre + " " + fila.ap_paterno + " " + fila.ap_materno + "</td>\
                        <td>" + fila.matricula + "</td>\
                        <td>" + fila.fecha_nacimiento + "</td>\
                        <td>" + fila.tipo_jugador+ "</td>\
                        <td>" + fila.nacionalidad + "</td>\
                    </tr>";
                });

                $('#table-jugadoresmostrar tbody').prepend(html);
            },
            error : function(xhr, status) {
                toastr.error('Disculpe, existio un problema!', 'Error Alert', {timeOut: 5000});
            },
        });
    }

    function VerModalJugadores(id_equipo){
        jugadores = [];
        cargarJugadoresmostrar(id_equipo);
        idEquipo = id_equipo;
        $("#mostrarModal_jugador_equipo").modal("show");
    }

    function captureJugadorborrar(id_jugador, botton){
        if ($(botton).attr('seleccionadoborrar') == 'false'){
            $(botton).css('background', '#C0392B');
            if ( validarJugadores(id_jugador) ){
                jugadores.push({
                    'id_jugador':id_jugador
                });
                console.log(jugadores);
            }
            $(botton).attr('seleccionadoborrar','true');
        }else{
            $(botton).css('background', 'green');
            removerJugador(id_jugador);
            $(botton).attr('seleccionadoborrar','false');
        }
    }

    function validarJugadoresborrar(id_jugador){
        let res = true;
        jugadores.forEach(function(elemento, index){
            if (id_jugador == elemento.id_jugador){
                res = false;
            } 
        });
        return res;
    }

    function BorrarJugadorConEquipo(){
         $.ajax({
            type: 'POST',
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            url: 'borrarequipojugador/',
            data: {
                "_token": "{{ csrf_token() }}",
                'jugadores':jugadores,
                'id_equipo': idEquipo                
            },
            success : function(data) {
                console.log(data);
                toastr.success(data.message, 'Success Alert', {timeOut: 5000});
            },
            error : function(xhr, status) {
                toastr.error('Disculpe, existio un problema!', 'Error Alert', {timeOut: 5000});
            },
        });
    }
</script>
