@extends('layouts.app')
@section('htmlheader_title')
Temporada
@endsection
@section('content')
@can('admin')
<a href="#" class="add_modal btn btn-primary" role="button">Nueva Temporada</a>
@endcan
<hr>
<div class="row">
    <div class="col-lg-12">
        <table class="table table-striped table-sm" >
            <thead>
                <tr>
                    <th>Nombre Categoria</th>
                    <th>Gestion</th>
                    <th>Torneo</th>
                    <th>Fecha Inicio</th>
                    <th>Fecha Fin</th>
                    <th>acciones</th>
                </tr>
                {{ csrf_field() }}
            </thead>
            <tbody id="temporada_table">
                @foreach($temporadas as $temporada)
                <tr class="item{{ $temporada->id_temporada }}">
                    <td>{{ $temporada->categoria->categoria }}</td>
                    <td>{{ $temporada->gestion }}</td>
                    <td>{{ $temporada->torneo }}</td>
                    <td>{{ $temporada->fecha_inicio }}</td>
                    <td>{{ $temporada->fecha_fin == NULL? 'sin definir':$temporada->fecha_fin }}</td>
                    <td>
                        @can('admin')
                        <a href="#" class="btn btn-danger btn-sm" onclick="eliminar({{ $temporada->id_temporada }}, '/temporada', event)" >
                            <i class="fa fa-trash fa-lg"></i>
                        </a>
                        <a href="#" class="btn btn-primary btn-sm edit-modal-temporada" data-id="{{ $temporada->id_temporada }}">
                            <i class="fa fa-edit fa-lg"></i>
                        </a>
                        <a href="#" class="btn btn-success btn-sm show-fases" data-id="{{ $temporada->id_temporada }}">
                            <i class="fa fa-edit fa-lg"> Fase</i>
                        </a>
                        @endcan
                        <a href="/temporada/{{ $temporada->id_temporada }}" target="_blank" class="btn btn-success btn-sm">
                            <i class="fa fa-eye fa-lg"> Fixture</i>
                        </a>
                        @can('admin')
                        <a href="/temporada/{{ $temporada->id_temporada }}/posiciones" target="_blank" class="btn btn-success btn-sm">
                            <i class="fa fa-table fa-lg"> Posiciones</i>
                        </a>
                        <a href="/temporada/clubes/{{$temporada->id_temporada}}" target="_blank" class="btn btn-success btn-sm">
                            <i class="fa fa-list-ol fa-lg"> Equipos</i>
                        </a>
                        @endcan
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<!-- div para el titulo de temporada -->
<div class="row" >
    <div class="col">
        <h5 id="titulo_temporada"></h5>
    </div>
</div>

<nav id="div_fases" hidden="hidden">
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" id="nav-pri-fase-tab" data-toggle="tab" href="#nav-pri-fase" role="tab" aria-controls="nav-pri-fase" aria-selected="true">Primera Fase</a>
        <a class="nav-item nav-link" id="nav-seg-fase-tab" data-toggle="tab" href="#nav-seg-fase" role="tab" aria-controls="nav-seg-fase" aria-selected="false">Segunda Fase</a>
        <a class="nav-item nav-link" id="nav-final-fase-tab" data-toggle="tab" href="#nav-final-fase" role="tab" aria-controls="nav-final-fase" aria-selected="false">Final</a>
    </div>
</nav>
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-pri-fase" role="tabpanel" aria-labelledby="nav-pri-fase-tab">
        
    </div>
    <div class="tab-pane fade" id="nav-seg-fase" role="tabpanel" aria-labelledby="nav-seg-fase-tab">
        
    </div>
    <div class="tab-pane fade" id="nav-final-fase" role="tabpanel" aria-labelledby="nav-final-fase-tab">
        
    </div>
</div>

<!-- modals para gestion de temporada -->
<!-- Modal form to add a temporada -->
<div id="addModal_temporada" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Nueva Temporada</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="id_categoria_add">Categoria:</label>
                                <select id="id_categoria_add" class="form-control">
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label># participantes:</label>
                                <input type="number" class="form-control" id="num_equipos" disabled>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="num_grupos">Numero de series:</label>
                                <input type="number" class="form-control" id="num_grupos" min="1" max="10">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="gestion_add">Gestion:</label>
                                <input type="text" class="form-control" id="gestion_add">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="torneo_add">Torneo:</label>
                                <input type="text" class="form-control" id="torneo_add">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="fecha_inicio_add">Fecha Inicio:</label>
                                <input type="date" class="form-control" id="fecha_inicio_add">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-4" for="fecha_fin_add">Fecha Fin:</label>
                                <input type="date" class="form-control" id="fecha_fin_add">
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success add_modal_temporada" data-dismiss="modal">
                    <i class="fa fa-check fa-lg"></i> Guardar
                </button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">
                    <i class="fa fa-remove fa-lg"></i> Cerrar
                </button>
            </div>
        </div>
    </div>
</div>
<!-- Modal form to edit temporada a form -->
<div id="editModal_temporada" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Editar Temporada</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="id_categoria_edit">Categoria:</label>
                                <input type="text" class="form-control" id="id_categoria_edit" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="gestion_edit">Gestion:</label>
                                <input type="text" class="form-control" id="gestion_edit">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="torneo_edit">Torneo:</label>
                                <input type="text" class="form-control" id="torneo_edit">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="fecha_inicio_edit">Fecha Inicio:</label>
                                <input type="date" class="form-control" id="fecha_inicio_edit">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="fecha_fin_edit">Fecha Fin:</label>
                                <input type="date" class="form-control" id="fecha_fin_edit">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="id_temporada_edit">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success edit-temporada" data-dismiss="modal">
                    <i class="fa fa-check fa-lg"></i> Guardar
                </button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">
                    <i class="fa fa-remove fa-lg"></i> Cerrar
                </button>
            </div>
        </div>
    </div>
</div>
<!-- find de modals para gestion de temporada -->

@endsection

@section('script')
<!-- script para gestionar TEMPORADA -->
<!-- script add a new temporada -->
<script >
    $(document).on('click', '.add_modal', function() {
        $('#gestion_add').val('');
        $('#torneo_add').val('');
        $('#fecha_inicio_add').val('');
        $('#fecha_fin_add').val('');
        $('#num_equipos').val('');
        $('#num_grupos').val('');
        $.get('{{url("temporada/create")}}', function(data){
            $('#id_categoria_add').empty();
            $('#id_categoria_add').append("<option value='' disabled selected style='display:none;'>Seleccione una categoria</option>");
            $.each(data,function(index,result){
                $('#id_categoria_add').append('<option value="'+result.id_categoria+'">'+result.categoria+'</option>');
            });
        });
        $('#addModal_temporada').modal('show');
    });
    //cuando hay cambios en el combobox cuenta la cantidad de equipos
    $("#id_categoria_add").change(event => {
        $.get('/temporada/countEquipos/'+ $('#id_categoria_add').val(), function(res, sta){
            $('#num_equipos').val(res.count);
            $('#num_grupos').val(1);
        });
    });
    $('.modal-footer').on('click', '.add_modal_temporada', function() {
        //sacamos el nombre del combo box
        var seleccion = document.getElementById('id_categoria_add')
        var categoria = seleccion.options[seleccion.selectedIndex].text;
        $.ajax({
            type: 'POST',
            url: '{{url("temporada")}}',
            data: {
                '_token': $('input[name=_token]').val(),
                'id_categoria': $('#id_categoria_add').val(),
                'gestion': $('#gestion_add').val(),
                'torneo': $('#torneo_add').val(),
                'fecha_inicio': $('#fecha_inicio_add').val(),
                'fecha_fin': $('#fecha_fin_add').val(),
                'num_equipos': $('#num_equipos').val(),
                'num_grupos': $('#num_grupos').val(),
            },
            success : function(data) {
                toastr.success(data.message, 'Success Alert', {timeOut: 5000});
                $('#temporada_table').prepend("\
                    <tr class='item" + data.id_temporada + "'>\
                        <td>" + categoria + "</td>\
                        <td>" + data.gestion + "</td>\
                        <td>" + data.torneo + "</td>\
                        <td>" + data.fecha_inicio + "</td>\
                        <td>" + data.fecha_fin + "</td>\
                        <td>\
                            <a href='#' class='btn btn-danger btn-sm' onclick=\"eliminar("+data.id_temporada+", '/temporada', event)\">\
                                <i class='fa fa-trash fa-lg'></i>\
                            </a>\
                            <a href='#' class='btn btn-primary btn-sm edit-modal-temporada' data-id="+data.id_temporada+">\
                                <i class='fa fa-pencil fa-lg'></i>\
                            </a>\
                            <a href='#' class='btn btn-success btn-sm show-fases' data-id="+data.id_temporada+">\
                                <i class='fa fa-edit fa-lg'> Fase</i>\
                            </a>\
                            <a href='/temporada/"+data.id_temporada+"' target='_blank' class='btn btn-success btn-sm'>\
                                <i class='fa fa-eye fa-lg'> Fixture</i>\
                            </a>\
                        </td>\
                    </tr>"
                );
            },
            error : function(xhr, status) {
                toastr.error('Disculpe, existio un problema!', 'Error Alert', {timeOut: 5000});
            },
        });
    });
</script>
<!-- script Edit a Temporada -->
<script>
    $(document).on('click', '.edit-modal-temporada', function() {
        $('#id_categoria_edit').val('');
        $('#gestion_edit').val('');
        $('#torneo_edit').val('');
        $('#fecha_inicio_edit').val('');
        $('#fecha_fin_edit').val('');
        $.get('/temporada/'+$(this).data('id')+'/edit', function(data){
            $.each(data.categorias,function(index,result){
                if (data.id_categoria == result.id_categoria) {
                    $('#id_categoria_edit').val(result.categoria);
                }
            });
            $('#gestion_edit').val(data.gestion);
            $('#torneo_edit').val(data.torneo);
            $('#fecha_inicio_edit').val(data.fecha_inicio);
            $('#fecha_fin_edit').val(data.fecha_fin);
            $('#id_temporada_edit').val(data.id_temporada);
        });
        $('#editModal_temporada').modal('show');
    });
    $('.modal-footer').on('click', '.edit-temporada', function() {
        $.ajax({
            type: 'PUT',
            url: '/temporada/' + $('#id_temporada_edit').val(),
            data: {
                '_token': $('input[name=_token]').val(),
                'id_categoria': 1,// el 1 es solo para pasar las validaciones
                'gestion': $('#gestion_edit').val(),
                'torneo': $('#torneo_edit').val(),
                'fecha_inicio': $('#fecha_inicio_edit').val(),
                'fecha_fin': $('#fecha_fin_edit').val(),
            },
            success : function(data) {
                toastr.success(data.message, 'Success Alert', {timeOut: 5000});
                $('.item' + data.id_temporada).replaceWith("\
                    <tr class='item" + data.id_temporada + "'>\
                        <td>" + $('#id_categoria_edit').val() + "</td>\
                        <td>" + data.gestion + "</td>\
                        <td>" + data.torneo + "</td>\
                        <td>" + data.fecha_inicio + "</td>\
                        <td>" + data.fecha_fin + "</td>\
                        <td>\
                            <a href='#' class='btn btn-danger btn-sm' onclick=\"eliminar("+data.id_temporada+", '/temporada', event)\">\
                                <i class='fa fa-trash fa-lg'></i>\
                            </a>\
                            <a href='#' class='btn btn-primary btn-sm edit-modal-temporada' data-id="+data.id_temporada+">\
                                <i class='fa fa-pencil fa-lg'></i>\
                            </a>\
                            <a href='#' class='btn btn-success btn-sm show-fases' data-id="+data.id_temporada+">\
                                <i class='fa fa-edit fa-lg'> Fase</i>\
                            </a>\
                            <a href='/temporada/"+data.id_temporada+"' target='_blank' class='btn btn-success btn-sm'>\
                                <i class='fa fa-eye fa-lg'> Fixture</i>\
                            </a>\
                        </td>\
                    </tr>"
                );
            },
            error : function(xhr, status) {
                toastr.error('Disculpe, existio un problema!', 'Error Alert', {timeOut: 5000});
            },
        });
    });
</script>
<!-- end script para gestionar TEMPORADA -->
<!-- script para mostrar la tabla grupos, dando click en la tabla temporada -->
<script>
    $(document).on('click', '.show-fases', function(e) {
        e.preventDefault();
        var cat = $(this).parents("tr").find("td")[0].innerHTML;
        var ges = $(this).parents("tr").find("td")[1].innerHTML;
        var tor = $(this).parents("tr").find("td")[2].innerHTML;

        $('#titulo_temporada').text('Categoria: ' + cat + ' - Torneo: ' + tor + ' - Gestion: ' + ges);

        $.ajax({
            type: 'POST',
            url: '/temporada/getFases',
            data: {
                '_token': $('input[name=_token]').val(),
                'id_temporada': $(this).data('id'),
            },
            success : function(data) {
                $('#div_fases').removeAttr('hidden');
                // BORRAMOS EL CONTENIDO DE LOS TABS
                $('#nav-pri-fase').empty();
                $('#nav-seg-fase').empty();
                $('#nav-final-fase').empty();

                $('#nav-pri-fase').append("\
                    <div class='p-3 mb-2 bg-danger text-center text-white'>\
                        <h1>"+data.fase_p.fase+"</h1>\
                    </div>\
                    ");
                $.each(data.grupos_p, function(g,grupo){
                    $('#nav-pri-fase').append("\
                        <div class='p-3 mb-2 bg-warning text-center text-dark'>\
                            <div class='row'>\
                                <div class='col-6 text-right'>\
                                    <h3>Nombre de Grupo:</h3>\
                                </div>\
                                <div class='col-3'>\
                                    <input type='text' class='form-control' onChange=\"modificar("+grupo.id_grupo+", '/temporada/updateGrupo', event)\" value='"+grupo.grupo+"' required>\
                                </div>\
                            </div>\
                        </div>\
                        <div class='row'>\
                            <div class='col-lg'>\
                                <table class='table table-bordered table-dark'>\
                                    <thead>\
                                        <tr>\
                                            <th class='text-center'>NOMBRE DEL CLUB</th>\
                                            <th class='text-center'>MODIFICAR</th>\
                                            <th class='text-center'>NOMBRE DEL CLUB</th>\
                                        </tr>\
                                    </thead>\
                                    <tbody id='tabla_grp_p"+g+"'>\
                                    </tbody>\
                                </table>\
                            </div>\
                        </div>"
                    );
                    
                    $.each(data.fechas_p, function(fe, fechas){
                        $.each(fechas, function(f, fecha){
                            if (fecha.id_grupo == grupo.id_grupo) {
                                $('#tabla_grp_p'+g).append("\
                                        <tr class='bg-info'>\
                                            <th colspan='3'>\
                                                <div class='row'>\
                                                    <div class='col-4 text-right'>\
                                                        <h3>Fecha "+(f+1)+":</h3>\
                                                    </div>\
                                                    <div class='col-8'>\
                                                        <input type='date' class='form-control' value='"+fecha.fecha+"' onChange=\"modificar("+fecha.id_fecha+", '/temporada/updateFecha', event)\">\
                                                    </div>\
                                                </div>\
                                            </th>\
                                        </tr>\
                                        ");
                                $.each(fecha.partidos, function(index, partido){
                                    $('#tabla_grp_p'+g).append("\
                                        <tr>\
                                            <td class='text-center'>"+partido.equipo1+"<br><img src='/img/"+partido.logo1+"' height='100'></td>\
                                            <td>\
                                                <div class='input-group mb-3'>\
                                                    <div class='input-group-prepend'>\
                                                        <span class='input-group-text'>Hora:</span>\
                                                    </div>\
                                                    <input type='time' class='form-control' value='"+((partido.hora==null)?"":partido.hora)+"' onChange=\"modificar("+partido.id_partido+", '/temporada/updateHora', event)\">\
                                                </div>\
                                                <div class='input-group mb-3'>\
                                                    <div class='input-group-prepend'>\
                                                        <label class='input-group-text'>Cancha:</label>\
                                                    </div>\
                                                    <select class='custom-select' id='combo_cancha"+partido.id_partido+"' onChange=\"modificar("+partido.id_partido+", '/temporada/updateCancha', event)\">\
                                                        <option value='' disabled selected>seleccione una opcion</option>\
                                                    </select>\
                                                </div>\
                                            </td>\
                                            <td class='text-center'>"+partido.equipo2+"<br><img src='/img/"+partido.logo2+"' height='100'></td>\
                                        </tr>\
                                        ");
                                    $.each(data.canchas, function(ind, cancha){
                                        $('#combo_cancha'+partido.id_partido).append('\
                                            <option value="'+cancha.id_cancha+'" '+((partido.id_cancha==cancha.id_cancha)?"selected":"")+'>'
                                                +cancha.nombre+
                                            '</option>');
                                    });
                                });
                            }
                        });
                    });
                });
                // VER SI EXISTE LA SEGUNDA FASE
                if (data.fase_s != null) {
                    $('#nav-seg-fase-tab').text(data.fase_s.fase);
                    $('#nav-seg-fase').append("\
                        <div class='p-3 mb-2 bg-danger text-center text-white'>\
                            <h1>"+data.fase_s.fase+"</h1>\
                        </div>\
                        ");
                    $.each(data.grupos_s, function(g,grupo){
                        $('#nav-seg-fase').append("\
                            <div class='p-3 mb-2 bg-warning text-center text-dark'>\
                                <div class='row'>\
                                    <div class='col-6 text-right'>\
                                        <h3>Nombre de Grupo:</h3>\
                                    </div>\
                                    <div class='col-3'>\
                                        <input type='text' class='form-control' onChange=\"modificar("+grupo.id_grupo+", '/temporada/updateGrupo', event)\" value='"+grupo.grupo+"' required>\
                                    </div>\
                                </div>\
                            </div>\
                            <div class='row'>\
                                <div class='col-lg'>\
                                    <table class='table table-bordered table-dark'>\
                                        <thead>\
                                            <tr>\
                                                <th class='text-center'>NOMBRE DEL CLUB</th>\
                                                <th class='text-center'>MODIFICAR</th>\
                                                <th class='text-center'>NOMBRE DEL CLUB</th>\
                                            </tr>\
                                        </thead>\
                                        <tbody id='tabla_grp_s"+g+"'>\
                                        </tbody>\
                                    </table>\
                                </div>\
                            </div>"
                        );
                        
                        $.each(data.fechas_s, function(fe, fechas){
                            $.each(fechas, function(f, fecha){
                                if (fecha.id_grupo == grupo.id_grupo) {
                                    $('#tabla_grp_s'+g).append("\
                                            <tr class='bg-info'>\
                                                <th colspan='3'>\
                                                    <div class='row'>\
                                                        <div class='col-4 text-right'>\
                                                            <h3>Fecha "+(f+1)+":</h3>\
                                                        </div>\
                                                        <div class='col-8'>\
                                                            <input type='date' class='form-control' value='"+fecha.fecha+"' onChange=\"modificar("+fecha.id_fecha+", '/temporada/updateFecha', event)\">\
                                                        </div>\
                                                    </div>\
                                                </th>\
                                            </tr>\
                                            ");
                                    $.each(fecha.partidos, function(index, partido){
                                        $('#tabla_grp_s'+g).append("\
                                            <tr>\
                                                <td class='text-center'>"+partido.equipo1+"<br><img src='/img/"+partido.logo1+"' height='100'></td>\
                                                <td>\
                                                    <div class='input-group mb-3'>\
                                                        <div class='input-group-prepend'>\
                                                            <span class='input-group-text'>Hora:</span>\
                                                        </div>\
                                                        <input type='time' class='form-control' value='"+((partido.hora==null)?"":partido.hora)+"' onChange=\"modificar("+partido.id_partido+", '/temporada/updateHora', event)\">\
                                                    </div>\
                                                    <div class='input-group mb-3'>\
                                                        <div class='input-group-prepend'>\
                                                            <label class='input-group-text'>Cancha:</label>\
                                                        </div>\
                                                        <select class='custom-select' id='combo_cancha"+partido.id_partido+"' onChange=\"modificar("+partido.id_partido+", '/temporada/updateCancha', event)\">\
                                                            <option value='' disabled selected>seleccione una opcion</option>\
                                                        </select>\
                                                    </div>\
                                                </td>\
                                                <td class='text-center'>"+partido.equipo2+"<br><img src='/img/"+partido.logo2+"' height='100'></td>\
                                            </tr>\
                                            ");
                                        $.each(data.canchas, function(ind, cancha){
                                            $('#combo_cancha'+partido.id_partido).append('\
                                                <option value="'+cancha.id_cancha+'" '+((partido.id_cancha==cancha.id_cancha)?"selected":"")+'>'
                                                    +cancha.nombre+
                                                '</option>');
                                        });
                                    });
                                }
                            });
                        });
                    });
                    // VER SI EXISTE LA FASE FINAL
                    if (data.fase_f != null) {
                        $('#nav-final-fase-tab').text(data.fase_f.fase);
                        $('#nav-final-fase').append("\
                            <div class='p-3 mb-2 bg-danger text-center text-white'>\
                                <h1>"+data.fase_f.fase+"</h1>\
                            </div>\
                            ");
                        $.each(data.grupos_f, function(g,grupo){
                            $('#nav-final-fase').append("\
                                <div class='p-3 mb-2 bg-warning text-center text-dark'>\
                                    <div class='row'>\
                                        <div class='col-6 text-right'>\
                                            <h3>Nombre de Grupo:</h3>\
                                        </div>\
                                        <div class='col-3'>\
                                            <input type='text' class='form-control' onChange=\"modificar("+grupo.id_grupo+", '/temporada/updateGrupo', event)\" value='"+grupo.grupo+"' required>\
                                        </div>\
                                    </div>\
                                </div>\
                                <div class='row'>\
                                    <div class='col-lg'>\
                                        <table class='table table-bordered table-dark'>\
                                            <thead>\
                                                <tr>\
                                                    <th class='text-center'>NOMBRE DEL CLUB</th>\
                                                    <th class='text-center'>MODIFICAR</th>\
                                                    <th class='text-center'>NOMBRE DEL CLUB</th>\
                                                </tr>\
                                            </thead>\
                                            <tbody id='tabla_grp_f"+g+"'>\
                                            </tbody>\
                                        </table>\
                                    </div>\
                                </div>"
                            );
                            
                            $.each(data.fechas_f, function(fe, fechas){
                                $.each(fechas, function(f, fecha){
                                    if (fecha.id_grupo == grupo.id_grupo) {
                                        $('#tabla_grp_f'+g).append("\
                                                <tr class='bg-info'>\
                                                    <th colspan='3'>\
                                                        <div class='row'>\
                                                            <div class='col-4 text-right'>\
                                                                <h3>Fecha "+(f+1)+":</h3>\
                                                            </div>\
                                                            <div class='col-8'>\
                                                                <input type='date' class='form-control' value='"+fecha.fecha+"' onChange=\"modificar("+fecha.id_fecha+", '/temporada/updateFecha', event)\">\
                                                            </div>\
                                                        </div>\
                                                    </th>\
                                                </tr>\
                                                ");
                                        $.each(fecha.partidos, function(index, partido){
                                            $('#tabla_grp_f'+g).append("\
                                                <tr>\
                                                    <td class='text-center'>"+partido.equipo1+"<br><img src='/img/"+partido.logo1+"' height='100'></td>\
                                                    <td>\
                                                        <div class='input-group mb-3'>\
                                                            <div class='input-group-prepend'>\
                                                                <span class='input-group-text'>Hora:</span>\
                                                            </div>\
                                                            <input type='time' class='form-control' value='"+((partido.hora==null)?"":partido.hora)+"' onChange=\"modificar("+partido.id_partido+", '/temporada/updateHora', event)\">\
                                                        </div>\
                                                        <div class='input-group mb-3'>\
                                                            <div class='input-group-prepend'>\
                                                                <label class='input-group-text'>Cancha:</label>\
                                                            </div>\
                                                            <select class='custom-select' id='combo_cancha"+partido.id_partido+"' onChange=\"modificar("+partido.id_partido+", '/temporada/updateCancha', event)\">\
                                                                <option value='' disabled selected>seleccione una opcion</option>\
                                                            </select>\
                                                        </div>\
                                                    </td>\
                                                    <td class='text-center'>"+partido.equipo2+"<br><img src='/img/"+partido.logo2+"' height='100'></td>\
                                                </tr>\
                                                ");
                                            $.each(data.canchas, function(ind, cancha){
                                                $('#combo_cancha'+partido.id_partido).append('\
                                                    <option value="'+cancha.id_cancha+'" '+((partido.id_cancha==cancha.id_cancha)?"selected":"")+'>'
                                                        +cancha.nombre+
                                                    '</option>');
                                            });
                                        });
                                    }
                                });
                            });
                        });
                    }
                }
            },
            error : function(xhr, status) {
                toastr.error('Disculpe, existio un problema!', 'Error Alert', {timeOut: 5000});
            },
        });
    });
</script>

<!-- script para modificar -->
<script>
    function modificar(id, ruta, e){
        $.ajax({
            type: 'POST',
            url: ruta,
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id,
                'campo': e.srcElement.value,
            },
            success : function(data) {
                toastr.success(data.message, 'Success Alert', {timeOut: 1000});
            },
            error : function(xhr, status) {
                toastr.error('Disculpe, existio un problema!', 'Error Alert', {timeOut: 1000});
            },
        });
    }
</script>
<!-- script para eliminar un registro -->
<script>
    function eliminar(id, ruta, e){
        e.preventDefault();
        if (!confirm('¿Estas seguro de eliminar?')) {
            return false;
        }
        $.ajax({
            type: 'DELETE',
            url: ruta + '/' + id,
            data: {
                '_token': $('input[name=_token]').val(),
            },
            success: function(data) {
                toastr.success(data['message'], 'Success Alert', {timeOut: 5000});
                if (ruta != 'grupo') {$('.item' + id).remove();}
            },
            error : function(xhr, status) {
                toastr.error('Disculpe, existio un problema!', 'Error Alert', {timeOut: 5000});
            },
        });
    }
</script>
@endsection