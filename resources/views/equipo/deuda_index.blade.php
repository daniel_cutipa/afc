@extends('layouts.app')
@section('htmlheader_title')
Deuda
@endsection
@section('content')
<a href="/deuda/create" class="btn btn-primary" role="button">Nueva Sancion</a>
<hr>
<table class="table table-striped table-sm">
    <thead>
        <tr>
            <th>#</th>
            <th>Club</th>
            <th>Categoria</th>
            <th>Descripcion</th>
            <th>Fecha amonestado</th>
            <th>Monto</th>
            <th>acciones</th>
        </tr>
    </thead>
    <tbody>
    	@foreach($deudas as $key => $sancion)
        <tr data-id="{{ $sancion->id_deuda_equipo }}">
            <td>{{ $key + 1 }}</td>
            <td>{{ $sancion->club }}</td>
            <td>{{ $sancion->categoria }}</td>
            <td>{{ $sancion->descripcion }}</td>
            <td>{{ $sancion->fecha_amonestacion }}</td>
            <td>{{ $sancion->monto }}</td>
            <td>
            	<form action="/deuda/{{ $sancion->id_deuda_equipo }}" method="POST" onSubmit="if(!confirm('¿Estas seguro de eliminar?')){return false;}">
            		<input type="hidden" name="_method" value="DELETE">
            		<input type="hidden" name="_token" value="{{ csrf_token() }}">

            		<button type="submit" class="btn btn-danger btn-sm">
						<i class="fa fa-trash fa-lg"></i>
					</button>
            	</form>
            </td>
        </tr>
		@endforeach
    </tbody>
</table>

@endsection
