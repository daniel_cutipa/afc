@extends('layouts.app')
@section('htmlheader_title')
Equipo/Edit
@endsection
@section('content')
<h3>
    <a href="/equipo">Equipos</a>
    Modificar Equipo
</h3>
<form method="POST" action="/equipo/{{ $equipo->id_equipo }}">
    <input name="_method" type="hidden" value="PUT">
    {{ csrf_field() }}
    <div class="form-group {{ $errors->has('id_club') ? 'has-error' : '' }}">
        <label for="id_club">Club</label>
        
        <select id="id_club" class="form-control" name="id_club">
            @foreach($equipo->clubes as $club)
            <option value="{{ $club->id_club }}" {{ $club->id_club==$equipo->id_club?'selected':'' }}>{{ $club->nombre }}</option>
            @endforeach
        </select>
        @if ($errors->has('id_club'))
            <span class="help-block">
                <strong>{{ $errors->first('id_club') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('id_categoria') ? 'has-error' : '' }}">
        <label for="id_categoria">Categoria</label>
        
        <select id="id_categoria" class="form-control" name="id_categoria">
            @foreach($equipo->categorias as $cat)
            <option value="{{ $cat->id_categoria }}" {{ $cat->id_categoria==$equipo->id_categoria?'selected':'' }}>{{ $cat->categoria }}</option>
            @endforeach
        </select>
        @if ($errors->has('id_categoria'))
            <span class="help-block">
                <strong>{{ $errors->first('id_categoria') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('director_tecnico') ? 'has-error' : '' }}">
        <label for="director_tecnico" >Director Tecnico</label>
        <input id="director_tecnico" class="form-control" type="text" name="director_tecnico" value="{{ $equipo->director_tecnico }}" required>
        @if ($errors->has('director_tecnico'))
            <span class="help-block">
                <strong>{{ $errors->first('director_tecnico') }}</strong>
            </span>
        @endif
    </div>
    <button class="btn btn-primary" type="submit">
        Guardar<i class="fa fa-send"></i>
    </button>
</form>

@endsection