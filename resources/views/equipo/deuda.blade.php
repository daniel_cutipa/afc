@extends('layouts.app')
@section('htmlheader_title')
Deuda
@endsection
@section('content')
{{ csrf_field() }}
<div class="row">
    <div class="col-lg-12">
        <h1>Lista de Temporadas</h1>
        <table class="table table-striped table-sm" >
            <thead>
                <tr>
                    <th>Categoria</th>
                    <th>Gestion</th>
                    <th>Torneo</th>
                    <th>Fecha Inicio</th>
                    <th>Fecha Fin</th>
                    <th>acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($temporadas as $temporada)
                <tr data-id="{{ $temporada->id_temporada }}">
                    <td>{{ $temporada->categoria->categoria }}</td>
                    <td>{{ $temporada->gestion }}</td>
                    <td>{{ $temporada->torneo }}</td>
                    <td>{{ $temporada->fecha_inicio }}</td>
                    <td>{{ $temporada->fecha_fin }}</td>
                    <td>
                        <a href="#" class="btn btn-success btn-sm list_equipos">
                            <i class="fa fa-list-ul fa-lg"> Equipos</i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <h1>Lista de Equipos</h1>
    </div>
    <div class="col-lg-6">
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <table class="table table-striped table-sm" >
            <thead>
                <tr>
                    <th>#</th>
                    <th>Club</th>
                    <th>Director Tecnico</th>
                    <th>acciones</th>
                </tr>
            </thead>
            <tbody id="table_equipos">

            </tbody>
        </table>

    </div>
</div>
<!-- modal agregar sancion -->
<div id="modal_deuda" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Agregar Sancion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="descripcion">Descripcion:</label>
                            <textarea class="form-control" id="descripcion"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="fecha_amonestacion">Fecha Amonestacion:</label>
                            <input type="date" class="form-control" id="fecha_amonestacion">
                        </div>
                    </div>
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="monto">Monto:</label>
                            <input type="number" class="form-control" id="monto" min="0">
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="id_equipo_deuda">
            <div class="modal-footer">
                <button type="button" class="btn btn-success add_modal_deuda" data-dismiss="modal">
                    <i class="fa fa-check fa-lg"></i> Guardar
                </button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">
                    <i class="fa fa-remove fa-lg"></i> Cerrar
                </button>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<script>
    var id_temporada = null;

    $( ".list_equipos" ).click(function() {
        // var res = str.substr(1, 4);
        id_temporada = $($(this).parents("tr")).data('id');
        $.get('/deuda/getEquipos/' + id_temporada, function(data){
            $('#table_equipos').empty();
            $.each(data.equipos, function(index, equipo){
                $('#table_equipos').append("\
                    <tr data-id='"+equipo.id_equipo+"'>\
                        <td>"+(index+1)+"</td>\
                        <td>"+equipo.club+"</td>\
                        <td>"+equipo.dt+"</td>\
                        <td>\
                            <a href='#' class='btn btn-success btn-sm modal_deuda_add' data-toggle='tooltip' data-placement='top' title='Agregar Sancion'>\
                                <i class='fa fa-plus fa-lg'> Sancion</i>\
                            </a>\
                        </td>\
                    </tr>"
                );
            });
        });
    });
</script>
<script>
    $(document).on('click', '.modal_deuda_add', function(e) {
        e.preventDefault();
        $('#id_equipo_deuda').val($($(this).parents("tr")).data('id')),
        $('#descripcion').val('');
        $('#fecha_amonestacion').val('');
        $('#monto').val('');
        $('#modal_deuda').modal('show');
    });

    $('.modal-footer').on('click', '.add_modal_deuda', function() {
        // verificar que uno por lo menos este con campo lleno
        $.ajax({
            type: 'POST',
            url: '/deuda/deudaStore',
            data: {
                '_token': $('input[name=_token]').val(),
                'id_equipo': $('#id_equipo_deuda').val(),
                'descripcion': $('#descripcion').val(),
                'fecha_amonestacion': $('#fecha_amonestacion').val(),
                'monto': $('#monto').val(),
            },
            success : function(data) {
                toastr.success(data.message, 'Success Alert', {timeOut: 5000});
                location.href = '{{ url("/deuda") }}';
            },
            error : function(xhr, status) {
                toastr.error('Disculpe, existio un problema!', 'Error Alert', {timeOut: 5000});
            },
        });
                            
    });
</script>
@endsection
