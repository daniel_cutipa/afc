@extends('layouts.app')
@section('htmlheader_title')
Equipo/Create
@endsection
@section('content')
<h3>
    <a href="/equipo">Equipos</a>
    Nuevo Equipo
</h3>
<form method="POST" action="/equipo">
    {{ csrf_field() }}
    <div class="form-group {{ $errors->has('id_club') ? 'has-error' : '' }}">
        <label for="id_club">Club</label>
        <select id="id_club" class="form-control" name="id_club">
            <option value="" disabled selected>Elige una opcion...</option>
            @foreach($clubes as $club)
            <option value="{{ $club->id_club }}">{{ $club->nombre }}</option>
            @endforeach
        </select>
        @if ($errors->has('id_club'))
            <span class="help-block">
                <strong>{{ $errors->first('id_club') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('id_categoria') ? 'has-error' : '' }}">
        <label for="id_categoria">Categoria</label>
        <select id="id_categoria" class="form-control" name="id_categoria">
            <option value="" disabled selected>Elige una opcion...</option>
            @foreach($categorias as $categoria)
            <option value="{{ $categoria->id_categoria }}">{{ $categoria->categoria }}</option>
            @endforeach
        </select>
        @if ($errors->has('id_categoria'))
            <span class="help-block">
                <strong>{{ $errors->first('id_categoria') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('director_tecnico') ? 'has-error' : '' }}">
        <label for="director_tecnico" >Director Tecnico</label>
        <input id="director_tecnico" class="form-control" type="text" name="director_tecnico" required>
        @if ($errors->has('director_tecnico'))
            <span class="help-block">
                <strong>{{ $errors->first('director_tecnico') }}</strong>
            </span>
        @endif
    </div>
    <button class="btn btn-primary" type="submit">
        Guardar<i class="fa fa-send"></i>
    </button>
</form>
@endsection