@extends('layouts.app')
@section('htmlheader_title')
Deuda
@endsection
@section('content')
<a href="/equipo/create" class="btn btn-primary" role="button">Nuevo Equipo</a>
<hr>
<table class="table table-striped table-sm">
    <thead>
        <tr>
            <th>Nombre Club</th>
            <th>Categoria</th>
            <th>Director Tecnico</th>
          	<th>acciones</th>
        </tr>
    </thead>
    <tbody>
    	@foreach($equipos as $equipo)
        <tr>
            <td>{{ $equipo->club->nombre }}</td>
            <td>{{ $equipo->categoria->categoria }}</td>
            <td>{{ $equipo->director_tecnico }}</td>
            <td>
            	<form action="/equipo/{{ $equipo->id_equipo }}" method="POST">
            		<input type="hidden" name="_method" value="DELETE">
            		<input type="hidden" name="_token" value="{{ csrf_token() }}">
                    
            		<button type="submit" class="btn btn-danger btn-sm">
						<i class="fa fa-trash fa-lg"></i>
					</button>

					<a href="{{ url('/equipo/'.$equipo->id_equipo.'/edit') }}" class="btn btn-primary btn-sm">
                    	<i class="fa fa-pencil fa-lg"></i>
                	</a>
            	</form>
            </td>
        </tr>
		@endforeach
    </tbody>
</table>
@endsection