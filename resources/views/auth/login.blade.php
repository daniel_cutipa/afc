@section('htmlheader_title')
Login
@endsection
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('layouts.htmlheader')
    </head>
    <body>
        <div class="page login-page">
            <div class="container">
                <div class="form-outer text-center d-flex align-items-center">
                    <div class="form-inner">
                        <div class="logo text-uppercase">
                            <span>----------System</span>
                            <strong class="text-primary">AFC----------</strong>
                        </div>
                        <form id="login-form" method="post" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            <div class="form-group-material">
                                <input id="email" type="email" name="email" class="input-material" required autofocus>
                                <label for="email" class="label-material">Email</label>
                            </div>
                            <div class="form-group-material">
                                <input id="password" type="password" name="password" required class="input-material">
                                <label for="login-password" class="label-material">Password</label>
                            </div>
                            <button id="login" type="submit" class="btn btn-primary">Ingresar</button>
                        </form>
                    </div>
                    <div class="copyrights text-center">
                        <p>Design by <a href="#" class="external">Name</a></p>
                    </div>
                </div>
            </div>
        </div>
        <!-- Javascript files-->
        @include('layouts.scripts')
        
    </body>
</html>

