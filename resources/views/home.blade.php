@extends('layouts.app')
@section('htmlheader_title')
Home
@endsection
@section('content')
<h1>Lista de Clubes con Sanciones</h1>
<table class="table table-striped table-sm">
    <thead>
        <tr>
            <th>#</th>
            <th>Club</th>
            <th>Categoria</th>
            <th>Descripcion</th>
            <th>Fecha amonestado</th>
            <th>Monto</th>
            <th>acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($deudas as $key => $deuda)
        <tr data-id="{{ $deuda->id_deuda_equipo }}">
            <td>{{ $key + 1 }}</td>
            <td>{{ $deuda->club }}</td>
            <td>{{ $deuda->categoria }}</td>
            <td>{{ $deuda->descripcion }}</td>
            <td>{{ $deuda->fecha_amonestacion }}</td>
            <td>{{ $deuda->monto }}</td>
            <td>
                <form action="/deuda/{{ $deuda->id_deuda_equipo }}" method="POST" onSubmit="if(!confirm('¿Estas seguro de eliminar?')){return false;}">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <button type="submit" class="btn btn-danger btn-sm">
                        <i class="fa fa-trash fa-lg"></i>
                    </button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>


<h1>Lista de Jugadores</h1>
<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" id="nav-goles-tab" data-toggle="tab" href="#nav-goles" role="tab" aria-controls="nav-goles" aria-selected="true">
            Mejores Goleadores
        </a>
        <a class="nav-item nav-link" id="nav-amarillas-tab" data-toggle="tab" href="#nav-amarillas" role="tab" aria-controls="nav-amarillas" aria-selected="false">
            Jugadores con tarjeta Amarilla
        </a>
        <a class="nav-item nav-link" id="nav-rojas-tab" data-toggle="tab" href="#nav-rojas" role="tab" aria-controls="nav-rojas" aria-selected="false">
            Jugadores con tarjeta Roja
        </a>
        <a class="nav-item nav-link" id="nav-sanciones-tab" data-toggle="tab" href="#nav-sanciones" role="tab" aria-controls="nav-sanciones" aria-selected="true">
            Jugadores con Sanciones
        </a>
    </div>
</nav>
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-goles" role="tabpanel" aria-labelledby="nav-goles-tab">
        <h2>Lista de Jugadores Goleadores</h2>
        <table class="table table-striped table-sm" >
            <thead>
                <tr>
                    <th>Matricula</th>
                    <th>Jugador</th>
                    <th>Club</th>
                    <th>Goles</th>
                </tr>
            </thead>
            <tbody>
                @foreach($goleadores as $goleador)
                <tr>
                    <td>{{ $goleador->matricula }}</td>
                    <td>{{ $goleador->nombre }}</td>
                    <td>{{ $goleador->club }}</td>
                    <td>{{ $goleador->goles }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="tab-pane fade" id="nav-amarillas" role="tabpanel" aria-labelledby="nav-amarillas-tab">
        <h2>Lista de Jugadores con Amarillas</h2>
        <table class="table table-striped table-sm" >
            <thead>
                <tr>
                    <th>Matricula</th>
                    <th>Jugador</th>
                    <th>Club</th>
                    <th>
                        Amarillas
                        <div class='badge badge-warning'>&nbsp;</div>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($amarillas as $amarilla)
                <tr>
                    <td>{{ $amarilla->matricula }}</td>
                    <td>{{ $amarilla->nombre }}</td>
                    <td>{{ $amarilla->club }}</td>
                    <td>{{ $amarilla->amarilla }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="tab-pane fade" id="nav-rojas" role="tabpanel" aria-labelledby="nav-rojas-tab">
        <h2>Lista de Jugadores con Rojas</h2>
        <table class="table table-striped table-sm" >
            <thead>
                <tr>
                    <th>Matricula</th>
                    <th>Jugador</th>
                    <th>Club</th>
                    <th>
                        Rojas
                        <div class='badge badge-danger'>&nbsp;</div>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($rojas as $roja)
                <tr>
                    <td>{{ $roja->matricula }}</td>
                    <td>{{ $roja->nombre }}</td>
                    <td>{{ $roja->club }}</td>
                    <td>{{ $roja->roja }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="tab-pane fade" id="nav-sanciones" role="tabpanel" aria-labelledby="nav-sanciones-tab">
        <h2>Lista de Jugadores con Sanciones</h2>
        <table class="table table-striped table-sm">
            <thead>
                <tr>
                    <th>Jugador</th>
                    <th>Descripcion</th>
                    <th>Fecha Inicio</th>
                    <th>Fecha Fin</th>
                    <th>Sancion Economica</th>
                    <th>Numero Partidos</th>
                    <th>acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($sanciones as $sancion)
                <tr data-id="{{ $sancion->id_sancion }}">
                    <td>{{ $sancion->jugador->ap_paterno . ' ' . $sancion->jugador->ap_materno . ' ' . $sancion->jugador->nombre}}</td>
                    <td>{{ $sancion->descripcion }}</td>
                    <td>{{ $sancion->fecha_inicio }}</td>
                    <td>{{ $sancion->fecha_fin }}</td>
                    <td>{{ $sancion->sancion_economica }}</td>
                    <td>{{ $sancion->numero_partidos }}</td>
                    <td>
                        <form action="/sancion/{{ $sancion->id_sancion }}" method="POST" onSubmit="if(!confirm('¿Estas seguro de eliminar?')){return false;}">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <button type="submit" class="btn btn-danger btn-sm">
                                <i class="fa fa-trash fa-lg"></i>
                            </button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection