<nav class="side-navbar">
    <div class="side-navbar-wrapper">
        <!-- Sidebar Header    -->
        <div class="sidenav-header d-flex align-items-center justify-content-center">
            <!-- User Info-->
            <div class="sidenav-header-inner text-center">
                <img src="/img/user.png" alt="person" class="img-fluid rounded-circle">
                <h2 class="h5">{{ auth()->user()->name }}</h2>
                <span>{{ auth()->user()->rol==1?'Administrador':(auth()->user()->rol==2?'Planillero':'Secretaria') }}</span>
            </div>
            <!-- Small Brand information, appears on minimized sidebar-->
            <div class="sidenav-header-logo">
                <a href="index.html" class="brand-small text-center"> 
                    <strong>B</strong>
                    <strong class="text-primary">D</strong>
                </a>
            </div>
        </div>
        <!-- Sidebar Navigation Menus-->
        <div class="main-menu">
            <h5 class="sidenav-heading">Main</h5>
            <ul id="side-main-menu" class="side-menu list-unstyled">
                @can('admin')
                <li>
                    <a href="/"> 
                        <i class="fa fa-home"></i>Home                             
                    </a>
                </li>
                <li>
                    <a href="/division"> 
                        <i class="fa fa-tags"></i>Division                             
                    </a>
                </li>
                <li>
                    <a href="/categoria"> 
                        <i class="fa fa-tags"></i>Categoria                             
                    </a>
                </li>
                @endcan
                @if(auth()->user()->rol == 1 || auth()->user()->rol == 2)
                <li>
                    <a href="/temporada"> 
                        <i class="fa fa-tags"></i>Temporada                             
                    </a>
                </li>
                @endif
                @can('admin')
                <li>
                    <a href="/club"> 
                        <i class="fa fa-shield"></i>Clubes                            
                    </a>
                </li>
                <li>
                    <a href="/equipo"> 
                        <i class="fa fa-shield"></i>Equipos                            
                    </a>
                </li>
                <li>
                    <a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> 
                        <i class="fa fa-stop"></i>Sanciones 
                    </a>
                    <ul id="exampledropdownDropdown" class="collapse list-unstyled ">
                        <li>
                            <a href="/sancion"> 
                                <i class="fa fa-exclamation-triangle"></i>Jugadores                             
                            </a>
                        </li>
                        <li>
                            <a href="/deuda"> 
                                <i class="fa fa-exclamation-triangle"></i>Equipos                             
                            </a>
                        </li>
                    </ul>
                </li>
                @endcan
            </ul>
        </div>
        <div class="admin-menu">
            <h5 class="sidenav-heading">Second menu</h5>
            <ul id="side-admin-menu" class="side-menu list-unstyled">
                @can('admin')
                <li> 
                    <a href="/cancha"> 
                        <i class="fa fa-delicious"> </i>Canchas
                    </a>
                </li>
                <li> 
                    <a href="/reporte"> 
                        <i class="fa fa-file"> </i>Reportes
                    </a>
                </li>
                <li> 
                    <a href="/graficos"> 
                        <i class="fa fa-bar-chart"> </i>Graficos
                    </a>
                </li>
                <li> 
                    <a href="/user"> 
                        <i class="fa fa-users"> </i>Usuarios
                    </a>
                </li>
                @endcan
            </ul>
        </div>
    </div>
</nav>