<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="description" content="">
<meta name="robots" content="all,follow">
<title> AFC - @yield('htmlheader_title', 'Your title here') </title>
<!-- Styles -->
<!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
<link href="{{ asset('css/toastr.min.css') }}" rel="stylesheet">
<!-- Bootstrap CSS-->
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<!-- Font Awesome CSS-->
<link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
<!-- Google fonts - Roboto -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
<!-- jQuery Circle-->
<link rel="stylesheet" href="{{ asset('css/grasp_mobile_progress_circle-1.0.0.min.css') }}">
<!-- Custom Scrollbar-->
<link rel="stylesheet" href="{{ asset('css/jquery.mCustomScrollbar.css') }}">
<!-- theme stylesheet-->
<!-- <link rel="stylesheet" href="{{ asset('css/style.default.css') }}" id="theme-stylesheet"> -->
<link rel="stylesheet" href="{{ asset('css/style.blue.css') }}" id="theme-stylesheet">
<!-- Custom stylesheet - for your changes-->
<link rel="stylesheet" href="{{ asset('css/custom.css') }}">
<!-- Favicon-->
<link rel="shortcut icon" href="/img/favicon.ico">