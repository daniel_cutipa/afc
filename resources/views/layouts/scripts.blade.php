<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"> </script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/grasp_mobile_progress_circle-1.0.0.min.js') }}"></script>
<script src="{{ asset('js/jquery.cookie.js') }}"> </script>
<!-- <script src="vendor/chart.js/Chart.min.js"></script> -->
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/jquery.mCustomScrollbar.concat.min.js') }}"></script>

<script src="{{ asset('js/toastr.min.js') }}"></script>
<!-- Main File-->
<script src="{{ asset('js/front.js') }}"></script>
<script>
$(function () {
	$('[data-toggle="tooltip"]').tooltip()
})
</script>
@yield('script')