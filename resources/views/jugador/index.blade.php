@extends('layouts.app')
@section('htmlheader_title')
Jugadores
@endsection
@section('content')
<a href="/jugador/create/{{$id_club}}" class="btn btn-primary" role="button">Nuevo Jugador</a>
<hr>
<p>{{$jugadores->total()}} registros | pagina {{$jugadores->currentPage()}} de {{$jugadores->lastPage()}}</p>
<table class="table table-striped table-sm">
    <thead>
        <tr>
            <th>Nombre completo</th>
            <th>Matricula</th>
            <th>Fecha de Nacimiento</th>
            <th>Direccion</th>
            <th>Telefono</th>
            <th>Celular</th>
            <th>Tipo de Jugador</th>
            <th>Nacionalidad</th>
          	<th>acciones</th>
        </tr>
    </thead>
    <tbody>
    	@foreach($jugadores as $jugador)
        <tr>
            <td>{{ $jugador->nombre . ' '. $jugador->ap_paterno . ' ' . $jugador->ap_materno }}</td>
            <td>{{ $jugador->matricula }}</td>
            <td>{{ $jugador->fecha_nacimiento }}</td>
            <td>{{ $jugador->direccion }}</td>
            <td>{{ $jugador->telefono }}</td>
            <td>{{ $jugador->celular }}</td>
            <td>{{ $jugador->tipo_jugador == 1? 'aficionado':'no aficionado' }}</td>
            <td>{{ $jugador->nacionalidad == 1? 'boliviano':'extranjero' }}</td>
            <td>
            	<form action="/jugador/{{ $jugador->id_jugador }}" method="POST">
            		<input type="hidden" name="_method" value="DELETE">
            		<input type="hidden" name="_token" value="{{ csrf_token() }}">
                    
            		<button type="submit" class="btn btn-danger btn-sm">
						<i class="fa fa-trash fa-lg"></i>
					</button>

					<a href="{{ url('/jugador/'.$jugador->id_jugador.'/edit') }}" class="btn btn-primary btn-sm">
                    	<i class="fa fa-pencil fa-lg"></i>
                	</a>
            	</form>
            </td>
        </tr>
		@endforeach
    </tbody>
</table>
{!! $jugadores->render() !!}
@endsection