@extends('layouts.app')
@section('htmlheader_title')
Jugadores/Create
@endsection
@section('content')

<h3>
    <a href="/club/jugador/{{$id_club}}">Club</a>
    Nuevo Jugador
</h3>
<form method="POST" action="/jugador">
    {{ csrf_field() }}
    <input type="hidden" name='id_club' value='{{$id_club}}'>
    
    <div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
        <label for="nombre" >Nombre de Jugador</label>
        <input id="nombre" class="form-control" type="text" name="nombre" required autofocus>
        @if ($errors->has('nombre'))
            <span class="help-block">
                <strong>{{ $errors->first('nombre') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('ap_paterno') ? 'has-error' : '' }}">
        <label for="ap_paterno" >Apellido Paterno</label>
        <input id="ap_paterno" class="form-control" type="text" name="ap_paterno" required autofocus>
        @if ($errors->has('ap_paterno'))
            <span class="help-block">
                <strong>{{ $errors->first('ap_paterno') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('ap_materno') ? 'has-error' : '' }}">
        <label for="ap_materno" >Apellido Materno</label>
        <input id="ap_materno" class="form-control" type="text" name="ap_materno" required autofocus>
        @if ($errors->has('ap_materno'))
            <span class="help-block">
                <strong>{{ $errors->first('ap_materno') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('matricula') ? 'has-error' : '' }}">
        <label for="matricula" >Matricula</label>
        <input id="matricula" class="form-control" type="integer" name="matricula" required autofocus>
        @if ($errors->has('matricula'))
            <span class="help-block">
                <strong>{{ $errors->first('matricula') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('fecha_nacimiento') ? 'has-error' : '' }}">
        <label for="fecha_nacimiento" >Fecha de Nacimiento</label>
        <input id="fecha_nacimiento" class="form-control" type="date" name="fecha_nacimiento" required>
        @if ($errors->has('fecha_nacimiento'))
            <span class="help-block">
                <strong>{{ $errors->first('fecha_nacimiento') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('sexo') ? 'has-error' : '' }}">
        <label for="sexo">Sexo</label>
        <select id="sexo" class="form-control" name="sexo">
            <option value="" disabled selected>Elige una opcion...</option>

            <option value="1">Masculino</option>
            <option value="0">Femenino</option>

        </select>
        @if ($errors->has('sexo'))
            <span class="help-block">
                <strong>{{ $errors->first('sexo') }}</strong>
            </span>
        @endif
    </div>


    <div class="form-group {{ $errors->has('nacionalidad') ? 'has-error' : '' }}">
        <label for="nacionalidad">nacionalidad</label>
        <select id="nacionalidad" class="form-control" name="nacionalidad">
            <option value="" disabled selected>Elige nacionalidad...</option>

            <option value="1">Nacional</option>
            <option value="0">Extranjero</option>

        </select>
        @if ($errors->has('nacionalidad'))
            <span class="help-block">
                <strong>{{ $errors->first('nacionalidad') }}</strong>
            </span>
        @endif
    </div>

  

    <div class="form-group {{ $errors->has('direccion') ? 'has-error' : '' }}">
        <label for="direccion" >Direccion</label>
        <input id="direccion" class="form-control" type="text" name="direccion" required autofocus>
        @if ($errors->has('direccion'))
            <span class="help-block">
                <strong>{{ $errors->first('direccion') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('telefono') ? 'has-error' : '' }}">
        <label for="telefono" >Telefono</label>
        <input id="telefono" class="form-control" type="text" name="telefono" required autofocus>
        @if ($errors->has('telefono'))
            <span class="help-block">
                <strong>{{ $errors->first('telefono') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('celular') ? 'has-error' : '' }}">
        <label for="celular" >Celular</label>
        <input id="celular" class="form-control" type="text" name="celular" required autofocus>
        @if ($errors->has('celular'))
            <span class="help-block">
                <strong>{{ $errors->first('celular') }}</strong>
            </span>
        @endif
    </div>
    
    <div class="form-group {{ $errors->has('tipo_jugador') ? 'has-error' : '' }}">
        <label for="tipo_jugador">Tipo de jugador</label>
        <select id="tipo_jugador" class="form-control" name="tipo_jugador">
            <option value="" disabled selected>Tipo de jugador...</option>

            <option value="1">Aficionado</option>
            <option value="0">No Aficionado</option>

        </select>
        @if ($errors->has('tipo_jugador'))
            <span class="help-block">
                <strong>{{ $errors->first('tipo_jugador') }}</strong>
            </span>
        @endif
    </div>
    
    <button class="btn btn-primary" type="submit">
        Guardar<i class="fa fa-send"></i>
    </button>
</form>
@endsection