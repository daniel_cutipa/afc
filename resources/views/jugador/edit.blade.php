@extends('layouts.app')
@section('htmlheader_title')
Jugadores/Edit
@endsection
@section('content')
<h3>
    <a href="/club/jugador/{{$jugador->id_club}}">Club</a>
    Modificar Jugador
</h3>
<form method="POST" action="/jugador/{{ $jugador->id_jugador }}">
    <input name="_method" type="hidden" value="PUT">
    {{ csrf_field() }}

    <div class="form-group {{ $errors->has('id_jugador') ? 'has-error' : '' }}">
          <label for="id_club">Nombre Equipo</label>

          <select id="id_club" class="form-control" name="id_equipo">
              @foreach($clubs as $club)
              <option value="{{ $club->id_club }}" {{ $club->id_club==$jugador->id_club?'selected':'' }}>{{ $club->club . ' '. $club ->categoria }}</option>
              @endforeach
          </select>
          @if ($errors->has('id_club'))
              <span class="help-block">
                  <strong>{{ $errors->first('id_club') }}</strong>
              </span>
          @endif
      </div>
	
    <div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
        <label for="nombre" >Nombre de Jugador</label>
        <input id="nombre" class="form-control" type="text" name="nombre" value="{{ $jugador->nombre }}" required autofocus>
        @if ($errors->has('nombre'))
            <span class="help-block">
                <strong>{{ $errors->first('nombre') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('ap_paterno') ? 'has-error' : '' }}">
        <label for="ap_paterno" >Apellido Paterno</label>
        <input id="ap_paterno" class="form-control" type="text" name="ap_paterno" value="{{ $jugador->ap_paterno }}" required autofocus>
        @if ($errors->has('ap_paterno'))
            <span class="help-block">
                <strong>{{ $errors->first('ap_paterno') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('ap_materno') ? 'has-error' : '' }}">
        <label for="ap_materno" >Apellido Materno</label>
        <input id="ap_materno" class="form-control" type="text" name="ap_materno" value="{{ $jugador->ap_materno }}" required autofocus>
        @if ($errors->has('ap_materno'))
            <span class="help-block">
                <strong>{{ $errors->first('ap_materno') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('direccion') ? 'has-error' : '' }}">
        <label for="direccion" >Direccion</label>
        <input id="direccion" class="form-control" type="text" name="direccion" value="{{ $jugador->direccion }}" required autofocus>
        @if ($errors->has('direccion'))
            <span class="help-block">
                <strong>{{ $errors->first('direccion') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('telefono') ? 'has-error' : '' }}">
        <label for="telefono" >Telefono</label>
        <input id="telefono" class="form-control" type="text" name="telefono" value="{{ $jugador->telefono }}" required autofocus>
        @if ($errors->has('telefono'))
            <span class="help-block">
                <strong>{{ $errors->first('telefono') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('celular') ? 'has-error' : '' }}">
        <label for="celular" >Celular</label>
        <input id="celular" class="form-control" type="text" name="celular" value="{{ $jugador->celular }}" required autofocus>
        @if ($errors->has('celular'))
            <span class="help-block">
                <strong>{{ $errors->first('celular') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('nacionalidad') ? 'has-error' : '' }}">
        <label for="nacionalidad">Nacionalidad</label>
        <select id="nacionalidad" class="form-control" name="nacionalidad">
            <?php
                // $valorNacional = ($jugador->nacionalidad = 1)?'Nacional':'Extranjero'
                $valorNacional = 'Nacional';
                if ($jugador->nacionalidad == 0){
                    $valorNacional = 'Extranjero';
                }
            ?>
            <option value="{{ $jugador->nacionalidad }}" selected> {{ $valorNacional }}  </option>

            <option value="1">Nacional</option>
            <option value="0">Extranjero</option>

        </select>
        @if ($errors->has('nacionalidad'))
            <span class="help-block">
                <strong>{{ $errors->first('nacionalidad') }}</strong>
            </span>
        @endif
    </div>

        <div class="form-group {{ $errors->has('tipo_jugador') ? 'has-error' : '' }}">
        <label for="tipo_jugador">Tipo de jugador</label>
        <select id="tipo_jugador" class="form-control" name="tipo_jugador">
            <?php
                // $valorNacional = ($jugador->nacionalidad = 1)?'Nacional':'Extranjero'
                $valortipo = 'Aficionado';
                if ($jugador->tipo_jugador == 0){
                    $valortipo = 'No aficionado';
                }
            ?>
            <option value="{{ $jugador->tipo_jugador }}" selected> {{ $valortipo }}  </option>

            <option value="1">Aficionado</option>
            <option value="0">No aficionado</option>

        </select>
        @if ($errors->has('tipo_jugador'))
            <span class="help-block">
                <strong>{{ $errors->first('tipo_jugador') }}</strong>
            </span>
        @endif
    </div>


    
    <button class="btn btn-primary" type="submit">
        Guardar<i class="fa fa-send"></i>
    </button>
</form>

@endsection