@extends('layouts.app')
@section('htmlheader_title')
Reporte
@endsection
@section('content')
{{ csrf_field() }}
<div class="row">
    <div class="col-lg-12">
        <h1>Lista de Temporadas</h1>
        <table class="table table-striped table-sm" >
            <thead>
                <tr>
                    <th>Categoria</th>
                    <th>Gestion</th>
                    <th>Torneo</th>
                    <th>Fecha Inicio</th>
                    <th>Fecha Fin</th>
                    <th>acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($temporadas as $temporada)
                <tr data-id="{{ $temporada->id_temporada }}">
                    <td>{{ $temporada->categoria->categoria }}</td>
                    <td>{{ $temporada->gestion }}</td>
                    <td>{{ $temporada->torneo }}</td>
                    <td>{{ $temporada->fecha_inicio }}</td>
                    <td>{{ $temporada->fecha_fin }}</td>
                    <td>
                        <a href="#" class="btn btn-success btn-sm list_jugadores">
                            <i class="fa fa-list-ul fa-lg"> Jugadores</i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" id="nav-goles-tab" data-toggle="tab" href="#nav-goles" role="tab" aria-controls="nav-goles" aria-selected="true">
            Mejores Goleadores
        </a>
        <a class="nav-item nav-link" id="nav-amarillas-tab" data-toggle="tab" href="#nav-amarillas" role="tab" aria-controls="nav-amarillas" aria-selected="false">
            Jugadores con tarjeta Amarilla
        </a>
        <a class="nav-item nav-link" id="nav-rojas-tab" data-toggle="tab" href="#nav-rojas" role="tab" aria-controls="nav-rojas" aria-selected="false">
            Jugadores con tarjeta Roja
        </a>
    </div>
</nav>
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-goles" role="tabpanel" aria-labelledby="nav-goles-tab">
        <table class="table table-striped table-sm" >
            <thead>
                <tr>
                    <th>Matricula</th>
                    <th>Jugador</th>
                    <th>Club</th>
                    <th>Goles</th>
                </tr>
            </thead>
            <tbody id="table_goleadores">

            </tbody>
        </table>
    </div>
    <div class="tab-pane fade" id="nav-amarillas" role="tabpanel" aria-labelledby="nav-amarillas-tab">
        <table class="table table-striped table-sm" >
            <thead>
                <tr>
                    <th>Matricula</th>
                    <th>Jugador</th>
                    <th>Club</th>
                    <th>
                        Amarillas
                        <div class='badge badge-warning'>&nbsp;</div>
                    </th>
                </tr>
            </thead>
            <tbody id="table_amarillas">

            </tbody>
        </table>
    </div>
    <div class="tab-pane fade" id="nav-rojas" role="tabpanel" aria-labelledby="nav-rojas-tab">
        <table class="table table-striped table-sm" >
            <thead>
                <tr>
                    <th>Matricula</th>
                    <th>Jugador</th>
                    <th>Club</th>
                    <th>
                        Rojas
                        <div class='badge badge-danger'>&nbsp;</div>
                    </th>
                </tr>
            </thead>
            <tbody id="table_rojas">

            </tbody>
        </table>
    </div>
</div>
@endsection
@section('script')
<script>
    var id_temporada = null;
    $( ".list_jugadores" ).click(function() {
        // var res = str.substr(1, 4);
        id_temporada = $($(this).parents("tr")).data('id');
        $.get('/reporte/'+id_temporada, function(data){
            $('#table_goleadores').empty();
            $('#table_amarillas').empty();
            $('#table_rojas').empty();
            $.each(data.goleadores, function(index, jugador){
                $('#table_goleadores').append("\
                    <tr>\
                        <td>"+jugador.matricula+"</td>\
                        <td>"+jugador.nombre+"</td>\
                        <td>"+jugador.club+"</td>\
                        <td>"+jugador.goles+"</td>\
                    </tr>"
                );
            });
            $.each(data.amarillas, function(index, jugador){
                $('#table_amarillas').append("\
                    <tr>\
                        <td>"+jugador.matricula+"</td>\
                        <td>"+jugador.nombre+"</td>\
                        <td>"+jugador.club+"</td>\
                        <td>"+jugador.amarilla+"</td>\
                    </tr>"
                );
            });
            $.each(data.rojas, function(index, jugador){
                $('#table_rojas').append("\
                    <tr>\
                        <td>"+jugador.matricula+"</td>\
                        <td>"+jugador.nombre+"</td>\
                        <td>"+jugador.club+"</td>\
                        <td>"+jugador.roja+"</td>\
                    </tr>"
                );
            });
        });
    });
</script>
@endsection
