@extends('layouts.app')
@section('htmlheader_title')
graficos
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1>Lista de Temporadas</h1>
        <table class="table table-striped table-sm" >
            <thead>
                <tr>
                    <th>Categoria</th>
                    <th>Gestion</th>
                    <th>Torneo</th>
                    <th>Fecha Inicio</th>
                    <th>Fecha Fin</th>
                    <th>acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($temporadas as $temporada)
                <tr data-id="{{ $temporada->id_temporada }}">
                    <td>{{ $temporada->categoria->categoria }}</td>
                    <td>{{ $temporada->gestion }}</td>
                    <td>{{ $temporada->torneo }}</td>
                    <td>{{ $temporada->fecha_inicio }}</td>
                    <td>{{ $temporada->fecha_fin }}</td>
                    <td>
                        <a href="#" class="btn btn-success btn-sm charts">
                            <i class="fa fa-bar-chart fa-lg"> Graficos</i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div id="divChart" hidden="">
    <div class="row">
        <div class="col-sm form-inline">
            <div class="i-checks">
                <input id="radio-line" type="radio" checked="" value="line" name="radio" class="form-control-custom radio-custom">
                <label for="radio-line">Line</label>
            </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <div class="i-checks">
                <input id="radio-bar" type="radio" value="bar" name="radio" class="form-control-custom radio-custom">
                <label for="radio-bar">Bar</label>
            </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <div class="i-checks">
                <input id="radio-doughnut" type="radio" value="doughnut" name="radio" class="form-control-custom radio-custom">
                <label for="radio-doughnut">Doughnut</label>
            </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <div class="i-checks">
                <input id="radio-pie" type="radio" value="pie" name="radio" class="form-control-custom radio-custom">
                <label for="radio-pie">Pie</label>
            </div>
        </div>
    </div>

    <a href="#delantera"><h2 id="delantera">Clubes con Mejor y Peor Delantera</h2></a>
    <div class="row">
        <div class="col-sm-7 div-canvas" id="div-mejorDelantera" style="width:30%"></div>
        <div class="col-sm-5">
            <table class="table table-striped table-sm" >
                <thead>
                    <tr>
                        <th>Club</th>
                        <th>Goles</th>
                    </tr>
                </thead>
                <tbody id="table-mejorDelantera"></tbody>
            </table>
        </div>
    </div>
    <a href="#defensa"><h2 id="defensa">Clubes con Peor y Mejor Defensa</h2></a>
    <div class="row">
        <div class="col-sm-7 div-canvas" id="div-mejorDefensa" style="width:40%"></div>
        <div class="col-sm-5">
            <table class="table table-striped table-sm" >
                <thead>
                    <tr>
                        <th>Club</th>
                        <th>Goles</th>
                    </tr>
                </thead>
                <tbody id="table-mejorDefensa"></tbody>
            </table>
        </div>
    </div>
    <a href="#amarilla"><h2 id="amarilla">Clubes con Mas y Menos Amarillas</h2></a>
    <div class="row">
        <div class="col-sm-7 div-canvas" id="div-masAmarillas" style="width:40%"></div>
        <div class="col-sm-5">
            <table class="table table-striped table-sm" >
                <thead>
                    <tr>
                        <th>Club</th>
                        <th>Tarjeta</th>
                    </tr>
                </thead>
                <tbody id="table-masAmarillas"></tbody>
            </table>
        </div>
    </div>
    <a href="#roja"><h2 id="roja">Clubes con Mas y Menos Rojas</h2></a>
    <div class="row">
        <div class="col-sm-7 div-canvas" id="div-masRojas" style="width:40%"></div>
        <div class="col-sm-5">
            <table class="table table-striped table-sm" >
                <thead>
                    <tr>
                        <th>Club</th>
                        <th>Tarjeta</th>
                    </tr>
                </thead>
                <tbody id="table-masRojas"></tbody>
            </table>
        </div>
    </div>
</div>


@endsection
@section('script')
<script>
    var colores = [
        [54, 162, 235],[255, 99, 132],[255, 206, 86],[75, 192, 192],[153, 102, 255],
        [255, 159, 64],[92, 163, 99],[20, 240, 120],[220, 240, 60],[40, 240, 60],
        [255, 99, 132],[54, 162, 235],[255, 206, 86],[75, 192, 192],[153, 102, 255],
        [255, 159, 64],[92, 163, 99],[20, 240, 120],[220, 240, 60],[40, 240, 60],
    ];
    var chart_type = 'line';
    var nombres = [];
    var golesF = [];
    var golesC = [];
    var amarillas = [];
    var rojas = [];
    var bgcolor = [];
    var bcolor = [];
    var id_temporada = null;
    $(document).on('click', '.charts', function(e) {
        e.preventDefault();
        id_temporada = $($(this).parents("tr")).data('id');
        $('#divChart').removeAttr('hidden');
        cargarDatos();
    });
</script>
<script src="{{ asset('/js/Chart.bundle.js') }}"></script>
<script src="{{ asset('/js/Chart.js') }}"></script>
<script>
    $('.radio-custom').change(function() {
        limpiar();
        switch($(this).val()){
            case 'line':
                chart_type = 'line';
                cargarDatos();
                break;
            case 'bar':
                chart_type = 'bar';
                cargarDatos();
                break;
            case 'doughnut':
                chart_type = 'doughnut';
                cargarDatos();
                break;
            case 'pie':
                chart_type = 'pie';
                cargarDatos();
                break;
        }

    });
    function limpiar(){
        $('.div-canvas').empty();
        nombres = [];
        golesF = [];
        golesC = [];
        amarillas = [];
        rojas = [];
        bgcolor = [];
        bcolor = [];
        $('#table-mejorDelantera').empty();
        $('#table-mejorDefensa').empty();
        $('#table-masAmarillas').empty();
        $('#table-masRojas').empty();
        // var canvas_del = document.getElementById("mejorDelantera");
        // var context_del = canvas_del.getContext("2d");
        // context_del.clearRect(0 ,0, canvas_del.width, canvas_del.height);

        // var canvas_def = document.getElementById("mejorDefensa");
        // var context_def = canvas_def.getContext("2d");
        // context_def.clearRect(0 ,0, canvas_def.width, canvas_def.height);

        // var canvas_ama = document.getElementById("masAmarillas");
        // var context_ama = canvas_ama.getContext("2d");
        // context_ama.clearRect(0 ,0, canvas_ama.width, canvas_ama.height);

        // var canvas_roj = document.getElementById("masRojas");
        // var context_roj = canvas_roj.getContext("2d");
        // context_roj.clearRect(0 ,0, canvas_roj.width, canvas_roj.height);
    }
    function cargarDatos(){
        limpiar();
        $.get('/datosGraficos/'+id_temporada, function(data){
            $.each(data.list_equipos, function(index, equipo){
                nombres.push(equipo.nombre);
                golesF.push(equipo.gf);
                golesC.push(equipo.gc);
                amarillas.push(equipo.amarilla>0?equipo.amarilla:0);
                rojas.push(equipo.roja>0?equipo.roja:0);
                bgcolor.push('rgba('+colores[index]+', 0.2)');
                bcolor.push('rgba('+colores[index]+', 1)');
            });
            // ++++++++++++++++++++EQUIPOS CON MEJOR Y PEOR DELANTERA++++++++++++++++++++
            $('#div-mejorDelantera').append("<canvas id='mejorDelantera' width='400' height='400'>su navegador no admite canvas</canvas>");
            var ctx_delantera = document.getElementById("mejorDelantera").getContext('2d');
            var mejorDelantera = new Chart(ctx_delantera, {
                type: chart_type,
                data: {
                    labels: nombres,
                    datasets: [{
                        label: '# de Goles',
                        data: golesF,
                        backgroundColor: bgcolor,
                        borderColor: bcolor,
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });
            $.each(data.list_equipos, function(index, equipo){
                $('#table-mejorDelantera').append("\
                    <tr>\
                        <th>"+equipo.nombre+"</th>\
                        <th>"+equipo.gf+"</th>\
                    </tr>"
                    );
            });
            // ++++++++++++++++++++EQUIPOS CON MEJOR Y PEOR DEFENSA++++++++++++++++++++
            $('#div-mejorDefensa').append("<canvas id='mejorDefensa' width='400' height='400'>su navegador no admite canvas</canvas>");
            var ctx_defensa = document.getElementById("mejorDefensa").getContext('2d');
            var mejorDefensa = new Chart(ctx_defensa, {
                type: chart_type,
                data: {
                    labels: nombres,
                    datasets: [{
                        label: '# de Goles',
                        data: golesC,
                        backgroundColor: bgcolor,
                        borderColor: bcolor,
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });
            $.each(data.list_equipos, function(index, equipo){
                $('#table-mejorDefensa').append("\
                    <tr>\
                        <th>"+equipo.nombre+"</th>\
                        <th>"+equipo.gc+"</th>\
                    </tr>"
                    );
            });
            // ++++++++++++++++++++CLUBES CON MAS Y MENOS AMARILLAS++++++++++++++++++++
            $('#div-masAmarillas').append("<canvas id='masAmarillas' width='400' height='400'>su navegador no admite canvas</canvas>");
            var ctx_amarilla = document.getElementById("masAmarillas").getContext('2d');
            var masAmarillas = new Chart(ctx_amarilla, {
                type: chart_type,
                data: {
                    labels: nombres,
                    datasets: [{
                        label: '# de Tarjetas Amarillas',
                        data: amarillas,
                        backgroundColor: bgcolor,
                        borderColor: bcolor,
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });
            $.each(data.list_equipos, function(index, equipo){
                $('#table-masAmarillas').append("\
                    <tr>\
                        <th>"+equipo.nombre+"</th>\
                        <th>"+(equipo.amarilla>0?equipo.amarilla:0)+"</th>\
                    </tr>"
                    );
            });
            // ++++++++++++++++++++CLUBES CON MAS Y MENOS ROJAS++++++++++++++++++++
            $('#div-masRojas').append("<canvas id='masRojas' width='400' height='400'>su navegador no admite canvas</canvas>");
            var ctx_roja = document.getElementById("masRojas").getContext('2d');
            var masRojas = new Chart(ctx_roja, {
                type: chart_type,
                data: {
                    labels: nombres,
                    datasets: [{
                        label: '# de Tarjetas Rojas',
                        data: rojas,
                        backgroundColor: bgcolor,
                        borderColor: bcolor,
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });
            $.each(data.list_equipos, function(index, equipo){
                $('#table-masRojas').append("\
                    <tr>\
                        <th>"+equipo.nombre+"</th>\
                        <th>"+(equipo.roja>0?equipo.roja:0)+"</th>\
                    </tr>"
                    );
            });

        });
    }
</script>

@endsection
