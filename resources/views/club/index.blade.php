@extends('layouts.app')
@section('htmlheader_title')
Club
@endsection
@section('content')


<a href="/club/create" class="btn btn-primary" role="button">Nuevo Club</a>
<hr>
<table class="table table-striped table-sm">
   <thead>
       <tr>
           <th>Nombre Club</th>
             <th>Fecha</th>
           <th>Logo</th>
           <th>Nombre Presidente</th>
           <th>Tipo de club</th>
       </tr>
   </thead>
   <tbody>
       @foreach($clubes as $club)
       <tr>
           <td>{{ $club->nombre }}</td>
           <td>{{ $club->fecha_fundacion }}</td>
           <td>
            <img src="img/{{$club->logo}}" width="100">
           </td>
           <td>{{ $club->presidente }}</td>
           <td>{{ $club->tipo_club ==1? 'Aficionados':'No Aficionados' }}</td>
           <td>
               <form action="/club/{{ $club->id_club }}" method="POST">
                   <input type="hidden" name="_method" value="DELETE">
                   <input type="hidden" name="_token" value="{{ csrf_token() }}">

                   <button type="submit" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="right" title="eliminar">
                        <i class="fa fa-trash fa-lg"></i>
                    </button>

                    <a href="{{ url('/club/'.$club->id_club.'/edit') }}" class="btn btn-primary btn-sm" role="button" data-toggle="tooltip" data-placement="right" title="editar">
                       <i class="fa fa-pencil fa-lg"></i>
                   </a>
                   <a href="{{ url('/club/jugador/'.$club->id_club.'') }}" class="btn btn-success btn-sm" role="button" data-toggle="tooltip" data-placement="right" title="jugadores">
                       <i class="fa fa-user fa-lg"></i>
                   </a>
               </form>
           </td>
       </tr>
        @endforeach
   </tbody>
</table>
@endsection
