@extends('layouts.app')
@section('htmlheader_title')
Club/Edit
@endsection
@section('content')
<h3>
<a href="/club">Clubes</a>
Modificar Club
</h3>
<form method="POST" action="/club/{{ $club->id_club }}" enctype="multipart/form-data">
    <input name="_method" type="hidden" value="PUT">
    {{ csrf_field() }}
    
    <div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
        <label for="nombre" >Nombre de Club</label>
        <input id="nombre" class="form-control" type="text" name="nombre" value="{{ $club->nombre }}" required autofocus>
        @if ($errors->has('nombre'))
        <span class="help-block">
            <strong>{{ $errors->first('nombre') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('fecha_fundacion') ? 'has-error' : '' }}">
        <label for="fecha_fundacion" >Fecha de Fundacion </label>
        <input id="fecha_fundacion" class="form-control" type="date" name="fecha_fundacion" value="{{ $club->fecha_fundacion }}" required>
        @if ($errors->has('fecha_fundacion'))
        <span class="help-block">
            <strong>{{ $errors->first('fecha_fundacion') }}</strong>
        </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('logo') ? 'has-error' : '' }}">
        <label for="logo">Logo</label>
        <input id="logo" type="hidden" name="logo" value="{{ csrf_token() }}">
        <div class="form-group">
            <div class="col-md-6">
                <input type="file" class="form-control" name="logo" required>
                @if ($errors->has('logo'))
                <span class="help-block">
                    <strong>{{ $errors->first('logo') }}</strong>
                </span> @endif
            </div>
        </div>
    </div>
    <br>

    <div class="form-group {{ $errors->has('tipo_club') ? 'has-error' : '' }}">
        <label for="tipo_club">Tipo Club</label>
        <select id="tipo_club" class="form-control" name="tipo_club">
            <?php
                // $valorNacional = ($jugador->nacionalidad = 1)?'Nacional':'Extranjero'
                $valortipoclub = 'Aficionados';
                if ($club->tipo_club == 0){
                    $valortipoclub = 'No aficionados';
                }
            ?>
            <option value="{{ $club->tipo_club }}" selected> {{ $valortipoclub }}  </option>

            <option value="1">Aficionados</option>
            <option value="0">No aficionados</option>

        </select>
        @if ($errors->has('tipo_club'))
            <span class="help-block">
                <strong>{{ $errors->first('tipo_club') }}</strong>
            </span>
        @endif
    </div>
    
    <div class="form-group {{ $errors->has('presidente') ? 'has-error' : '' }}">
        <label for="presidente" >Nombre Presidente</label>
        <input id="presidente" class="form-control" type="text" name="presidente" value="{{ $club->presidente }}" required>
        @if ($errors->has('presidente'))
        <span class="help-block">
            <strong>{{ $errors->first('presidente') }}</strong>
        </span>
        @endif
    </div>
    
    
    <button class="btn btn-primary" type="submit">
    Guardar<i class="fa fa-send"></i>
    </button>
</form>
@endsection