@extends('layouts.app')
@section('htmlheader_title')
Club/Create
@endsection
@section('content')

<h3>
   <a href="/club">Clubes</a>
   Nuevo Club
</h3>
<form method="POST" action="/club" enctype="multipart/form-data">
 {{ csrf_field() }}

 <div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}" >
   <label for="nombre"> Club</label>
   <input id="nombre" class="form-control" type="text" name="nombre" required autofocus> @if ($errors->has('nombre'))
   <span class="help-block">
               <strong>{{ $errors->first('nombre') }}</strong>
           </span> @endif
 </div>

 <div class="form-group {{ $errors->has('categorias') ? 'has-error' : '' }}" >
   <label for="categorias"> Categorias (para seleccionar varios presionar ctrl)</label></br>
    <select id="prueba" name="categorias[]" multiple class="form-control">
        <optgroup value="Categorias">
            @foreach ($categorias as $categoria)
                <option value="{{ $categoria->id_categoria }}">{{ $categoria->categoria }}</option>
            @endforeach
        </optgroup>
    </select>
    
    @if ($errors->has('categorias'))
        <span class="help-block">
                <strong>{{ $errors->first('categorias') }}</strong>
        </span> 
    @endif
 </div>





 <div class="form-group {{ $errors->has('fecha_fundacion') ? 'has-error' : '' }}">
   <label for="fecha_fundacion">Fecha de Fundación</label>
   <input id="fecha_fundacion" class="form-control" type="date" name="fecha_fundacion" required> @if ($errors->has('fecha_fundacion'))
   <span class="help-block">
               <strong>{{ $errors->first('fecha_fundacion') }}</strong>
           </span> @endif
 </div>



 <div class="form-group {{ $errors->has('logo') ? 'has-error' : '' }}">
   <label for="logo">Logo</label>
     <input id="logo" type="hidden" name="logo" value="{{ csrf_token() }}">
       <div class="form-group">
         <div class="col-md-6">
           <input type="file" class="form-control" name="logo" required>
           @if ($errors->has('logo'))
           <span class="help-block">
               <strong>{{ $errors->first('logo') }}</strong>
           </span> @endif
         </div>
       </div>
 </div>

<div class="form-group {{ $errors->has('tipo_club') ? 'has-error' : '' }}">
        <label for="tipo_club">Tipo Club</label>
        <select id="tipo_club" class="form-control" name="tipo_club">
            <option value="" disabled selected>Elige una opcion...</option>

            <option value="1">Aficionados</option>
            <option value="0">No aficionados</option>

        </select>
        @if ($errors->has('tipo_club'))
            <span class="help-block">
                <strong>{{ $errors->first('tipo_club') }}</strong>
            </span>
        @endif
</div>
<br>
 <div class="form-group {{ $errors->has('presidente') ? 'has-error' : '' }}">
   <label for="presidente">Presidente</label>
   <input id="presidente" class="form-control" type="text" name="presidente" required> @if ($errors->has('presidente'))
   <span class="help-block">
               <strong>{{ $errors->first('presidente') }}</strong>
           </span> @endif
 </div>





 <button class="btn btn-primary" type="submit">
       Guardar<i class="fa fa-send"></i>
   </button>
</form>
@endsection