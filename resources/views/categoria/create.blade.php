@extends('layouts.app')
@section('htmlheader_title')
Categoria/Create
@endsection
@section('content')

<h3>
    <a href="/categoria">Categorias</a>
    Nueva Categoria
</h3>
<form method="POST" action="/categoria">
    {{ csrf_field() }}
    <div class="form-group {{ $errors->has('categoria') ? 'has-error' : '' }}">
        <label for="categoria" >Nombre Categoria</label>
        <input id="categoria" class="form-control" type="text" name="categoria" required autofocus>
        @if ($errors->has('categoria'))
            <span class="help-block">
                <strong>{{ $errors->first('categoria') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('id_division') ? 'has-error' : '' }}">
        <label for="id_division">Division</label>
        <select id="id_division" class="form-control" name="id_division">
            <option value="" disabled selected>Elige una opcion...</option>
            @foreach($divisiones as $division)
            <option value="{{ $division->id_division }}">{{ $division->division }}</option>
            @endforeach
        </select>
        @if ($errors->has('id_division'))
            <span class="help-block">
                <strong>{{ $errors->first('id_division') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('edad_min') ? 'has-error' : '' }}">
        <label for="edad_min" >Edad Minima</label>
        <input id="edad_min" class="form-control" type="text" name="edad_min">
        @if ($errors->has('edad_min'))
            <span class="help-block">
                <strong>{{ $errors->first('edad_min') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('edad_max') ? 'has-error' : '' }}">
        <label for="edad_max" >Edad Maxima</label>
        <input id="edad_max" class="form-control" type="text" name="edad_max">
        @if ($errors->has('edad_max'))
            <span class="help-block">
                <strong>{{ $errors->first('edad_max') }}</strong>
            </span>
        @endif
    </div>
    <button class="btn btn-primary" type="submit">
        Guardar<i class="fa fa-send"></i>
    </button>
</form>
@endsection