@extends('layouts.app')
@section('htmlheader_title')
Categoria
@endsection
@section('content')
<a href="/categoria/create" class="btn btn-primary" role="button">Nueva Categoria</a>
<hr>
<table class="table table-striped table-sm">
    <thead>
        <tr>
            <th>Nombre Categoria</th>
            <th>Division</th>
            <th>Edad Minima</th>
            <th>Edad Maxima</th>
          	<th>acciones</th>
        </tr>
    </thead>
    <tbody>
    	@foreach($categorias as $categoria)
        <tr>
            <td>{{ $categoria->categoria }}</td>
            <td>{{ $categoria->division->division }}</td>
            <td>{{ $categoria->edad_min == NULL? 'sin definir' : $categoria->edad_min.' años' }}</td>
            <td>{{ $categoria->edad_max == NULL? 'sin definir' : $categoria->edad_max.' años' }}</td>
            <td>
            	<form action="/categoria/{{ $categoria->id_categoria }}" method="POST">
            		<input type="hidden" name="_method" value="DELETE">
            		<input type="hidden" name="_token" value="{{ csrf_token() }}">
                    
            		<button type="submit" class="btn btn-danger btn-sm">
						<i class="fa fa-trash fa-lg"></i>
					</button>

					<a href="{{ url('/categoria/'.$categoria->id_categoria.'/edit') }}" class="btn btn-primary btn-sm">
                    	<i class="fa fa-pencil fa-lg"></i>
                	</a>
            	</form>
            </td>
        </tr>
		@endforeach
    </tbody>
</table>
@endsection