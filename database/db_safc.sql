-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-04-2018 a las 17:26:29
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_safc`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `arbitro`
--

CREATE TABLE `arbitro` (
  `id_arbitro` int(11) NOT NULL,
  `id_partido` int(11) NOT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  `informe` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cancha`
--

CREATE TABLE `cancha` (
  `id_cancha` int(11) NOT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  `ubicacion` varchar(30) DEFAULT NULL,
  `tipo_cancha` varchar(30) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cancha`
--

INSERT INTO `cancha` (`id_cancha`, `nombre`, `ubicacion`, `tipo_cancha`, `estado`) VALUES
(3, 'stadium wilsterman', 'av. libertador', 'stadium', 1),
(4, 'beato', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id_categoria` int(11) NOT NULL,
  `id_division` int(11) NOT NULL,
  `categoria` varchar(45) DEFAULT NULL,
  `edad_min` int(11) DEFAULT NULL,
  `edad_max` int(11) DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `id_division`, `categoria`, `edad_min`, `edad_max`, `estado`) VALUES
(4, 6, 'Primera A', NULL, NULL, 1),
(5, 6, 'Primera B', NULL, 25, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `club`
--

CREATE TABLE `club` (
  `id_club` int(11) NOT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  `fecha_fundacion` date DEFAULT NULL,
  `logo` varchar(100) DEFAULT 'sin_logo.png',
  `presidente` varchar(30) DEFAULT NULL,
  `tipo_club` tinyint(1) DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `club`
--

INSERT INTO `club` (`id_club`, `nombre`, `fecha_fundacion`, `logo`, `presidente`, `tipo_club`, `estado`) VALUES
(1, 'real cochabamba', '2017-12-06', '1568.png', 'sin presidente', NULL, 1),
(2, 'nacer', '2017-12-06', '1793.jpg', 'sin presidente', NULL, 1),
(3, 'beato salomon', '2017-12-06', '1921.png', 'sin presidente', NULL, 1),
(4, 'funinca', NULL, '2139.jpg', NULL, NULL, 1),
(5, 'tiquipaya', NULL, 'avatar.png', NULL, NULL, 1),
(6, 'colcapirhua', NULL, '1568.png', NULL, NULL, 1),
(7, 'enrique happ', NULL, '1568.png', NULL, NULL, 1),
(8, 'arsenal', NULL, '1568.png', NULL, NULL, 1),
(9, 'dogo futbol club', '2018-02-15', '4975.jpg', 'julio', NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `delegado`
--

CREATE TABLE `delegado` (
  `id_delegado` int(11) NOT NULL,
  `id_partido` int(11) NOT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  `informe` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `deuda_equipo`
--

CREATE TABLE `deuda_equipo` (
  `id_deuda_equipo` int(11) NOT NULL,
  `id_equipo` int(11) NOT NULL,
  `fecha_amonestacion` date DEFAULT NULL,
  `descripcion` text,
  `monto` int(11) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `deuda_equipo`
--

INSERT INTO `deuda_equipo` (`id_deuda_equipo`, `id_equipo`, `fecha_amonestacion`, `descripcion`, `monto`, `estado`) VALUES
(1, 22, '2018-03-16', '11233', 1000, 1),
(2, 20, '2018-03-28', 'tiquipaya description', 2000, 1),
(3, 16, '2018-03-30', 'real cochabamba description', 3000, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `division`
--

CREATE TABLE `division` (
  `id_division` int(11) NOT NULL,
  `division` varchar(45) DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `division`
--

INSERT INTO `division` (`id_division`, `division`, `estado`) VALUES
(6, 'Sección No Aficionados', 1),
(7, 'Sección Aficionados', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipo`
--

CREATE TABLE `equipo` (
  `id_equipo` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `id_club` int(11) DEFAULT NULL,
  `licencia` tinyint(1) DEFAULT NULL,
  `director_tecnico` varchar(30) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `equipo`
--

INSERT INTO `equipo` (`id_equipo`, `id_categoria`, `id_club`, `licencia`, `director_tecnico`, `estado`) VALUES
(16, 4, 1, 0, 'juan perez', 1),
(17, 4, 2, 0, 'juanito', 1),
(18, 4, 3, 0, 'a', 1),
(19, 4, 4, 0, 'w', 1),
(20, 4, 5, 0, 'q', 1),
(21, 4, 6, 0, 'g', 1),
(22, 4, 7, 0, 't', 1),
(23, 4, 8, 0, 'r', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipo_jugador`
--

CREATE TABLE `equipo_jugador` (
  `id_equipo` int(11) NOT NULL,
  `id_jugador` int(11) DEFAULT NULL,
  `en_espera` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `equipo_jugador`
--

INSERT INTO `equipo_jugador` (`id_equipo`, `id_jugador`, `en_espera`) VALUES
(16, 1, 0),
(16, 2, 0),
(16, 3, 0),
(16, 4, 0),
(16, 5, 0),
(16, 6, 0),
(16, 7, 0),
(16, 8, 0),
(16, 9, 0),
(16, 10, 0),
(16, 11, 0),
(16, 12, 0),
(16, 13, 0),
(16, 14, 0),
(16, 15, 0),
(16, 16, 0),
(16, 17, 0),
(16, 18, 0),
(16, 19, 0),
(16, 20, 0),
(16, 21, 0),
(16, 22, 0),
(18, 45, 0),
(18, 46, 1),
(18, 47, 0),
(18, 48, 0),
(18, 49, 0),
(18, 50, 0),
(18, 51, 0),
(18, 52, 0),
(18, 55, 0),
(18, 54, 0),
(18, 53, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fase`
--

CREATE TABLE `fase` (
  `id_fase` int(11) NOT NULL,
  `id_temporada` int(11) NOT NULL,
  `fase` varchar(45) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `fase`
--

INSERT INTO `fase` (`id_fase`, `id_temporada`, `fase`, `estado`) VALUES
(132, 55, 'Primera Fase', 1),
(136, 55, 'Segunda Fase', 1),
(137, 55, 'Final', 1),
(138, 56, 'Primera Fase', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fecha`
--

CREATE TABLE `fecha` (
  `id_fecha` int(11) NOT NULL,
  `id_grupo` int(11) NOT NULL,
  `fecha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `fecha`
--

INSERT INTO `fecha` (`id_fecha`, `id_grupo`, `fecha`) VALUES
(825, 185, '2018-03-21'),
(826, 185, '2018-03-21'),
(827, 185, '2018-03-21'),
(828, 185, '2018-03-21'),
(829, 185, '2018-03-21'),
(830, 185, '2018-03-21'),
(831, 185, '2018-03-21'),
(832, 185, '2018-03-21'),
(833, 185, '2018-03-21'),
(834, 185, '2018-03-21'),
(835, 185, '2018-03-21'),
(836, 185, '2018-03-21'),
(837, 185, '2018-03-21'),
(838, 185, '2018-03-21'),
(852, 189, '2018-03-22'),
(853, 189, '2018-03-22'),
(854, 189, '2018-03-22'),
(855, 190, '2018-03-22'),
(856, 190, '2018-03-22'),
(857, 191, '2018-03-22'),
(858, 191, '2018-03-22'),
(859, 191, '2018-03-22'),
(860, 191, '2018-03-22'),
(861, 191, '2018-03-22'),
(862, 191, '2018-03-22'),
(863, 191, '2018-03-22'),
(864, 191, '2018-03-22'),
(865, 191, '2018-03-22'),
(866, 191, '2018-03-22'),
(867, 191, '2018-03-22'),
(868, 191, '2018-03-22'),
(869, 191, '2018-03-22'),
(870, 191, '2018-03-22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo`
--

CREATE TABLE `grupo` (
  `id_grupo` int(11) NOT NULL,
  `id_fase` int(11) NOT NULL,
  `grupo` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `grupo`
--

INSERT INTO `grupo` (`id_grupo`, `id_fase`, `grupo`) VALUES
(185, 132, 'Grupo 1'),
(189, 136, 'Grupo 1'),
(190, 137, 'Grupo 1'),
(191, 138, 'Grupo 1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo_equipo`
--

CREATE TABLE `grupo_equipo` (
  `id_grupo` int(11) NOT NULL,
  `id_equipo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `grupo_equipo`
--

INSERT INTO `grupo_equipo` (`id_grupo`, `id_equipo`) VALUES
(185, 16),
(185, 17),
(185, 18),
(185, 19),
(185, 20),
(185, 21),
(185, 22),
(185, 23),
(189, 16),
(189, 17),
(189, 18),
(189, 19),
(190, 16),
(190, 17),
(190, 18),
(190, 19),
(191, 16),
(191, 17),
(191, 18),
(191, 19),
(191, 20),
(191, 21),
(191, 22),
(191, 23);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jugador`
--

CREATE TABLE `jugador` (
  `id_jugador` int(11) NOT NULL,
  `id_club` int(11) DEFAULT NULL,
  `nombre` varchar(20) DEFAULT NULL,
  `ap_paterno` varchar(20) DEFAULT NULL,
  `ap_materno` varchar(20) DEFAULT NULL,
  `ci` varchar(10) DEFAULT NULL,
  `matricula` int(11) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `sexo` tinyint(1) DEFAULT NULL,
  `direccion` varchar(30) DEFAULT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `celular` varchar(15) DEFAULT NULL,
  `tipo_jugador` varchar(45) DEFAULT NULL,
  `nacionalidad` tinyint(1) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `jugador`
--

INSERT INTO `jugador` (`id_jugador`, `id_club`, `nombre`, `ap_paterno`, `ap_materno`, `ci`, `matricula`, `fecha_nacimiento`, `sexo`, `direccion`, `telefono`, `celular`, `tipo_jugador`, `nacionalidad`, `estado`) VALUES
(1, 1, 'juan1', 'perez1', 'lopez1', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(2, 1, 'juan2', 'perez2', 'lopez2', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(3, 1, 'juan3', 'perez3', 'lopez3', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(4, 1, 'juan4', 'perez4', 'lopez4', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(5, 1, 'juan5', 'perez5', 'lopez5', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(6, 1, 'juan6', 'perez6', 'lopez6', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(7, 1, 'juan7', 'perez7', 'lopez7', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(8, 1, 'juan8', 'perez8', 'lopez8', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(9, 1, 'juan9', 'perez9', 'lopez9', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(10, 1, 'juan10', 'perez10', 'lopez10', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(11, 1, 'juan11', 'perez11', 'lopez11', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(12, 1, 'juan12', 'perez12', 'lopez12', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(13, 1, 'juan13', 'perez13', 'lopez13', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(14, 1, 'juan14', 'perez14', 'lopez14', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(15, 1, 'juan15', 'perez15', 'lopez15', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(16, 1, 'juan16', 'perez16', 'lopez16', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(17, 1, 'juan17', 'perez17', 'lopez17', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(18, 1, 'juan18', 'perez18', 'lopez18', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(19, 1, 'juan19', 'perez19', 'lopez19', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 0, 1),
(20, 1, 'juan20', 'perez20', 'lopez20', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 0, 1),
(21, 1, 'juan21', 'perez21', 'lopez21', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 0, 1),
(22, 1, 'juan22', 'perez22', 'lopez22', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 0, 1),
(23, 2, 'pedro1', 'cussi1', 'sanchez1', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(24, 2, 'pedro2', 'cussi2', 'sanchez2', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(25, 2, 'pedro3', 'cussi3', 'sanchez3', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(26, 2, 'pedro4', 'cussi4', 'sanchez4', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(27, 2, 'pedro5', 'cussi5', 'sanchez5', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(28, 2, 'pedro6', 'cussi6', 'sanchez6', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(29, 2, 'pedro7', 'cussi7', 'sanchez7', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(30, 2, 'pedro8', 'cussi8', 'sanchez8', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(31, 2, 'pedro9', 'cussi9', 'sanchez9', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(32, 2, 'pedro10', 'cussi10', 'sanchez10', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(33, 2, 'pedro11', 'cussi11', 'sanchez11', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(34, 2, 'pedro12', 'cussi12', 'sanchez12', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(35, 2, 'pedro13', 'cussi13', 'sanchez13', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(36, 2, 'pedro14', 'cussi14', 'sanchez14', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(37, 2, 'pedro15', 'cussi15', 'sanchez15', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(38, 2, 'pedro16', 'cussi16', 'sanchez16', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(39, 2, 'pedro17', 'cussi17', 'sanchez17', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(40, 2, 'pedro18', 'cussi18', 'sanchez18', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(41, 2, 'pedro19', 'cussi19', 'sanchez19', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 0, 1),
(42, 2, 'pedro20', 'cussi20', 'sanchez20', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 0, 1),
(43, 2, 'pedro21', 'cussi21', 'sanchez21', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 0, 1),
(44, 2, 'pedro22', 'cussi22', 'sanchez22', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 0, 1),
(45, 3, 'jose1', 'castro1', 'sainz1', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(46, 3, 'jose2', 'castro2', 'sainz2', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(47, 3, 'jose3', 'castro3', 'sainz3', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(48, 3, 'jose4', 'castro4', 'sainz4', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(49, 3, 'jose5', 'castro5', 'sainz5', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(50, 3, 'jose6', 'castro6', 'sainz6', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(51, 3, 'jose7', 'castro7', 'sainz7', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(52, 3, 'jose8', 'castro8', 'sainz8', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(53, 3, 'jose9', 'castro9', 'sainz9', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(54, 3, 'jose10', 'castro10', 'sainz10', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(55, 3, 'jose11', 'castro11', 'sainz11', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(56, 3, 'jose12', 'castro12', 'sainz12', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(57, 3, 'jose13', 'castro13', 'sainz13', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(58, 3, 'jose14', 'castro14', 'sainz14', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(59, 3, 'jose15', 'castro15', 'sainz15', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(60, 3, 'jose16', 'castro16', 'sainz16', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(61, 3, 'jose17', 'castro17', 'sainz17', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(62, 3, 'jose18', 'castro18', 'sainz18', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(63, 3, 'jose19', 'castro19', 'sainz19', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 0, 1),
(64, 3, 'jose20', 'castro20', 'sainz20', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 0, 1),
(65, 3, 'jose21', 'castro21', 'sainz21', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 0, 1),
(66, 3, 'jose22', 'castro22', 'sainz22', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 0, 1),
(67, 4, 'luis1', 'rodriguez1', 'rocha1', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(68, 4, 'luis2', 'rodriguez2', 'rocha2', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(69, 4, 'luis3', 'rodriguez3', 'rocha3', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(70, 4, 'luis4', 'rodriguez4', 'rocha4', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(71, 4, 'luis5', 'rodriguez5', 'rocha5', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(72, 4, 'luis6', 'rodriguez6', 'rocha6', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(73, 4, 'luis7', 'rodriguez7', 'rocha7', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(74, 4, 'luis8', 'rodriguez8', 'rocha8', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(75, 4, 'luis9', 'rodriguez9', 'rocha9', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(76, 4, 'luis10', 'rodriguez10', 'rocha10', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(77, 4, 'luis11', 'rodriguez11', 'rocha11', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(78, 4, 'luis12', 'rodriguez12', 'rocha12', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(79, 4, 'luis13', 'rodriguez13', 'rocha13', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(80, 4, 'luis14', 'rodriguez14', 'rocha14', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(81, 4, 'luis15', 'rodriguez15', 'rocha15', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(82, 4, 'luis16', 'rodriguez16', 'rocha16', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(83, 4, 'luis17', 'rodriguez17', 'rocha17', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(84, 4, 'luis18', 'rodriguez18', 'rocha18', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 1, 1),
(85, 4, 'luis19', 'rodriguez19', 'rocha19', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 0, 1),
(86, 4, 'luis20', 'rodriguez20', 'rocha20', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 0, 1),
(87, 4, 'luis21', 'rodriguez21', 'rocha21', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 0, 1),
(88, 4, 'luis22', 'rodriguez22', 'rocha22', '123', 123, '1995-05-05', 1, NULL, NULL, NULL, '1', 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log_partido`
--

CREATE TABLE `log_partido` (
  `id_partido` int(11) NOT NULL,
  `id_equipo` int(11) NOT NULL,
  `id_jugador` int(11) DEFAULT NULL,
  `titular` tinyint(1) DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `numero_s` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `log_partido`
--

INSERT INTO `log_partido` (`id_partido`, `id_equipo`, `id_jugador`, `titular`, `numero`, `numero_s`) VALUES
(1928, 16, 1, 1, NULL, NULL),
(1928, 16, 2, 1, NULL, NULL),
(1928, 16, 3, 1, NULL, NULL),
(1928, 16, 4, 1, NULL, NULL),
(1928, 16, 5, 1, NULL, NULL),
(1928, 16, 6, 1, NULL, NULL),
(1928, 16, 7, 1, NULL, NULL),
(1928, 16, 8, 1, NULL, NULL),
(1928, 16, 9, 1, NULL, NULL),
(1928, 16, 10, 1, NULL, NULL),
(1928, 16, 11, 1, NULL, NULL),
(1929, 18, 45, 1, NULL, NULL),
(1929, 18, 46, 1, NULL, NULL),
(1929, 18, 47, 1, NULL, NULL),
(1929, 18, 48, 1, NULL, NULL),
(1929, 18, 49, 1, NULL, NULL),
(1929, 18, 50, 1, NULL, NULL),
(1841, 16, 4, 1, NULL, NULL),
(1842, 18, 45, 1, NULL, NULL),
(1842, 18, 46, 1, NULL, NULL),
(1923, 16, 1, 1, NULL, NULL),
(1923, 16, 2, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `observacion`
--

CREATE TABLE `observacion` (
  `id_observacion` int(11) NOT NULL,
  `id_jugador` int(11) DEFAULT NULL,
  `id_partido` int(11) DEFAULT NULL,
  `id_tipo_observacion` int(11) NOT NULL,
  `observacion` varchar(45) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `observacion`
--

INSERT INTO `observacion` (`id_observacion`, `id_jugador`, `id_partido`, `id_tipo_observacion`, `observacion`, `estado`) VALUES
(1, 6, 1928, 1, '1', 1),
(2, 45, 1929, 1, '1', 1),
(3, 4, 1841, 1, '2', 1),
(4, 45, 1842, 1, '1', 1),
(5, 46, 1842, 1, '1', 1),
(6, 1, 1923, 1, '1', 1),
(7, 4, 1841, 3, NULL, 1),
(8, 46, 1842, 2, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `partido`
--

CREATE TABLE `partido` (
  `id_partido` int(11) NOT NULL,
  `id_equipo1` int(11) DEFAULT NULL,
  `id_equipo2` int(11) DEFAULT NULL,
  `id_cancha` int(11) DEFAULT NULL,
  `id_fecha` int(11) NOT NULL,
  `hora` time DEFAULT NULL,
  `ptos_equi1` int(11) DEFAULT '0',
  `ptos_equi2` int(11) DEFAULT '0',
  `goles_equi1` int(11) DEFAULT '0',
  `goles_equi2` int(11) DEFAULT '0',
  `jugado` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `partido`
--

INSERT INTO `partido` (`id_partido`, `id_equipo1`, `id_equipo2`, `id_cancha`, `id_fecha`, `hora`, `ptos_equi1`, `ptos_equi2`, `goles_equi1`, `goles_equi2`, `jugado`) VALUES
(1840, 23, 19, NULL, 825, NULL, 0, 0, 0, 0, 0),
(1841, 16, 22, NULL, 825, NULL, 0, 0, 2, 0, 1),
(1842, 17, 18, NULL, 825, NULL, 0, 0, 0, 2, 1),
(1843, 21, 20, NULL, 825, NULL, 0, 0, 0, 0, 0),
(1844, 23, 16, NULL, 826, NULL, 0, 0, 0, 0, 0),
(1845, 17, 19, NULL, 826, NULL, 0, 0, 0, 0, 0),
(1846, 21, 22, NULL, 826, NULL, 0, 0, 0, 0, 0),
(1847, 20, 18, NULL, 826, NULL, 0, 0, 0, 0, 0),
(1848, 23, 17, NULL, 827, NULL, 0, 0, 0, 0, 0),
(1849, 21, 16, NULL, 827, NULL, 0, 0, 0, 0, 0),
(1850, 20, 19, NULL, 827, NULL, 0, 0, 0, 0, 0),
(1851, 18, 22, NULL, 827, NULL, 0, 0, 0, 0, 0),
(1852, 23, 21, NULL, 828, NULL, 0, 0, 0, 0, 0),
(1853, 20, 17, NULL, 828, NULL, 0, 0, 0, 0, 0),
(1854, 18, 16, NULL, 828, NULL, 0, 0, 0, 0, 0),
(1855, 22, 19, NULL, 828, NULL, 0, 0, 0, 0, 0),
(1856, 23, 20, NULL, 829, NULL, 0, 0, 0, 0, 0),
(1857, 18, 21, NULL, 829, NULL, 0, 0, 0, 0, 0),
(1858, 22, 17, NULL, 829, NULL, 0, 0, 0, 0, 0),
(1859, 19, 16, NULL, 829, NULL, 0, 0, 0, 0, 0),
(1860, 23, 18, NULL, 830, NULL, 0, 0, 0, 0, 0),
(1861, 22, 20, NULL, 830, NULL, 0, 0, 0, 0, 0),
(1862, 19, 21, NULL, 830, NULL, 0, 0, 0, 0, 0),
(1863, 16, 17, NULL, 830, NULL, 0, 0, 0, 0, 0),
(1864, 23, 22, NULL, 831, NULL, 0, 0, 0, 0, 0),
(1865, 19, 18, NULL, 831, NULL, 0, 0, 0, 0, 0),
(1866, 16, 20, NULL, 831, NULL, 0, 0, 0, 0, 0),
(1867, 17, 21, NULL, 831, NULL, 0, 0, 0, 0, 0),
(1868, 23, 19, NULL, 832, NULL, 0, 0, 0, 0, 0),
(1869, 16, 22, NULL, 832, NULL, 0, 0, 0, 0, 0),
(1870, 17, 18, NULL, 832, NULL, 0, 0, 0, 0, 0),
(1871, 21, 20, NULL, 832, NULL, 0, 0, 0, 0, 0),
(1872, 23, 16, NULL, 833, NULL, 0, 0, 0, 0, 0),
(1873, 17, 19, NULL, 833, NULL, 0, 0, 0, 0, 0),
(1874, 21, 22, NULL, 833, NULL, 0, 0, 0, 0, 0),
(1875, 20, 18, NULL, 833, NULL, 0, 0, 0, 0, 0),
(1876, 23, 17, NULL, 834, NULL, 0, 0, 0, 0, 0),
(1877, 21, 16, NULL, 834, NULL, 0, 0, 0, 0, 0),
(1878, 20, 19, NULL, 834, NULL, 0, 0, 0, 0, 0),
(1879, 18, 22, NULL, 834, NULL, 0, 0, 0, 0, 0),
(1880, 23, 21, NULL, 835, NULL, 0, 0, 0, 0, 0),
(1881, 20, 17, NULL, 835, NULL, 0, 0, 0, 0, 0),
(1882, 18, 16, NULL, 835, NULL, 0, 0, 0, 0, 0),
(1883, 22, 19, NULL, 835, NULL, 0, 0, 0, 0, 0),
(1884, 23, 20, NULL, 836, NULL, 0, 0, 0, 0, 0),
(1885, 18, 21, NULL, 836, NULL, 0, 0, 0, 0, 0),
(1886, 22, 17, NULL, 836, NULL, 0, 0, 0, 0, 0),
(1887, 19, 16, NULL, 836, NULL, 0, 0, 0, 0, 0),
(1888, 23, 18, NULL, 837, NULL, 0, 0, 0, 0, 0),
(1889, 22, 20, NULL, 837, NULL, 0, 0, 0, 0, 0),
(1890, 19, 21, NULL, 837, NULL, 0, 0, 0, 0, 0),
(1891, 16, 17, NULL, 837, NULL, 0, 0, 0, 0, 0),
(1892, 23, 22, NULL, 838, NULL, 0, 0, 0, 0, 0),
(1893, 19, 18, NULL, 838, NULL, 0, 0, 0, 0, 0),
(1894, 16, 20, NULL, 838, NULL, 0, 0, 0, 0, 0),
(1895, 17, 21, NULL, 838, NULL, 0, 0, 0, 0, 0),
(1922, 19, 17, NULL, 852, NULL, 0, 0, 0, 0, 0),
(1923, 16, 18, NULL, 852, NULL, 0, 0, 1, 0, 1),
(1924, 19, 16, NULL, 853, NULL, 0, 0, 0, 0, 0),
(1925, 18, 17, NULL, 853, NULL, 0, 0, 0, 0, 0),
(1926, 19, 18, NULL, 854, NULL, 0, 0, 0, 0, 0),
(1927, 17, 16, NULL, 854, NULL, 0, 0, 0, 0, 0),
(1928, 16, 17, NULL, 855, NULL, 0, 0, 1, 0, 1),
(1929, 18, 19, NULL, 856, NULL, 0, 0, 1, 0, 1),
(1930, 19, 17, NULL, 857, NULL, 0, 0, 0, 0, 0),
(1931, 23, 22, NULL, 857, NULL, 0, 0, 0, 0, 0),
(1932, 20, 16, NULL, 857, NULL, 0, 0, 0, 0, 0),
(1933, 18, 21, NULL, 857, NULL, 0, 0, 0, 0, 0),
(1934, 19, 23, NULL, 858, NULL, 0, 0, 0, 0, 0),
(1935, 20, 17, NULL, 858, NULL, 0, 0, 0, 0, 0),
(1936, 18, 22, NULL, 858, NULL, 0, 0, 0, 0, 0),
(1937, 21, 16, NULL, 858, NULL, 0, 0, 0, 0, 0),
(1938, 19, 20, NULL, 859, NULL, 0, 0, 0, 0, 0),
(1939, 18, 23, NULL, 859, NULL, 0, 0, 0, 0, 0),
(1940, 21, 17, NULL, 859, NULL, 0, 0, 0, 0, 0),
(1941, 16, 22, NULL, 859, NULL, 0, 0, 0, 0, 0),
(1942, 19, 18, NULL, 860, NULL, 0, 0, 0, 0, 0),
(1943, 21, 20, NULL, 860, NULL, 0, 0, 0, 0, 0),
(1944, 16, 23, NULL, 860, NULL, 0, 0, 0, 0, 0),
(1945, 22, 17, NULL, 860, NULL, 0, 0, 0, 0, 0),
(1946, 19, 21, NULL, 861, NULL, 0, 0, 0, 0, 0),
(1947, 16, 18, NULL, 861, NULL, 0, 0, 0, 0, 0),
(1948, 22, 20, NULL, 861, NULL, 0, 0, 0, 0, 0),
(1949, 17, 23, NULL, 861, NULL, 0, 0, 0, 0, 0),
(1950, 19, 16, NULL, 862, NULL, 0, 0, 0, 0, 0),
(1951, 22, 21, NULL, 862, NULL, 0, 0, 0, 0, 0),
(1952, 17, 18, NULL, 862, NULL, 0, 0, 0, 0, 0),
(1953, 23, 20, NULL, 862, NULL, 0, 0, 0, 0, 0),
(1954, 19, 22, NULL, 863, NULL, 0, 0, 0, 0, 0),
(1955, 17, 16, NULL, 863, NULL, 0, 0, 0, 0, 0),
(1956, 23, 21, NULL, 863, NULL, 0, 0, 0, 0, 0),
(1957, 20, 18, NULL, 863, NULL, 0, 0, 0, 0, 0),
(1958, 19, 17, NULL, 864, NULL, 0, 0, 0, 0, 0),
(1959, 23, 22, NULL, 864, NULL, 0, 0, 0, 0, 0),
(1960, 20, 16, NULL, 864, NULL, 0, 0, 0, 0, 0),
(1961, 18, 21, NULL, 864, NULL, 0, 0, 0, 0, 0),
(1962, 19, 23, NULL, 865, NULL, 0, 0, 0, 0, 0),
(1963, 20, 17, NULL, 865, NULL, 0, 0, 0, 0, 0),
(1964, 18, 22, NULL, 865, NULL, 0, 0, 0, 0, 0),
(1965, 21, 16, NULL, 865, NULL, 0, 0, 0, 0, 0),
(1966, 19, 20, NULL, 866, NULL, 0, 0, 0, 0, 0),
(1967, 18, 23, NULL, 866, NULL, 0, 0, 0, 0, 0),
(1968, 21, 17, NULL, 866, NULL, 0, 0, 0, 0, 0),
(1969, 16, 22, NULL, 866, NULL, 0, 0, 0, 0, 0),
(1970, 19, 18, NULL, 867, NULL, 0, 0, 0, 0, 0),
(1971, 21, 20, NULL, 867, NULL, 0, 0, 0, 0, 0),
(1972, 16, 23, NULL, 867, NULL, 0, 0, 0, 0, 0),
(1973, 22, 17, NULL, 867, NULL, 0, 0, 0, 0, 0),
(1974, 19, 21, NULL, 868, NULL, 0, 0, 0, 0, 0),
(1975, 16, 18, NULL, 868, NULL, 0, 0, 0, 0, 0),
(1976, 22, 20, NULL, 868, NULL, 0, 0, 0, 0, 0),
(1977, 17, 23, NULL, 868, NULL, 0, 0, 0, 0, 0),
(1978, 19, 16, NULL, 869, NULL, 0, 0, 0, 0, 0),
(1979, 22, 21, NULL, 869, NULL, 0, 0, 0, 0, 0),
(1980, 17, 18, NULL, 869, NULL, 0, 0, 0, 0, 0),
(1981, 23, 20, NULL, 869, NULL, 0, 0, 0, 0, 0),
(1982, 19, 22, NULL, 870, NULL, 0, 0, 0, 0, 0),
(1983, 17, 16, NULL, 870, NULL, 0, 0, 0, 0, 0),
(1984, 23, 21, NULL, 870, NULL, 0, 0, 0, 0, 0),
(1985, 20, 18, NULL, 870, NULL, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `planillero`
--

CREATE TABLE `planillero` (
  `id_planillero` int(11) NOT NULL,
  `id_partido` int(11) NOT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  `informe` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sancion`
--

CREATE TABLE `sancion` (
  `id_sancion` int(11) NOT NULL,
  `id_jugador` int(11) NOT NULL,
  `descripcion` text,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL,
  `sancion_economica` int(11) DEFAULT NULL,
  `numero_partidos` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `estado` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sancion`
--

INSERT INTO `sancion` (`id_sancion`, `id_jugador`, `descripcion`, `fecha_inicio`, `fecha_fin`, `sancion_economica`, `numero_partidos`, `fecha`, `estado`) VALUES
(1, 1, 'mal jugador', '2018-03-06', '2018-03-28', 1000, 1, '2018-03-15', 1),
(2, 54, 'bla bla bla bla', '2018-03-07', '2018-03-30', 1000, 1, '2018-03-20', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temporada`
--

CREATE TABLE `temporada` (
  `id_temporada` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `gestion` varchar(30) DEFAULT NULL,
  `torneo` varchar(30) DEFAULT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL,
  `estado` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `temporada`
--

INSERT INTO `temporada` (`id_temporada`, `id_categoria`, `gestion`, `torneo`, `fecha_inicio`, `fecha_fin`, `estado`) VALUES
(55, 4, '2-2018', 'fin de año', '2018-03-24', '2018-03-31', 1),
(56, 4, 'III-2018', 'diciembre fin de año', '2018-03-07', '2018-03-31', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_observacion`
--

CREATE TABLE `tipo_observacion` (
  `id_tipo_observacion` int(11) NOT NULL,
  `tipo` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_observacion`
--

INSERT INTO `tipo_observacion` (`id_tipo_observacion`, `tipo`) VALUES
(1, 'gol'),
(2, 'tarjeta roja'),
(3, 'tarjeta amarilla');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `rol` tinyint(1) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `rol`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'dani', 'dandiel_23@hotmail.es', '$2y$10$coPYduMFAwlD1LQRTTEbmetD5UmoeFNrT2ktyG3Lit6zFJhnX66RG', 1, 'Trk472kXa9LRFnmRdfnAIrNGdUzKv5yWsniMfND5ZnavTDaa6hth9bmPlmE9', '2017-08-14 11:42:22', '2017-08-14 11:42:22'),
(2, 'boris', 'boris@gmail.com', '$2y$10$J1Jv6WyMaRtqsoLS4v8agu/VyNhSzmeZbumGvEB8NI4C7pveYsV2G', 2, 'vDyesoNKMe8pL9gElCNFpfljBSig59LnmlJCbbG8Ih7JxO48yrkrtjAtNCnI', '2018-02-16 19:03:23', '2018-02-16 19:03:23'),
(3, 'Selena Gomez', 'selena@gmail.com', '$2y$10$Smm6zEWZchwXzcufZimS9u4817Gj0iA0xHGNGBJAP3RHsZ8RYETvG', 3, 'cwxULssHaEs69QebLKPYQ1HASuINpDipbKIlSpLNsV7dG4wyhU3euqXzVbLq', '2018-03-20 08:42:18', '2018-03-20 08:42:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `veedor`
--

CREATE TABLE `veedor` (
  `id_veedor` int(11) NOT NULL,
  `id_partido` int(11) NOT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  `conducta_publico` varchar(30) DEFAULT NULL,
  `conducta_jugadores` varchar(30) DEFAULT NULL,
  `calificacion_arbitro` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `arbitro`
--
ALTER TABLE `arbitro`
  ADD PRIMARY KEY (`id_arbitro`),
  ADD KEY `FK_REFERENCE_18` (`id_partido`);

--
-- Indices de la tabla `cancha`
--
ALTER TABLE `cancha`
  ADD PRIMARY KEY (`id_cancha`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_categoria`),
  ADD KEY `fk_categoria_division1_idx` (`id_division`);

--
-- Indices de la tabla `club`
--
ALTER TABLE `club`
  ADD PRIMARY KEY (`id_club`);

--
-- Indices de la tabla `delegado`
--
ALTER TABLE `delegado`
  ADD PRIMARY KEY (`id_delegado`),
  ADD KEY `FK_REFERENCE_19` (`id_partido`);

--
-- Indices de la tabla `deuda_equipo`
--
ALTER TABLE `deuda_equipo`
  ADD PRIMARY KEY (`id_deuda_equipo`),
  ADD KEY `fk_deuda_equipo_equipo1_idx` (`id_equipo`);

--
-- Indices de la tabla `division`
--
ALTER TABLE `division`
  ADD PRIMARY KEY (`id_division`);

--
-- Indices de la tabla `equipo`
--
ALTER TABLE `equipo`
  ADD PRIMARY KEY (`id_equipo`),
  ADD KEY `fk_equipo_categoria1_idx` (`id_categoria`),
  ADD KEY `FK_REFERENCE_4` (`id_club`);

--
-- Indices de la tabla `equipo_jugador`
--
ALTER TABLE `equipo_jugador`
  ADD KEY `fk_equipo_jugador_grupo_equipo1_idx` (`id_equipo`);

--
-- Indices de la tabla `fase`
--
ALTER TABLE `fase`
  ADD PRIMARY KEY (`id_fase`),
  ADD KEY `fk_fase_temporada1_idx` (`id_temporada`);

--
-- Indices de la tabla `fecha`
--
ALTER TABLE `fecha`
  ADD PRIMARY KEY (`id_fecha`),
  ADD KEY `fk_fecha_grupo1_idx` (`id_grupo`);

--
-- Indices de la tabla `grupo`
--
ALTER TABLE `grupo`
  ADD PRIMARY KEY (`id_grupo`),
  ADD KEY `fk_grupo_fase1_idx` (`id_fase`);

--
-- Indices de la tabla `grupo_equipo`
--
ALTER TABLE `grupo_equipo`
  ADD PRIMARY KEY (`id_grupo`,`id_equipo`),
  ADD KEY `fk_grupo_equipo_equipo2_idx` (`id_equipo`),
  ADD KEY `fk_grupo_equipo_grupo1_idx` (`id_grupo`);

--
-- Indices de la tabla `jugador`
--
ALTER TABLE `jugador`
  ADD PRIMARY KEY (`id_jugador`),
  ADD KEY `fk_jugador_club1_idx` (`id_club`);

--
-- Indices de la tabla `log_partido`
--
ALTER TABLE `log_partido`
  ADD KEY `fk_log_partido_partido1_idx` (`id_partido`);

--
-- Indices de la tabla `observacion`
--
ALTER TABLE `observacion`
  ADD PRIMARY KEY (`id_observacion`),
  ADD KEY `FK_REFERENCE_9` (`id_jugador`),
  ADD KEY `fk_observacion_tipo_observacion1_idx` (`id_tipo_observacion`),
  ADD KEY `FK_REFERENCE_14` (`id_partido`);

--
-- Indices de la tabla `partido`
--
ALTER TABLE `partido`
  ADD PRIMARY KEY (`id_partido`),
  ADD KEY `FK_REFERENCE_11` (`id_equipo1`),
  ADD KEY `FK_REFERENCE_12` (`id_equipo2`),
  ADD KEY `fk_partido_fecha1_idx` (`id_fecha`),
  ADD KEY `fk_partido_cancha1_idx` (`id_cancha`);

--
-- Indices de la tabla `planillero`
--
ALTER TABLE `planillero`
  ADD PRIMARY KEY (`id_planillero`),
  ADD KEY `FK_REFERENCE_20` (`id_partido`);

--
-- Indices de la tabla `sancion`
--
ALTER TABLE `sancion`
  ADD PRIMARY KEY (`id_sancion`),
  ADD KEY `fk_sancion_jugador1_idx` (`id_jugador`);

--
-- Indices de la tabla `temporada`
--
ALTER TABLE `temporada`
  ADD PRIMARY KEY (`id_temporada`),
  ADD KEY `fk_temporada_categoria1_idx` (`id_categoria`);

--
-- Indices de la tabla `tipo_observacion`
--
ALTER TABLE `tipo_observacion`
  ADD PRIMARY KEY (`id_tipo_observacion`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `veedor`
--
ALTER TABLE `veedor`
  ADD PRIMARY KEY (`id_veedor`),
  ADD KEY `fk_veedor_partido1_idx` (`id_partido`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `arbitro`
--
ALTER TABLE `arbitro`
  MODIFY `id_arbitro` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cancha`
--
ALTER TABLE `cancha`
  MODIFY `id_cancha` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `club`
--
ALTER TABLE `club`
  MODIFY `id_club` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `delegado`
--
ALTER TABLE `delegado`
  MODIFY `id_delegado` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `deuda_equipo`
--
ALTER TABLE `deuda_equipo`
  MODIFY `id_deuda_equipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `division`
--
ALTER TABLE `division`
  MODIFY `id_division` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `equipo`
--
ALTER TABLE `equipo`
  MODIFY `id_equipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT de la tabla `fase`
--
ALTER TABLE `fase`
  MODIFY `id_fase` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;
--
-- AUTO_INCREMENT de la tabla `fecha`
--
ALTER TABLE `fecha`
  MODIFY `id_fecha` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=871;
--
-- AUTO_INCREMENT de la tabla `grupo`
--
ALTER TABLE `grupo`
  MODIFY `id_grupo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=192;
--
-- AUTO_INCREMENT de la tabla `jugador`
--
ALTER TABLE `jugador`
  MODIFY `id_jugador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT de la tabla `observacion`
--
ALTER TABLE `observacion`
  MODIFY `id_observacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `partido`
--
ALTER TABLE `partido`
  MODIFY `id_partido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1986;
--
-- AUTO_INCREMENT de la tabla `planillero`
--
ALTER TABLE `planillero`
  MODIFY `id_planillero` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `sancion`
--
ALTER TABLE `sancion`
  MODIFY `id_sancion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `temporada`
--
ALTER TABLE `temporada`
  MODIFY `id_temporada` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT de la tabla `tipo_observacion`
--
ALTER TABLE `tipo_observacion`
  MODIFY `id_tipo_observacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `veedor`
--
ALTER TABLE `veedor`
  MODIFY `id_veedor` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `arbitro`
--
ALTER TABLE `arbitro`
  ADD CONSTRAINT `FK_REFERENCE_18` FOREIGN KEY (`id_partido`) REFERENCES `partido` (`id_partido`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD CONSTRAINT `fk_categoria_division1` FOREIGN KEY (`id_division`) REFERENCES `division` (`id_division`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `delegado`
--
ALTER TABLE `delegado`
  ADD CONSTRAINT `FK_REFERENCE_19` FOREIGN KEY (`id_partido`) REFERENCES `partido` (`id_partido`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `deuda_equipo`
--
ALTER TABLE `deuda_equipo`
  ADD CONSTRAINT `fk_deuda_equipo_equipo1` FOREIGN KEY (`id_equipo`) REFERENCES `equipo` (`id_equipo`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `equipo`
--
ALTER TABLE `equipo`
  ADD CONSTRAINT `FK_REFERENCE_4` FOREIGN KEY (`id_club`) REFERENCES `club` (`id_club`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_equipo_categoria1` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `equipo_jugador`
--
ALTER TABLE `equipo_jugador`
  ADD CONSTRAINT `fk_equipo_jugador_grupo_equipo1` FOREIGN KEY (`id_equipo`) REFERENCES `grupo_equipo` (`id_equipo`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `fase`
--
ALTER TABLE `fase`
  ADD CONSTRAINT `fk_fase_temporada1` FOREIGN KEY (`id_temporada`) REFERENCES `temporada` (`id_temporada`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `fecha`
--
ALTER TABLE `fecha`
  ADD CONSTRAINT `fk_fecha_grupo1` FOREIGN KEY (`id_grupo`) REFERENCES `grupo` (`id_grupo`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `grupo`
--
ALTER TABLE `grupo`
  ADD CONSTRAINT `fk_grupo_fase1` FOREIGN KEY (`id_fase`) REFERENCES `fase` (`id_fase`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `grupo_equipo`
--
ALTER TABLE `grupo_equipo`
  ADD CONSTRAINT `fk_grupo_equipo_equipo2` FOREIGN KEY (`id_equipo`) REFERENCES `equipo` (`id_equipo`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_grupo_equipo_grupo1` FOREIGN KEY (`id_grupo`) REFERENCES `grupo` (`id_grupo`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `jugador`
--
ALTER TABLE `jugador`
  ADD CONSTRAINT `fk_jugador_club1` FOREIGN KEY (`id_club`) REFERENCES `club` (`id_club`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `log_partido`
--
ALTER TABLE `log_partido`
  ADD CONSTRAINT `fk_log_partido_partido1` FOREIGN KEY (`id_partido`) REFERENCES `partido` (`id_partido`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `observacion`
--
ALTER TABLE `observacion`
  ADD CONSTRAINT `FK_REFERENCE_14` FOREIGN KEY (`id_partido`) REFERENCES `partido` (`id_partido`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_REFERENCE_9` FOREIGN KEY (`id_jugador`) REFERENCES `jugador` (`id_jugador`),
  ADD CONSTRAINT `fk_observacion_tipo_observacion1` FOREIGN KEY (`id_tipo_observacion`) REFERENCES `tipo_observacion` (`id_tipo_observacion`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `partido`
--
ALTER TABLE `partido`
  ADD CONSTRAINT `FK_REFERENCE_11` FOREIGN KEY (`id_equipo1`) REFERENCES `equipo` (`id_equipo`),
  ADD CONSTRAINT `FK_REFERENCE_12` FOREIGN KEY (`id_equipo2`) REFERENCES `equipo` (`id_equipo`),
  ADD CONSTRAINT `fk_partido_cancha1` FOREIGN KEY (`id_cancha`) REFERENCES `cancha` (`id_cancha`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_partido_fecha1` FOREIGN KEY (`id_fecha`) REFERENCES `fecha` (`id_fecha`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `planillero`
--
ALTER TABLE `planillero`
  ADD CONSTRAINT `FK_REFERENCE_20` FOREIGN KEY (`id_partido`) REFERENCES `partido` (`id_partido`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `sancion`
--
ALTER TABLE `sancion`
  ADD CONSTRAINT `fk_sancion_jugador1` FOREIGN KEY (`id_jugador`) REFERENCES `jugador` (`id_jugador`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `temporada`
--
ALTER TABLE `temporada`
  ADD CONSTRAINT `fk_temporada_categoria1` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `veedor`
--
ALTER TABLE `veedor`
  ADD CONSTRAINT `fk_veedor_partido1` FOREIGN KEY (`id_partido`) REFERENCES `partido` (`id_partido`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
