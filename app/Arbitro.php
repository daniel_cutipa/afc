<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Arbitro extends Model
{
    protected $table = 'arbitro';
    protected $primaryKey = 'id_arbitro';
    protected $fillable = ['id_partido', 'nombre','informe'];
    public $timestamps = false;
}
