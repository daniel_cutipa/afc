<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'rol',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // roles = [
    //     1 => 'admin',
    //     2 => 'planillero',
    // ];

    // para usar el @can = [
    //     @can('admin') => 'admin',
    //     @can('plani') => 'planillero',
    // ];
    // misma idea para usar los middleware
}
