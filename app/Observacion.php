<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Observacion extends Model
{
    protected $table = 'observacion';
    protected $primaryKey = 'id_observacion';
    protected $fillable = ['id_jugador','id_partido','id_tipo_observacion','observacion', 'estado'];
    public $timestamps = false;
}

// el atributo estado es para saber si esta vigente o no las amarillas y las rojas
// 1 => vigente
// 0 => no vigente