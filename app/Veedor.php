<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Veedor extends Model
{

	//le damos el nombre a la tabla
    protected $table = 'veedor';
    protected $primaryKey = 'id_veedor';
    protected $fillable = ['id_partido','nombre', 'conducta_publico', 'conducta_jugadores', 'calificacion_arbitro'];
    //ya no guarda el created_at, updated_at 
    public $timestamps = false;

   

}
