<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Temporada extends Model
{
    //le damos el nombre a la tabla
    protected $table = 'temporada';
    protected $primaryKey = 'id_temporada';
    protected $fillable = ['id_categoria', 'gestion', 'torneo', 'fecha_inicio', 'fecha_fin', 'estado' ];
    //ya no guarda el created_at, updated_at 
    public $timestamps = false;

    public function categoria(){
    	return $this->hasOne(Categoria::class, 'id_categoria', 'id_categoria');
    }
}
