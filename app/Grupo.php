<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grupo extends Model
{
    //le damos el nombre a la tabla
    protected $table = 'grupo';
    protected $primaryKey = 'id_grupo';
    protected $fillable = ['id_fase', 'grupo' ];
    //ya no guarda el created_at, updated_at 
    public $timestamps = false;

    public function fase(){
    	return $this->hasOne(Fase::class, 'id_fase', 'id_fase');
    }
}
