<?php

namespace App\Providers;
// implementado por mi
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Support\Facades\Auth;
// hasta aqui
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(GateContract $gate) // GateContract $gate implement for me
    {
        $this->registerPolicies();

        //
        $gate->define('admin', function () {
            return Auth::user()->rol == 1;
        });
        $gate->define('plani', function () {
            return Auth::user()->rol == 2;
        });
        $gate->define('secre', function () {
            return Auth::user()->rol == 3;
        });
    }
}
