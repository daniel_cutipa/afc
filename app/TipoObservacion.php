<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoObservacion extends Model
{

	//le damos el nombre a la tabla
    protected $table = 'tipo_observacion';
    protected $primaryKey = 'id_tipo_observacion';
    protected $fillable = ['tipo'];
    //ya no guarda el created_at, updated_at 
    public $timestamps = false;

   

}
