<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fecha extends Model
{
    //le damos el nombre a la tabla
    protected $table = 'fecha';
    protected $primaryKey = 'id_fecha';
    protected $fillable = ['id_fase', 'fecha' ];
    //ya no guarda el created_at, updated_at 
    public $timestamps = false;

    public function grupo(){
    	return $this->hasOne(Grupo::class, 'id_grupo', 'id_grupo');
    }
}
