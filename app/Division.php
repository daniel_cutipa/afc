<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{

	//le damos el nombre a la tabla
    protected $table = 'division';
    protected $primaryKey = 'id_division';
    protected $fillable = ['division', 'estado'];
    //ya no guarda el created_at, updated_at 
    public $timestamps = false;
}
