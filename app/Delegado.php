<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delegado extends Model
{
  //le damos el nombre a la tabla
  protected $table = 'delegado';
  protected $primaryKey = 'id_delegado';
  protected $fillable = ['id_partido','nombre', 'informe'];
  //ya no guarda el created_at, updated_at
  public $timestamps = false;
}
