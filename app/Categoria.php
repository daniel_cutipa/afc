<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    //le damos el nombre a la tabla
    protected $table = 'categoria';
    protected $primaryKey = 'id_categoria';
    protected $fillable = ['id_division', 'categoria', 'edad_min', 'edad_max', 'estado' ];
    //ya no guarda el created_at, updated_at 
    public $timestamps = false;

    public function division(){
    	return $this->hasOne(Division::class, 'id_division', 'id_division');
    }
}
