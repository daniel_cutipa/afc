<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sancion extends Model
{
  //le damos el nombre a la tabla
  protected $table = 'sancion';
  protected $primaryKey = 'id_sancion';
  protected $fillable = ['id_jugador','descripcion', 'fecha_inicio', 'fecha_fin', 'sancion_economica', 'numero_partidos', 'fecha', 'estado'];
  //ya no guarda el created_at, updated_at
  public $timestamps = false;

  public function jugador(){
    	return $this->hasOne(Jugador::class, 'id_jugador', 'id_jugador');
    }
}
