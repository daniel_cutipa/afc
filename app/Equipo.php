<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipo extends Model
{
    //le damos el nombre a la tabla
    protected $table = 'equipo';
    protected $primaryKey = 'id_equipo';
    protected $fillable = [ 'id_categoria', 'id_club', 'licencia', 'director_tecnico', 'estado' ];
    //el atributo licencia es para cuando un equipo no quiere participar en una temporada
    //ya no guarda el created_at, updated_at 
    public $timestamps = false;

    public function categoria(){
    	return $this->hasOne(Categoria::class, 'id_categoria', 'id_categoria');
    }

    public function club(){
    	return $this->hasOne(Club::class, 'id_club', 'id_club');
    }
}
