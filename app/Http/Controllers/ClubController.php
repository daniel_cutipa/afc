<?php

namespace App\Http\Controllers;

use App\Club;
use App\Equipo_jugador;
use App\Equipo;
use App\Jugador;
use App\Categoria;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;
use File;
use Html;
class ClubController extends Controller
{
   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
       $clubes = Club::where('estado', 1)->get();
       return view('club.index', compact('clubes'));
    }
    public function jugador ($id)
    {
        $jugadores = Jugador::where([
            'estado'=>1,
            'id_club'=> $id
        ])->paginate();

        $id_club = $id;

        return view('jugador.index', compact('jugadores', 'id_club'));       
    }


   /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function create()
   {
       $categorias = Categoria::all();
       return view('club.create', compact('categorias') );
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request)
   {
       $this->validate($request,[

           'nombre' => 'required|string|max:20',
           'fecha_fundacion' => 'required|date',
           'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
           'presidente' => 'required|string|max:30'
       ]);

        if ($request ->hasFile('logo'))
        {
            $destinationPath = "img";
            $file = $request -> logo;
            $extension =$file->getClientOriginalExtension();
            $fileName = rand(1111, 9999).".".$extension;
            $file->move($destinationPath,$fileName);
            $logo = $fileName;
        }
        $club = Club::create([
			'nombre' => $request['nombre'],
			'fecha_fundacion' => $request['fecha_fundacion'],
			'logo' => $logo,
            'presidente' => $request['presidente'],
            'tipo_club' => $request['tipo_club'],
			'estado' => 1,
        ]);

        foreach ($request['categorias'] as $key => $value) {
            Equipo::create([
                'id_categoria' => $value,
                'id_club' => $club->id_club,
                'estado' => 1,
            ]);
        }

       return redirect('/club')->with('message','Club registrado');

   }

   /**
    * Display the specified resource.
    *
    * @param  \App\Club  $club
    * @return \Illuminate\Http\Response
    */
   public function show(Club $club)
   {
       //
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Club  $club
    * @return \Illuminate\Http\Response
    */
   public function edit(Club $club)
   {
       
        return view('club.edit', compact('club'));

   }

   /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\Club  $club
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request, Club $club)
   {
    // dd($request);
        $this->validate($request, [
           'nombre' => 'required|string|max:20',
           'fecha_fundacion' => 'required|date',
           'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
           'presidente' => 'required|string|max:30'
       ]);

       if ($request->hasFile('logo'))
       {
        $destinationPath = "img";
        $file = $request->file('logo');
        $extension =$file->getClientOriginalExtension();
        $fileName = rand(1111, 9999).".".$extension;
        $file->move($destinationPath,$fileName);
        $logo = $fileName;
       }

       $club->nombre = $request->nombre;
       // $club->matricula = $request->matricula;
       $club->fecha_fundacion = $request->fecha_fundacion;
       $club->logo = $logo;
       $club->presidente = $request->presidente;
       $club->tipo_club=$request->tipo_club;

       $club->save();
       return redirect('/club')->with('message','Datos guardados');

   }

   /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Club  $club
    * @return \Illuminate\Http\Response
    */
   public function destroy($club)
   {
       Club::where('id_club', $club)->update(
           array(
               'estado'=>0,
           )
       );
        return redirect('/club');
   }
}
