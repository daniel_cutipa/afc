<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Temporada;


class ReporteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $temporadas = Temporada::orderBy('id_temporada', 'desc')->where('estado', 1)->get();
        return view('reporte.index', compact('temporadas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_temporada){
        $goleadores = DB::select('
            select j.matricula, concat_ws(" ", j.ap_paterno, j.ap_materno, j.nombre) as nombre, sum(o.observacion) as goles, c.nombre as club
            from fase as f, grupo as g, fecha as fe, partido as p, observacion as o, jugador as j, club as c
            where f.id_temporada = "'.$id_temporada.'"
            and f.id_fase = g.id_fase
            and g.id_grupo = fe.id_grupo
            and fe.id_fecha = p.id_fecha
            and p.id_partido = o.id_partido
            and o.id_tipo_observacion = 1
            and o.id_jugador = j.id_jugador
            and j.id_club = c.id_club
            group by j.id_jugador
            order by goles desc
            ');
        $rojas = DB::select('
            select j.matricula, concat_ws(" ", j.ap_paterno, j.ap_materno, j.nombre) as nombre, count(o.id_observacion) as roja, c.nombre as club
            from fase as f, grupo as g, fecha as fe, partido as p, observacion as o, jugador as j, club as c
            where f.id_temporada = "'.$id_temporada.'"
            and f.id_fase = g.id_fase
            and g.id_grupo = fe.id_grupo
            and fe.id_fecha = p.id_fecha
            and p.id_partido = o.id_partido
            and o.id_tipo_observacion = 2
            and o.id_jugador = j.id_jugador
            and j.id_club = c.id_club
            group by j.id_jugador
            order by roja desc
            ');
        $amarillas = DB::select('
            select j.matricula, concat_ws(" ", j.ap_paterno, j.ap_materno, j.nombre) as nombre, count(o.id_observacion) as amarilla, c.nombre as club
            from fase as f, grupo as g, fecha as fe, partido as p, observacion as o, jugador as j, club as c
            where f.id_temporada = "'.$id_temporada.'"
            and f.id_fase = g.id_fase
            and g.id_grupo = fe.id_grupo
            and fe.id_fecha = p.id_fecha
            and p.id_partido = o.id_partido
            and o.id_tipo_observacion = 3
            and o.id_jugador = j.id_jugador
            and j.id_club = c.id_club
            group by j.id_jugador
            ');
        return response()->json([
            'goleadores' => $goleadores,
            'rojas' => $rojas,
            'amarillas' => $amarillas,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function graficos(){
        $temporadas = Temporada::orderBy('id_temporada', 'desc')->where('estado', 1)->get();
        return view('reporte.graficos', compact('temporadas'));
    }

    public function datosGraficos($id_temporada){
        // LISTA EQUIPOS DE LA TEMPORADA
        $list_equipos = DB::select('
            select distinct c.nombre, e.id_equipo
            from fase as f, grupo_equipo as ge, grupo as g, equipo as e, club as c
            where f.id_temporada = '.$id_temporada.'
            and f.id_fase = g.id_fase
            and g.id_grupo = ge.id_grupo
            and ge.id_equipo = e.id_equipo
            and e.id_club = c.id_club
            order by c.nombre
            ');
        // LISTA LOS PARTIDOS QUE SE AN JUGADO en una temporada
        $list_partidos = DB::select('
            select p.id_equipo1, p.id_equipo2, p.goles_equi1, p.goles_equi2
            from fase as fa, grupo as g, fecha as fe, partido as p 
            where fa.id_temporada = '.$id_temporada.'
            and fa.id_fase = g.id_fase
            and g.id_grupo = fe.id_grupo
            and fe.id_fecha = p.id_fecha
            and p.jugado = 1
            ');
        $goleadores = DB::select('
            select sum(o.observacion) as goles, c.nombre as club
            from fase as f, grupo as g, fecha as fe, partido as p, observacion as o, jugador as j, club as c
            where f.id_temporada = '.$id_temporada.'
            and f.id_fase = g.id_fase
            and g.id_grupo = fe.id_grupo
            and fe.id_fecha = p.id_fecha
            and p.id_partido = o.id_partido
            and o.id_tipo_observacion = 1
            and o.id_jugador = j.id_jugador
            and j.id_club = c.id_club
            group by club
            ');
        $rojas = DB::select('
            select count(o.id_observacion) as roja, c.nombre as club
            from fase as f, grupo as g, fecha as fe, partido as p, observacion as o, jugador as j, club as c
            where f.id_temporada = '.$id_temporada.'
            and f.id_fase = g.id_fase
            and g.id_grupo = fe.id_grupo
            and fe.id_fecha = p.id_fecha
            and p.id_partido = o.id_partido
            and o.id_tipo_observacion = 2
            and o.id_jugador = j.id_jugador
            and j.id_club = c.id_club
            group by club
            ');
        $amarillas = DB::select('
            select count(o.id_observacion) as amarilla, c.nombre as club
            from fase as f, grupo as g, fecha as fe, partido as p, observacion as o, jugador as j, club as c
            where f.id_temporada = '.$id_temporada.'
            and f.id_fase = g.id_fase
            and g.id_grupo = fe.id_grupo
            and fe.id_fecha = p.id_fecha
            and p.id_partido = o.id_partido
            and o.id_tipo_observacion = 3
            and o.id_jugador = j.id_jugador
            and j.id_club = c.id_club
            group by club
            ');
        $partidos = collect([]);
        foreach ($list_partidos as $p => $partido) {
            $partidos->push([
                [
                    'id_equipo' =>$partido->id_equipo1, 
                    'gf' => $partido->goles_equi1,
                    'gc' => $partido->goles_equi2,
                ],
            ]);
            $partidos->push([
                [
                    'id_equipo' =>$partido->id_equipo2, 
                    'gf' => $partido->goles_equi2,
                    'gc' => $partido->goles_equi1,
                ],
            ]);
        }
        foreach ($list_equipos as $e => $equipo) {
            $gf = 0;
            $gc = 0;
            foreach ($partidos as $p => $part) {
                if ($equipo->id_equipo == $part[0]['id_equipo']) {
                    $gf += $part[0]['gf'];
                    $gc += $part[0]['gc'];
                }
            }
            $equipo->gf = $gf;
            $equipo->gc = $gc;
            foreach ($rojas as $r => $roja) {
                if ($equipo->nombre == $roja->club) {
                    $equipo->roja = $roja->roja;
                    break;
                }
            }
            foreach ($amarillas as $a => $amarilla) {
                if ($equipo->nombre == $amarilla->club) {
                    $equipo->amarilla = $amarilla->amarilla;
                    break;
                }
            }
        }
        return response()->json([
            'list_equipos' => $list_equipos,
            'goleadores' => $goleadores,
        ]);
    }





            

}
