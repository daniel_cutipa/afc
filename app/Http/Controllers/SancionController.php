<?php

namespace App\Http\Controllers;

use App\Sancion;
use App\Temporada;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SancionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sanciones = Sancion::where('estado', 1)->orderBy('id_sancion', 'desc')->get();
        return view('sancion.index', compact('sanciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $temporadas = Temporada::orderBy('id_temporada', 'desc')->where('estado', 1)->get();
        return view('sancion.create', compact('temporadas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'id_jugador' => 'required|integer',
            'descripcion' => 'required|string',
            // 'fecha_inicio' => 'required|date',
            // 'fecha_fin' => 'required|date',
            // 'sancion_economica' => 'required|integer',
            // 'numero_partidos' => 'required|integer',
        ]);
        Sancion::create([
            'id_jugador' => $request->id_jugador,
            'descripcion' => $request->descripcion,
            'fecha_inicio' => $request->fecha_inicio,
            'fecha_fin' => $request->fecha_fin,
            'sancion_economica' => $request->sancion_economica,
            'numero_partidos' => $request->numero_partidos,
            'fecha' => today(),
            'estado' => 1,
        ]);
        return response()->json([
            'message' => 'Sancion agregada correctamente!',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Suspension  $suspension
     * @return \Illuminate\Http\Response
     */
    public function show(Sancion $sancion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Suspension  $suspension
     * @return \Illuminate\Http\Response
     */
    public function edit(Sancion $sancion)
    {
        return response()->json([
            'sancion' => $sancion,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Suspension  $suspension
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sancion $sancion)
    {
        $this->validate($request, [
        'descripcion' => 'required|string',
        // 'fecha_inicio' => 'required|date',
        // 'fecha_fin' => 'required|date',
        // 'sancion_economica' => 'required|integer',
        // 'numero_partidos' => 'required|integer',
        ]);
        $sancion->descripcion = $request->descripcion;
        $sancion->fecha_inicio = $request->fecha_inicio;
        $sancion->fecha_fin = $request->fecha_fin;
        $sancion->sancion_economica = $request->sancion_economica;
        $sancion->numero_partidos = $request->numero_partidos;
        $sancion->save();
        return response()->json([
            'message' => 'Sancion modificada correctamente!',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Suspension  $suspension
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sancion $sancion)
    {
        $sancion->estado = 0;
        $sancion->save();
        // EN LA TABLA EQUIPO_JUGADOR CAMBIAR EL VALOR DE LA COLUMNA ES_ESPERA
        DB::table('equipo_jugador')->where('id_jugador', $sancion->id_jugador)->update(['en_espera' => 0]);
        return redirect()->back();
    }

    public function getJugadores($id_temporada){
        $jugadores = DB::select('
            select distinct j.id_jugador, j.matricula, concat_ws(" ", j.ap_paterno, j.ap_materno, j.nombre) as nombre, ej.en_espera, c.nombre as club
            from fase as f, grupo as g, grupo_equipo as ge, equipo_jugador as ej, jugador as j, club as c
            where f.id_temporada = '.$id_temporada.'
            and f.id_fase = g.id_fase
            and g.id_grupo = ge.id_grupo
            and ge.id_equipo = ej.id_equipo
            and ej.id_jugador = j.id_jugador
            and j.id_club = c.id_club
            order by club, nombre
            ');
        foreach ($jugadores as $j => $jugador) {
            $tarjeta_roja = DB::select('
                select o.id_observacion
                from fase as f, grupo as g, fecha as fe, partido as p, observacion as o, tipo_observacion as t_o
                where f.id_temporada = '.$id_temporada.'
                and f.id_fase = g.id_fase
                and g.id_grupo = fe.id_grupo
                and fe.id_fecha = p.id_fecha
                and p.id_partido = o.id_partido
                and o.id_tipo_observacion = t_o.id_tipo_observacion
                and t_o.tipo = "tarjeta roja"
                and o.id_jugador = '.$jugador->id_jugador.'
                ');
            $jugadores[$j]->roja = collect($tarjeta_roja)->count();
            $tarjeta_amarilla = DB::select('
                select o.id_observacion
                from fase as f, grupo as g, fecha as fe, partido as p, observacion as o, tipo_observacion as t_o
                where f.id_temporada = '.$id_temporada.'
                and f.id_fase = g.id_fase
                and g.id_grupo = fe.id_grupo
                and fe.id_fecha = p.id_fecha
                and p.id_partido = o.id_partido
                and o.id_tipo_observacion = t_o.id_tipo_observacion
                and t_o.tipo = "tarjeta amarilla"
                and o.id_jugador = '.$jugador->id_jugador.'
                ');
            $jugadores[$j]->amarilla = collect($tarjeta_amarilla)->count();
        }
        $jugadores = (collect($jugadores)->sortByDesc('amarilla'))->values();
        return response()->json([
            'jugadores' => $jugadores,
        ]);
    }
    public function searchJugadores($id_temporada, $txt){
        $jugadores = DB::select('
            select distinct j.id_jugador, j.matricula, concat_ws(" ", j.ap_paterno, j.ap_materno, j.nombre) as nombre, ej.en_espera, c.nombre as club
            from fase as f, grupo as g, grupo_equipo as ge, equipo_jugador as ej, jugador as j, club as c
            where f.id_temporada = '.$id_temporada.'
            and f.id_fase = g.id_fase
            and g.id_grupo = ge.id_grupo
            and ge.id_equipo = ej.id_equipo
            and ej.id_jugador = j.id_jugador
            and (j.nombre like "%'.$txt.'%" or j.ap_paterno like "%'.$txt.'%" or j.matricula = "'.$txt.'")
            and j.id_club = c.id_club
            order by club, nombre
            ');
        foreach ($jugadores as $j => $jugador) {
            $tarjeta_roja = DB::select('
                select o.id_observacion
                from fase as f, grupo as g, fecha as fe, partido as p, observacion as o, tipo_observacion as t_o
                where f.id_temporada = '.$id_temporada.'
                and f.id_fase = g.id_fase
                and g.id_grupo = fe.id_grupo
                and fe.id_fecha = p.id_fecha
                and p.id_partido = o.id_partido
                and o.id_tipo_observacion = t_o.id_tipo_observacion
                and t_o.tipo = "tarjeta roja"
                and o.id_jugador = '.$jugador->id_jugador.'
                ');
            $jugadores[$j]->roja = collect($tarjeta_roja)->count();
            $tarjeta_amarilla = DB::select('
                select o.id_observacion
                from fase as f, grupo as g, fecha as fe, partido as p, observacion as o, tipo_observacion as t_o
                where f.id_temporada = '.$id_temporada.'
                and f.id_fase = g.id_fase
                and g.id_grupo = fe.id_grupo
                and fe.id_fecha = p.id_fecha
                and p.id_partido = o.id_partido
                and o.id_tipo_observacion = t_o.id_tipo_observacion
                and t_o.tipo = "tarjeta amarilla"
                and o.id_jugador = '.$jugador->id_jugador.'
                ');
            $jugadores[$j]->amarilla = collect($tarjeta_amarilla)->count();
        }
        $jugadores = (collect($jugadores)->sortByDesc('amarilla'))->values();
        return response()->json([
            'jugadores' => $jugadores,
        ]);
    }
}
