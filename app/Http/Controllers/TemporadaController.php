<?php
//http://hdtuto.com/article/ajax-crud-example-in-laravel-55-application
//https://www.jmkleger.com/post/ajax-crud-for-laravel-5-4
// https://www.youtube.com/watch?v=ZWhKNRL4-Lo
namespace App\Http\Controllers;

use App\Temporada;
use App\Club;
use App\Categoria;
use App\Fase;
use App\Grupo;
use App\Fecha;
use App\Partido;
use App\Observacion;
use App\Suspension;
use App\Arbitro;
use App\Delegado;
use App\Planillero;
use App\Veedor;
use App\Equipo_jugador;
use DB;
use Illuminate\Http\Request;

class TemporadaController extends Controller
{
    protected $rules =
    [
        'id_categoria' => 'required|integer',
        'gestion' => 'required|string|max:45',
        'torneo' => 'required|string|max:45',
        'fecha_inicio' => 'required|date',
        // 'fecha_fin' => 'required|date',
        // 'num_equipos' => 'required|integer',
        // 'num_grupos' => 'required|integer',
        
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $temporadas = Temporada::orderBy('id_temporada', 'desc')->where('estado', 1)->get();
        return view('temporada.index', compact('temporadas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = Categoria::orderBy('id_categoria', 'desc')->where('estado', 1)->get();
        return response()->json($categorias);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
        DB::beginTransaction();
        try {
            // guarda la temporada y saca el dato que se registro
            $temporada = Temporada::create([
                'id_categoria' => $request['id_categoria'],
                'gestion' => $request['gestion'],
                'torneo' => $request['torneo'],
                'fecha_inicio' => $request['fecha_inicio'],
                'fecha_fin' => $request['fecha_fin'],
                'estado' => 1,
            ]);
            // insertar la Primera Fase
            $id_fase = DB::table('fase')->insertGetId([
                'id_temporada' => $temporada->id_temporada,
                'fase' => 'Primera Fase',
            ]);
            // buscar todos los equipos que estan en esta categoria
            $result_id_equipos = DB::select('
                select e.id_equipo
                from temporada as t, categoria as c, equipo as e 
                where t.id_temporada = '.$temporada->id_temporada.' 
                    and t.id_categoria = c.id_categoria
                    and c.id_categoria = e.id_categoria
                ');
            //la colleccion es aleatorio con shuffle
            $id_equipos = collect($result_id_equipos)->shuffle();
            // $id_equipos = collect($result_id_equipos);
            // recorre la cantidad de grupos a crear
            $j = 0;// variable para agregar en grupo_equipos
            // partimos los equipos en grupos
            $groups = $id_equipos->split($request->num_grupos);
            for ($i=0; $i < $request->num_grupos; $i++) { 
                //inserta un grupo y saca el id del ultimo insertado
                $id_grupo = DB::table('grupo')->insertGetId([
                    'id_fase' => $id_fase,
                    'grupo' => 'Grupo '.($i+1),
                ]);

                for ($n=0; $n < ($request->num_equipos/$request->num_grupos); $n++) {
                    DB::table('grupo_equipo')->insert([
                        'id_grupo' => $id_grupo, 
                        'id_equipo' => $id_equipos[$j]->id_equipo,
                    ]);
                    $j++;
                }
                if (($request->num_equipos/$request->num_grupos) % 2 == 0) {
                    // insertar el fechas, con equipos pares
                    $this->equiposPar($id_grupo, ($groups->get($i))->values());
                    $this->equiposPar($id_grupo, ($groups->get($i))->values());
                }
                else {
                    // insertar el fechas para ida, con equipos impares
                    $this->equiposImpar($id_grupo, ($groups->get($i))->values());
                    $this->equiposImpar($id_grupo, ($groups->get($i))->values());
                }
            }
            DB::commit();
            return response()->json([
                'message' =>'Torneo ' . $temporada->torneo . ' fue agregado correctamente!',
                'id_temporada' => $temporada->id_temporada,
                'gestion' => $temporada->gestion,
                'torneo' => $temporada->torneo,
                'fecha_inicio' => $temporada->fecha_inicio,
                'fecha_fin' => $temporada->fecha_fin,
            ]);
        } catch (Exception $e) {
            DB::rollback();
        }
    }
    private function equiposPar($id_grupo, $id_equipos){
        // ciclo para agregar fechas de ida
        for ($i=0; $i < ($id_equipos->count())-1; $i++) { 
            $id_fecha = DB::table('fecha')->insertGetId(['id_grupo' => $id_grupo, 'fecha' => today()]);
            // inserta el total de partidos por fecha
            $id_equipo1 = $id_equipos[0]->id_equipo;
            $id_equipo2 = $id_equipos[1]->id_equipo;
            $e1 = 2;
            $e2 = $id_equipos->count()-1;
            for ($j=0; $j < ($id_equipos->count())/2; $j++) { 
                Partido::create([
                    'id_equipo1' => $id_equipo1, 
                    'id_equipo2' => $id_equipo2, 
                    'id_cancha' => NULL, 
                    'id_fecha' => $id_fecha, 
                    'hora' => NULL,
                    'ptos_equi1' => 0,
                    'ptos_equi2' => 0,
                    'goles_equi1' => 0,
                    'goles_equi2' => 0,
                    'jugado' => 0,
                ]);
                $id_equipo1 = $id_equipos[$e1]->id_equipo;
                $id_equipo2 = $id_equipos[$e2]->id_equipo;
                $e1 += 1;
                $e2 -= 1;
            }
            //organizar el orden de los equipos
            $aux = $id_equipos[1];
            for ($k=1; $k < $id_equipos->count()-1; $k++) { 
                $id_equipos[$k] = $id_equipos[$k+1];
            }
            $id_equipos[$id_equipos->count()-1] = $aux;
        }
    }
    private function equiposImpar($id_grupo, $id_equipos){
        // ciclo para agregar fechas de ida
        for ($i=0; $i < $id_equipos->count(); $i++) { 
            $id_fecha = DB::table('fecha')->insertGetId(['id_grupo' => $id_grupo, 'fecha' => today()]);
            // inserta el total de partidos por fecha
            $e1 = 1;
            $e2 = $id_equipos->count()-1;
            for ($j=0; $j < ($id_equipos->count() - 1)/2; $j++) { 
                $id_equipo1 = $id_equipos[$e1]->id_equipo;
                $id_equipo2 = $id_equipos[$e2]->id_equipo;
                Partido::create([
                    'id_equipo1' => $id_equipo1, 
                    'id_equipo2' => $id_equipo2, 
                    'id_cancha' => NULL, 
                    'id_fecha' => $id_fecha, 
                    'hora' => NULL,
                    'ptos_equi1' => 0,
                    'ptos_equi2' => 0,
                    'goles_equi1' => 0,
                    'goles_equi2' => 0,
                    'jugado' => 0,
                ]);
                $e1 += 1;
                $e2 -= 1;
            }
            //organizar el orden de los equipos
            $aux = $id_equipos[0];
            for ($k=0; $k < $id_equipos->count(); $k++) { 
                $id_equipos[$k] = $id_equipos[$k+1];
            }
            $id_equipos[$id_equipos->count()-1] = $aux;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Temporada  $temporada
     * @return \Illuminate\Http\Response
     */
    public function show(Temporada $temporada)
    {
        $fase_p = Fase::where('id_temporada', $temporada->id_temporada)
            ->where('fase', 'Primera Fase')
            ->first();
        $grupos_p = Grupo::where('id_fase', $fase_p->id_fase)->get();
        foreach ($grupos_p as $g => $grupo) {
            $fechas_p[$g] = DB::select('
            select f.id_fecha, f.fecha, f.id_grupo
            from grupo as g, fecha as f
            where g.id_grupo = '.$grupo->id_grupo.'
            and g.id_grupo = f.id_grupo
            ');
            foreach ($fechas_p[$g] as $f => $fecha) {
                $fechas_p[$g][$f]->partidos = DB::select('
                    select p.id_partido, p.id_cancha, p.hora , p.id_equipo1, p.id_equipo2, c1.nombre as equipo1, c2.nombre as equipo2, c1.logo as logo1, c2.logo as logo2, p.goles_equi1, p.goles_equi2, p.jugado
                    from fecha as f, partido as p, equipo as e1, equipo as e2, club as c1, club as c2 
                    where f.id_fecha = '.$fecha->id_fecha.'
                    and f.id_fecha = p.id_fecha
                    and p.id_equipo1 = e1.id_equipo
                    and e1.id_club = c1.id_club
                    and p.id_equipo2 = e2.id_equipo
                    and e2.id_club = c2.id_club
                    order by p.hora asc
                    ');
            }
        }
        // $fase_s = Fase::where('id_temporada', $temporada->id_temporada)
        //     ->where('fase', 'Segunda Fase')
        //     ->first();
        $fase_s = Fase::where('id_temporada', $temporada->id_temporada)
            ->whereIn('fase', ['Segunda Fase', 'Semi Final'])
            ->first();
        if ($fase_s != NULL) {
            $grupos_s = Grupo::where('id_fase', $fase_s->id_fase)->get();
            foreach ($grupos_s as $g => $grupo) {
                $fechas_s[$g] = DB::select('
                select f.id_fecha, f.fecha, f.id_grupo
                from grupo as g, fecha as f
                where g.id_grupo = '.$grupo->id_grupo.'
                and g.id_grupo = f.id_grupo
                ');
                foreach ($fechas_s[$g] as $f => $fecha) {
                    $fechas_s[$g][$f]->partidos = DB::select('
                        select p.id_partido, p.id_cancha, p.hora , p.id_equipo1, p.id_equipo2, c1.nombre as equipo1, c2.nombre as equipo2, c1.logo as logo1, c2.logo as logo2, p.goles_equi1, p.goles_equi2, p.jugado
                        from fecha as f, partido as p, equipo as e1, equipo as e2, club as c1, club as c2 
                        where f.id_fecha = '.$fecha->id_fecha.'
                        and f.id_fecha = p.id_fecha
                        and p.id_equipo1 = e1.id_equipo
                        and e1.id_club = c1.id_club
                        and p.id_equipo2 = e2.id_equipo
                        and e2.id_club = c2.id_club
                        order by p.hora asc
                        ');
                }
            }
            $fase_f = Fase::where('id_temporada', $temporada->id_temporada)
                ->where('fase', 'Final')
                ->first();
            if ($fase_f != NULL) {
                $grupos_f = Grupo::where('id_fase', $fase_f->id_fase)->get();
                foreach ($grupos_f as $g => $grupo) {
                    $fechas_f[$g] = DB::select('
                    select f.id_fecha, f.fecha, f.id_grupo
                    from grupo as g, fecha as f
                    where g.id_grupo = '.$grupo->id_grupo.'
                    and g.id_grupo = f.id_grupo
                    ');
                    foreach ($fechas_f[$g] as $f => $fecha) {
                        $fechas_f[$g][$f]->partidos = DB::select('
                            select p.id_partido, p.id_cancha, p.hora , p.id_equipo1, p.id_equipo2, c1.nombre as equipo1, c2.nombre as equipo2, c1.logo as logo1, c2.logo as logo2, p.goles_equi1, p.goles_equi2, p.jugado
                            from fecha as f, partido as p, equipo as e1, equipo as e2, club as c1, club as c2 
                            where f.id_fecha = '.$fecha->id_fecha.'
                            and f.id_fecha = p.id_fecha
                            and p.id_equipo1 = e1.id_equipo
                            and e1.id_club = c1.id_club
                            and p.id_equipo2 = e2.id_equipo
                            and e2.id_club = c2.id_club
                            order by p.hora asc
                            ');
                    }
                }
            }
            else{ $grupos_f = NULL; $fechas_f = NULL; }
        }
        else{ $grupos_s = NULL; $fechas_s = NULL; $fase_f = NULL; $grupos_f = NULL; $fechas_f = NULL;}
        
        $canchas = DB::select('
            select id_cancha, nombre
            from cancha
            where estado = 1
            ');
        return view('temporada.fase')->with([
            'temporada' => $temporada,
            'fase_p' => $fase_p,
            'grupos_p' => $grupos_p,
            'fechas_p' => $fechas_p,
            'fase_s' => $fase_s,
            'grupos_s' => $grupos_s,
            'fechas_s' => $fechas_s,
            'fase_f' => $fase_f,
            'grupos_f' => $grupos_f,
            'fechas_f' => $fechas_f,
            'canchas' => $canchas,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Temporada  $temporada
     * @return \Illuminate\Http\Response
     */
    public function edit(Temporada $temporada)
    {
        $temporada->categorias = Categoria::where('estado', 1)->get();
        return response()->json($temporada);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Temporada  $temporada
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Temporada $temporada)
    {
        $this->validate($request, $this->rules);
        $temporada->gestion = $request->gestion;
        $temporada->torneo = $request->torneo;
        $temporada->fecha_inicio = $request->fecha_inicio;
        $temporada->fecha_fin = $request->fecha_fin;
        $temporada->save();
        return response()->json([
            'message' =>'Torneo ' . $temporada->torneo . ' fue modificado correctamente!',
            'id_temporada' => $temporada->id_temporada,
            'gestion' => $temporada->gestion,
            'torneo' => $temporada->torneo,
            'fecha_inicio' => $temporada->fecha_inicio,
            'fecha_fin' => $temporada->fecha_fin,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Temporada  $temporada
     * @return \Illuminate\Http\Response
     */
    public function destroy(Temporada $temporada){
        $temporada->estado = 0;
        $temporada->save();
        return response()->json([
            'message' => 'Torneo ' . $temporada->torneo . ' fue eliminado correctamente',
        ]);
    }

    // cuenta la cantidad de equipos que pertenece a una categoria en especifico
    public function countEquipos($id_categoria){
        $count = DB::select('select count(e.id_equipo) as cantidad from categoria as c, equipo as e where c.id_categoria = e.id_categoria and c.id_categoria = '.$id_categoria);
        return response()->json(['count' => $count[0]->cantidad]);
    }
    // retorna como un fixture de un grupo en especifico
    public function getFases(Request $request){
        $fase_p = Fase::select('fase', 'id_fase')
            ->where('id_temporada', $request->id_temporada)
            ->where('fase', 'Primera Fase')
            ->first();
        $grupos_p = Grupo::where('id_fase', $fase_p->id_fase)->get();
        foreach ($grupos_p as $g => $grupo) {
            $fechas_p[$g] = DB::select('
                select f.id_fecha, f.fecha, f.id_grupo
                from grupo as g, fecha as f
                where g.id_grupo = '.$grupo->id_grupo.'
                and g.id_grupo = f.id_grupo
                ');
            foreach ($fechas_p[$g] as $f => $fecha) {
                $fechas_p[$g][$f]->partidos = DB::select('
                    select p.id_partido, p.id_cancha, p.hora , p.id_equipo1, p.id_equipo2, c1.nombre as equipo1, c2.nombre as equipo2, c1.logo as logo1, c2.logo as logo2
                    from fecha as f, partido as p, equipo as e1, equipo as e2, club as c1, club as c2 
                    where f.id_fecha = '.$fecha->id_fecha.'
                    and f.id_fecha = p.id_fecha
                    and p.id_equipo1 = e1.id_equipo
                    and e1.id_club = c1.id_club
                    and p.id_equipo2 = e2.id_equipo
                    and e2.id_club = c2.id_club
                    ');
                foreach ($fechas_p[$g][$f]->partidos as $p => $partido) {
                    $fechas_p[$g][$f]->partidos[$p]->equi1 = DB::select('
                        select sum(o.observacion) as goles
                        from partido as p, observacion as o, tipo_observacion as t, equipo as e, equipo_jugador as eq, jugador as j
                        where p.id_partido = '.$partido->id_partido.'
                        and e.id_equipo = '.$partido->id_equipo1.'
                        and p.id_partido = o.id_partido
                        and o.id_tipo_observacion = t.id_tipo_observacion
                        and t.tipo = "gol"
                        and e.id_equipo = eq.id_equipo
                        and eq.id_jugador = j.id_jugador
                        and j.id_jugador = o.id_jugador
                        ');
                    $fechas_p[$g][$f]->partidos[$p]->equi2 = DB::select('
                        select sum(o.observacion) as goles
                        from partido as p, observacion as o, tipo_observacion as t, equipo as e, equipo_jugador as eq, jugador as j
                        where p.id_partido = '.$partido->id_partido.'
                        and e.id_equipo = '.$partido->id_equipo2.'
                        and p.id_partido = o.id_partido
                        and o.id_tipo_observacion = t.id_tipo_observacion
                        and t.tipo = "gol"
                        and e.id_equipo = eq.id_equipo
                        and eq.id_jugador = j.id_jugador
                        and j.id_jugador = o.id_jugador
                        ');
                }
            }
        }
        $fase_s = Fase::select('fase', 'id_fase')
            ->where('id_temporada', $request->id_temporada)
            ->whereIn('fase', ['Segunda Fase', 'Semi Final'])
            ->first();
        if ($fase_s != NULL) {
            $grupos_s = Grupo::where('id_fase', $fase_s->id_fase)->get();
            foreach ($grupos_s as $g => $grupo) {
                $fechas_s[$g] = DB::select('
                    select f.id_fecha, f.fecha, f.id_grupo
                    from grupo as g, fecha as f
                    where g.id_grupo = '.$grupo->id_grupo.'
                    and g.id_grupo = f.id_grupo
                    ');
                foreach ($fechas_s[$g] as $f => $fecha) {
                    $fechas_s[$g][$f]->partidos = DB::select('
                        select p.id_partido, p.id_cancha, p.hora , p.id_equipo1, p.id_equipo2, c1.nombre as equipo1, c2.nombre as equipo2, c1.logo as logo1, c2.logo as logo2
                        from fecha as f, partido as p, equipo as e1, equipo as e2, club as c1, club as c2 
                        where f.id_fecha = '.$fecha->id_fecha.'
                        and f.id_fecha = p.id_fecha
                        and p.id_equipo1 = e1.id_equipo
                        and e1.id_club = c1.id_club
                        and p.id_equipo2 = e2.id_equipo
                        and e2.id_club = c2.id_club
                        ');
                    foreach ($fechas_s[$g][$f]->partidos as $p => $partido) {
                        $fechas_s[$g][$f]->partidos[$p]->equi1 = DB::select('
                            select sum(o.observacion) as goles
                            from partido as p, observacion as o, tipo_observacion as t, equipo as e, equipo_jugador as eq, jugador as j
                            where p.id_partido = '.$partido->id_partido.'
                            and e.id_equipo = '.$partido->id_equipo1.'
                            and p.id_partido = o.id_partido
                            and o.id_tipo_observacion = t.id_tipo_observacion
                            and t.tipo = "gol"
                            and e.id_equipo = eq.id_equipo
                            and eq.id_jugador = j.id_jugador
                            and j.id_jugador = o.id_jugador
                            ');
                        $fechas_s[$g][$f]->partidos[$p]->equi2 = DB::select('
                            select sum(o.observacion) as goles
                            from partido as p, observacion as o, tipo_observacion as t, equipo as e, equipo_jugador as eq, jugador as j
                            where p.id_partido = '.$partido->id_partido.'
                            and e.id_equipo = '.$partido->id_equipo2.'
                            and p.id_partido = o.id_partido
                            and o.id_tipo_observacion = t.id_tipo_observacion
                            and t.tipo = "gol"
                            and e.id_equipo = eq.id_equipo
                            and eq.id_jugador = j.id_jugador
                            and j.id_jugador = o.id_jugador
                            ');
                    }
                }
            }
            $fase_f = Fase::select('fase', 'id_fase')
                ->where('id_temporada', $request->id_temporada)
                ->where('fase', 'Final')
                ->first();
            if ($fase_f != NULL) {
                $grupos_f = Grupo::where('id_fase', $fase_f->id_fase)->get();
                foreach ($grupos_f as $g => $grupo) {
                    $fechas_f[$g] = DB::select('
                        select f.id_fecha, f.fecha, f.id_grupo
                        from grupo as g, fecha as f
                        where g.id_grupo = '.$grupo->id_grupo.'
                        and g.id_grupo = f.id_grupo
                        ');
                    foreach ($fechas_f[$g] as $f => $fecha) {
                        $fechas_f[$g][$f]->partidos = DB::select('
                            select p.id_partido, p.id_cancha, p.hora , p.id_equipo1, p.id_equipo2, c1.nombre as equipo1, c2.nombre as equipo2, c1.logo as logo1, c2.logo as logo2
                            from fecha as f, partido as p, equipo as e1, equipo as e2, club as c1, club as c2 
                            where f.id_fecha = '.$fecha->id_fecha.'
                            and f.id_fecha = p.id_fecha
                            and p.id_equipo1 = e1.id_equipo
                            and e1.id_club = c1.id_club
                            and p.id_equipo2 = e2.id_equipo
                            and e2.id_club = c2.id_club
                            ');
                        foreach ($fechas_f[$g][$f]->partidos as $p => $partido) {
                            $fechas_f[$g][$f]->partidos[$p]->equi1 = DB::select('
                                select sum(o.observacion) as goles
                                from partido as p, observacion as o, tipo_observacion as t, equipo as e, equipo_jugador as eq, jugador as j
                                where p.id_partido = '.$partido->id_partido.'
                                and e.id_equipo = '.$partido->id_equipo1.'
                                and p.id_partido = o.id_partido
                                and o.id_tipo_observacion = t.id_tipo_observacion
                                and t.tipo = "gol"
                                and e.id_equipo = eq.id_equipo
                                and eq.id_jugador = j.id_jugador
                                and j.id_jugador = o.id_jugador
                                ');
                            $fechas_f[$g][$f]->partidos[$p]->equi2 = DB::select('
                                select sum(o.observacion) as goles
                                from partido as p, observacion as o, tipo_observacion as t, equipo as e, equipo_jugador as eq, jugador as j
                                where p.id_partido = '.$partido->id_partido.'
                                and e.id_equipo = '.$partido->id_equipo2.'
                                and p.id_partido = o.id_partido
                                and o.id_tipo_observacion = t.id_tipo_observacion
                                and t.tipo = "gol"
                                and e.id_equipo = eq.id_equipo
                                and eq.id_jugador = j.id_jugador
                                and j.id_jugador = o.id_jugador
                                ');
                        }
                    }
                }
            }
            else{ $grupos_f = NULL; $fechas_f = NULL; }
        }
        else{ $grupos_s = NULL; $fechas_s = NULL; $fase_f = NULL; $grupos_f = NULL; $fechas_f = NULL;}
        $canchas = DB::select('
            select id_cancha, nombre
            from cancha
            where estado = 1
            ');
        return response()->json([
            'fase_p' => $fase_p,
            'grupos_p' => $grupos_p,
            'fechas_p' => $fechas_p,
            'fase_s' => $fase_s,
            'grupos_s' => $grupos_s,
            'fechas_s' => $fechas_s,
            'fase_f' => $fase_f,
            'grupos_f' => $grupos_f,
            'fechas_f' => $fechas_f,
            'canchas' => $canchas,
        ]);
    }

    public function updateGrupo(Request $request){
        $this->validate($request, [
            'campo' => 'required|string|max:45',
        ]);
        Grupo::where('id_grupo', $request->id)->update(array(
            'grupo' => $request->campo,
        ));
        return response()->json([
            'message' => 'Grupo ' . $request->campo . ' fue modificado correctamente!',
        ]);
    }

    public function updateFecha(Request $request){
        $this->validate($request, [
            'campo' => 'required|date',
        ]);
        Fecha::where('id_fecha', $request->id)->update(array(
            'fecha' => $request->campo,
        ));
        return response()->json([
            'message' => 'Fecha ' . $request->campo . ' fue modificado correctamente!',
        ]);
    }

    public function updateHora(Request $request){
        Partido::where('id_partido', $request->id)->update(array(
            'hora' => $request->campo,
        ));
        return response()->json([
            'message' => 'Hora ' . $request->campo . ' fue modificado correctamente!',
        ]);
    }

    public function updateCancha(Request $request){
        Partido::where('id_partido', $request->id)->update(array(
            'id_cancha' => $request->campo,
        ));
        return response()->json([
            'message' => 'Cancha fue agregado correctamente!',
        ]);
    }

    public function planilla($id_partido){
        $partido = DB::select('
            select id_equipo1, id_equipo2, jugado, ptos_equi1, ptos_equi2
            from partido 
            where id_partido = '.$id_partido
            );
        // saca los nombres de los equipos
        $club1 = DB::select('
            select c.nombre, c.logo, e.id_equipo
            from equipo as e, club as c
            where e.id_equipo = '.$partido[0]->id_equipo1.'
            and e.id_club = c.id_club
            ');
        $club2 = DB::select('
            select c.nombre, c.logo, e.id_equipo
            from equipo as e, club as c
            where e.id_equipo = '.$partido[0]->id_equipo2.'
            and e.id_club = c.id_club
            ');
        // MANDAR LOS JUGADORES QUE PARTICIPAN
        $titulares1 = DB::select('
            select j.id_jugador, concat_ws(" ", j.ap_paterno, j.ap_materno, j.nombre) as nombre, j.matricula, l.numero
            from log_partido as l, jugador as j
            where l.id_partido = '.$id_partido.'
            and l.id_equipo = '.$partido[0]->id_equipo1.'
            and l.id_jugador = j.id_jugador
            and l.titular = 1
            ');
        $suplentes1 = DB::select('
            select j.id_jugador, concat_ws(" ", j.ap_paterno, j.ap_materno, j.nombre) as nombre, j.matricula, l.numero, l.numero_s
            from log_partido as l, jugador as j
            where l.id_partido = '.$id_partido.'
            and l.id_equipo = '.$partido[0]->id_equipo1.'
            and l.id_jugador = j.id_jugador
            and l.titular = 0
            ');
        $titulares2 = DB::select('
            select j.id_jugador, concat_ws(" ", j.ap_paterno, j.ap_materno, j.nombre) as nombre, j.matricula, l.numero
            from log_partido as l, jugador as j
            where l.id_partido = '.$id_partido.'
            and l.id_equipo = '.$partido[0]->id_equipo2.'
            and l.id_jugador = j.id_jugador
            and l.titular = 1
            ');
        $suplentes2 = DB::select('
            select j.id_jugador, concat_ws(" ", j.ap_paterno, j.ap_materno, j.nombre) as nombre, j.matricula, l.numero, l.numero_s
            from log_partido as l, jugador as j
            where l.id_partido = '.$id_partido.'
            and l.id_equipo = '.$partido[0]->id_equipo2.'
            and l.id_jugador = j.id_jugador
            and l.titular = 0
            ');
        $jugado = $partido[0]->jugado;
        return view('temporada.planilla', compact(['club1', 'club2', 'id_partido', 'jugado', 'titulares1', 'titulares2', 'suplentes1', 'suplentes2', 'partido']));
    }
    public function addLogPartido(Request $request){
        $existe = DB::table('log_partido')
            ->where('id_partido', $request->id_partido)
            ->where('id_equipo', $request->id_equipo)
            ->where('id_jugador', $request->id_jugador)
            ->count();
        if ($existe == 0) {
            DB::table('log_partido')->insert([
                'id_partido' => $request->id_partido, 
                'id_equipo' => $request->id_equipo, 
                'id_jugador' => $request->id_jugador, 
                'titular' => $request->titular, 
                'numero' => NULL
            ]);
        }
        // SACAR SI ES MENOR DE EDAD
        $edad_var = DB::table('jugador')->select('fecha_nacimiento')->where('id_jugador', $request->id_jugador)->first();
        // SACAR SI ES EXTRANJERO
        $nac_var = DB::table('jugador')->select('nacionalidad')->where('id_jugador', $request->id_jugador)->first();
        return response()->json([
            'message' => 'todo salio okey',
            'nacionalidad' => $nac_var->nacionalidad,
            'edad' => $edad_var->fecha_nacimiento,
        ]);
    }
    public function deleteLogPartido(Request $request){
        $goles = Observacion::select('observacion')
            ->where('id_partido', $request->id_partido)
            ->where('id_jugador', $request->id_jugador)
            ->get();
        foreach ($goles as $key => $value) {
            if ($value != NULL) {
                Partido::where('id_partido', $request->id_partido)
                    ->where('id_equipo1', $request->id_equipo)
                    ->decrement('goles_equi1', $value->observacion);
                Partido::where('id_partido', $request->id_partido)
                    ->where('id_equipo2', $request->id_equipo)
                    ->decrement('goles_equi2', $value->observacion);
            }
        }
        DB::table('log_partido')
            ->where('id_partido', $request->id_partido)
            ->where('id_equipo', $request->id_equipo)
            ->where('id_jugador', $request->id_jugador)
            ->delete();
        // SE ELIMINA LA OBSERVACION MEDIANTE TRIGGER
        return response()->json([
            'message' => 'todo salio okey',
        ]);
    }
    public function update_num(Request $request){
        $existe = DB::table('log_partido')
            ->where('id_partido', $request->id_partido)
            ->where('id_equipo', $request->id_equipo)
            ->where('numero', $request->numero)
            ->count();
        if ($existe < 1) {
            DB::table('log_partido')
                ->where('id_partido', $request->id_partido)
                ->where('id_equipo', $request->id_equipo)
                ->where('id_jugador', $request->id_jugador)
                ->update(array('numero' => $request->numero));
            return response()->json([
                'message' => 1,
            ]);
        }
        else {
            return response()->json([
                'message' => 'Disculpe, no puede repetir numeros!',
            ]);
        }
    }
    public function update_num_s(Request $request){
        $existe = DB::table('log_partido')
            ->where('id_partido', $request->id_partido)
            ->where('id_equipo', $request->id_equipo)
            ->where('numero', $request->numero_s)
            ->count();
        if ($existe > 0) {
            DB::table('log_partido')
                ->where('id_partido', $request->id_partido)
                ->where('id_equipo', $request->id_equipo)
                ->where('id_jugador', $request->id_jugador)
                ->update(array('numero_s' => $request->numero_s));
            return response()->json([
                'message' => 1,
            ]);
        }
        else {
            return response()->json([
                'message' => 'Disculpe, el numero de jugador no existe!',
            ]);
        }
    }
    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    public function getJugadores($id_partido, $id_equipo){
        $jugadores = DB::select('
            select j.id_jugador, concat_ws(" ", j.ap_paterno, j.ap_materno, j.nombre) as nombre, j.matricula, j.fecha_nacimiento, j.nacionalidad
            from partido as p, fecha as f, grupo as g, grupo_equipo as ge, equipo_jugador as eq, jugador as j
            where p.id_partido = '.$id_partido.'
            and p.id_fecha = f.id_fecha
            and f.id_grupo = g.id_grupo
            and g.id_grupo = ge.id_grupo
            and ge.id_equipo = eq.id_equipo
            and eq.id_equipo = '.$id_equipo.'
            and eq.id_jugador = j.id_jugador
            and eq.en_espera = 0
            ');
        foreach ($jugadores as $key => $value) {
            $value->fecha_nacimiento = $this->calculaedad($value->fecha_nacimiento);
            $value->nacionalidad = $value->nacionalidad?'Nacional':'Extranjero';
        }
        $cant_titulares = DB::table('log_partido')
            ->where('id_partido', $id_partido)
            ->where('id_equipo', $id_equipo)
            ->where('titular', 1)
            ->count();
        return response()->json([
            'jugadores' => $jugadores,
            'cant_titulares' => $cant_titulares,
        ]);
    }


    public function updateJugado(Request $request){
        Partido::where('id_partido', $request->id_partido)->update(array('jugado' => 1));
        return response()->json([
            'message' => 'Partido completado!',
        ]);
    }

    public function addObservacion(Request $request){
        DB::beginTransaction();
        try {
            Observacion::create([
                'id_jugador' => $request->id_jugador,
                'id_partido' => $request->id_partido,
                'id_tipo_observacion' => $request->id_tipo_observacion,
                'observacion' => $request->observacion,
                'estado' => 1,
            ]);
            // VER SI ES TARJETA ROJA
            $partido = Partido::where('id_partido', $request->id_partido)->first();
            if ($request->id_tipo_observacion == 2) {
                $res = DB::table('equipo_jugador')
                    ->where('id_jugador', $request->id_jugador)
                    ->where('id_equipo', $partido->id_equipo1)
                    ->update(['en_espera' => 1]);
                if (!$res) {
                    DB::table('equipo_jugador')
                        ->where('id_jugador', $request->id_jugador)
                        ->where('id_equipo', $partido->id_equipo2)
                        ->update(['en_espera' => 1]);
                }
            }
            else if ($request->id_tipo_observacion == 3) {
                // DOS AMARILLAS EN EL MISMO PARTIDO
                // CINCO AMARILLAS EN EL TORNEO
                $amarilla_partido = Observacion::where('id_jugador', $request->id_jugador)
                    ->where('id_partido', $request->id_partido)
                    ->where('id_tipo_observacion', 3)
                    ->where('estado', 1)
                    ->count();
                
                $id_temporada = $partido->fecha->grupo->fase->temporada->id_temporada;
                $amarilla_torneo_ = DB::select('
                    select o.id_observacion
                    from fase as f, grupo as g, fecha as fe, partido as p, observacion as o 
                    where f.id_temporada = '.$id_temporada.'
                    and f.id_fase = g.id_fase
                    and g.id_grupo = fe.id_grupo
                    and fe.id_fecha = p.id_fecha
                    and p.id_partido = o.id_partido
                    and o.id_tipo_observacion = 2
                    and o.estado = 1
                    and o.id_jugador = '.$request->id_jugador.'
                    ');
                $amarilla_torneo = collect($amarilla_torneo_)->count();
                // return response()->json([
                //     'message' => $amarilla_torneo,
                // ]);
                if ($amarilla_partido >= 2 || $amarilla_torneo >= 5) {
                    $res = DB::table('equipo_jugador')
                        ->where('id_jugador', $request->id_jugador)
                        ->where('id_equipo', $partido->id_equipo1)
                        ->update(['en_espera' => 1]);
                    if (!$res) {
                        DB::table('equipo_jugador')
                            ->where('id_jugador', $request->id_jugador)
                            ->where('id_equipo', $partido->id_equipo2)
                            ->update(['en_espera' => 1]);
                    }
                }
            }

            if ($request->id_equipo != NULL) {
                // HACER UN UPDATE EN PARTIDO CUANDO SEA EL EQUIPO 1
                $res = Partido::where('id_partido', $request->id_partido)
                    ->where('id_equipo1', $request->id_equipo)
                    ->increment('goles_equi1', $request->observacion);
                if (!$res) {
                    // HACER UN UPDATE EN PARTIDO CUANDO SEA EL EQUIPO 2
                    Partido::where('id_partido', $request->id_partido)
                        ->where('id_equipo2', $request->id_equipo)
                        ->increment('goles_equi2', $request->observacion);
                }
            }
            DB::commit();
            return response()->json([
                'message' => 'agregado correctamente!',
            ]);
        } catch (Exception $e) {
            DB::rollback();
        }
    }

    public function add_informe_arbitro(Request $request){
        Arbitro::create([
            'id_partido' => $request->id_partido,
            'nombre' => $request->nombre, 
            'informe' => $request->informe,
        ]);
        return response()->json([
            'message' => 'Informe de Arbitro agregado correctamente!',
        ]);
    }

    public function add_informe_delegado(Request $request){
        Delegado::create([
            'id_partido' => $request->id_partido,
            'nombre' => $request->nombre, 
            'informe' => $request->informe,
        ]);
        return response()->json([
            'message' => 'Informe de Delegado agregado correctamente!',
        ]);
    }

    public function add_informe_planillero(Request $request){
        Planillero::create([
            'id_partido' => $request->id_partido,
            'nombre' => $request->nombre, 
            'informe' => $request->informe,
        ]);
        return response()->json([
            'message' => 'Informe de Planillero agregado correctamente!',
        ]);
    }

    public function add_informe_veedor(Request $request){
        Veedor::create([
            'id_partido' => $request->id_partido,
            'nombre' => $request->nombre, 
            'conducta_publico' => $request->conducta_publico,
            'conducta_jugadores' => $request->conducta_jugadores,
            'calificacion_arbitro' => $request->calificacion_arbitro,
        ]);
        return response()->json([
            'message' => 'Informe de Veedor agregado correctamente!',
        ]);
    }

    public function posiciones($id_temporada){
        $fase_p = Fase::where('id_temporada', $id_temporada)
            ->where('fase', 'Primera Fase')
            ->first();
        $grupos_p = Grupo::where('id_fase', $fase_p->id_fase)->get();
        $partidos_p = collect([]);
        foreach ($grupos_p as $g => $grupo) {
            // saca la lista de equipos que pertenecen a un grupo
            $list_equipos_p[$g] = DB::select('
                select c.nombre, e.id_equipo
                from grupo_equipo as ge, grupo as g, equipo as e, club as c
                where g.id_grupo = '.$grupo->id_grupo.'
                and g.id_grupo = ge.id_grupo
                and ge.id_equipo = e.id_equipo
                and e.id_club = c.id_club
                ');
            // lista de partidos que se an jugado de un grupo especifico
            $list_partidos = DB::select('
                select p.id_partido, p.id_equipo1, p.id_equipo2, p.ptos_equi1, p.ptos_equi2, p.goles_equi1, p.goles_equi2
                from fecha as f, partido as p
                where f.id_grupo = '.$grupo->id_grupo.'
                and f.id_fecha = p.id_fecha
                and p.jugado = 1
                ');

            foreach ($list_partidos as $p => $partido) {
                // lista de goles de un partido en especifico, de los dos equipos que jugaron
                if ($partido->ptos_equi1 == 0 && $partido->ptos_equi2 == 0) {
                    // entonces contamos los goles
                    $partidos_p->push([
                        [
                            'id_equipo' =>$partido->id_equipo1, 
                            'goles' => $partido->goles_equi1, 
                            'ptos' => 0,
                        ],
                        [
                            'id_equipo' =>$partido->id_equipo2, 
                            'goles' => $partido->goles_equi2, 
                            'ptos' => 0,
                        ],
                    ]);
                }
                else {
                    // entonces contamos los puntajes
                    $partidos_p->push([
                        [
                            'id_equipo' =>$partido->id_equipo1, 
                            'goles' => 0,
                            'ptos' => $partido->ptos_equi1,
                        ],
                        [
                            'id_equipo' =>$partido->id_equipo2, 
                            'goles' => 0,
                            'ptos' => $partido->ptos_equi2,
                        ],
                    ]);
                }
            }
        }
        foreach ($list_equipos_p as $e => $equipo) {
            foreach ($equipo as $key => $equi) {
                $pj = 0;
                $pg = 0;
                $pe = 0;
                $pp = 0;
                $gf = 0;
                $gc = 0;
                $ptos = 0;
                foreach ($partidos_p as $p => $partido) {
                    //comparando con la lista de quipos y los partidos si es el equipo 1
                    if ($equi->id_equipo == $partido[0]['id_equipo']) {
                        if ($partido[0]['ptos'] == 0 && $partido[1]['ptos'] == 0) {
                            if ($partido[0]['goles'] == $partido[1]['goles']) 
                                $pe++;//empate
                            elseif ($partido[0]['goles'] > $partido[1]['goles']) 
                                $pg++;//gano
                            elseif ($partido[0]['goles'] < $partido[1]['goles']) 
                                $pp++;//perdio
                            $gf += $partido[0]['goles'];
                            $gc += $partido[1]['goles'];
                        }
                        else
                            $ptos += $partido[0]['ptos'];
                        $pj++;
                    }
                    //comparando con la lista de quipos y los partidos si es el equipo 2
                    elseif ($equi->id_equipo == $partido[1]['id_equipo']) {
                        if ($partido[0]['ptos'] == 0 && $partido[1]['ptos'] == 0) {
                            if ($partido[1]['goles'] == $partido[0]['goles']) 
                                $pe++;//empate
                            elseif ($partido[1]['goles'] > $partido[0]['goles']) 
                                $pg++;//gano
                            elseif ($partido[1]['goles'] < $partido[0]['goles']) 
                                $pp++;//perdio
                            $gf += $partido[1]['goles'];
                            $gc += $partido[0]['goles'];
                        }
                        else
                            $ptos += $partido[1]['ptos'];
                        $pj++;
                    }
                }
                $list_equipos_p[$e][$key]->pj = $pj;// ver si funciona esta variable correctamente
                $list_equipos_p[$e][$key]->pg = $pg;
                $list_equipos_p[$e][$key]->pe = $pe;
                $list_equipos_p[$e][$key]->pp = $pp;
                $list_equipos_p[$e][$key]->gf = $gf;
                $list_equipos_p[$e][$key]->gc = $gc;
                $list_equipos_p[$e][$key]->dif = $gf - $gc;
                $list_equipos_p[$e][$key]->ptos = ($pg * 3) + ($pe * 1) + $ptos;// contar los puntajes de los walkover
            }
        }
        $partidos_p = collect([]);
        $list_equipos_p = collect($list_equipos_p);
        foreach ($list_equipos_p as $key => $value) {
            $value = collect($value);
            $sorted = $value->sortByDesc('ptos');
            $sorted = $sorted->values();
            $partidos_p->push($sorted);
        }
        // SACAMOS SI YA EXISTE SEGUNDA FASE
        $fase_s = Fase::where('id_temporada', $id_temporada)
            ->whereIn('fase', ['Segunda Fase', 'Semi Final'])
            ->first();
        if ($fase_s != NULL) {
            $grupos_s = Grupo::where('id_fase', $fase_s->id_fase)->get();
            $partidos_s = collect([]);
            foreach ($grupos_s as $g => $grupo) {
                // saca la lista de equipos que pertenecen a un grupo
                $list_equipos_s[$g] = DB::select('
                    select c.nombre, e.id_equipo
                    from grupo_equipo as ge, grupo as g, equipo as e, club as c
                    where g.id_grupo = '.$grupo->id_grupo.'
                    and g.id_grupo = ge.id_grupo
                    and ge.id_equipo = e.id_equipo
                    and e.id_club = c.id_club
                    ');
                // lista de partidos que se an jugado de un grupo especifico
                $list_partidos = DB::select('
                    select p.id_partido, p.id_equipo1, p.id_equipo2, p.ptos_equi1, p.ptos_equi2, p.goles_equi1, p.goles_equi2
                    from fecha as f, partido as p
                    where f.id_grupo = '.$grupo->id_grupo.'
                    and f.id_fecha = p.id_fecha
                    and p.jugado = 1
                    ');

                foreach ($list_partidos as $p => $partido) {
                    // lista de goles de un partido en especifico, de los dos equipos que jugaron
                    if ($partido->ptos_equi1 == 0 && $partido->ptos_equi2 == 0) {
                        // entonces contamos los goles
                        $partidos_s->push([
                            [
                                'id_equipo' =>$partido->id_equipo1, 
                                'goles' => $partido->goles_equi1, 
                                'ptos' => 0,
                            ],
                            [
                                'id_equipo' =>$partido->id_equipo2, 
                                'goles' => $partido->goles_equi2, 
                                'ptos' => 0,
                            ],
                        ]);
                    }
                    else {
                        // entonces contamos los puntajes
                        $partidos_s->push([
                            [
                                'id_equipo' =>$partido->id_equipo1, 
                                'goles' => 0,
                                'ptos' => $partido->ptos_equi1,
                            ],
                            [
                                'id_equipo' =>$partido->id_equipo2, 
                                'goles' => 0,
                                'ptos' => $partido->ptos_equi2,
                            ],
                        ]);
                    }
                }
            }
            foreach ($list_equipos_s as $e => $equipo) {
                foreach ($equipo as $key => $equi) {
                    $pj = 0;
                    $pg = 0;
                    $pe = 0;
                    $pp = 0;
                    $gf = 0;
                    $gc = 0;
                    $ptos = 0;
                    foreach ($partidos_s as $p => $partido) {
                        //comparando con la lista de quipos y los partidos si es el equipo 1
                        if ($equi->id_equipo == $partido[0]['id_equipo']) {
                            if ($partido[0]['ptos'] == 0 && $partido[1]['ptos'] == 0) {
                                if ($partido[0]['goles'] == $partido[1]['goles']) 
                                    $pe++;//empate
                                elseif ($partido[0]['goles'] > $partido[1]['goles']) 
                                    $pg++;//gano
                                elseif ($partido[0]['goles'] < $partido[1]['goles']) 
                                    $pp++;//perdio
                                $gf += $partido[0]['goles'];
                                $gc += $partido[1]['goles'];
                            }
                            else
                                $ptos += $partido[0]['ptos'];
                            $pj++;
                        }
                        //comparando con la lista de quipos y los partidos si es el equipo 2
                        elseif ($equi->id_equipo == $partido[1]['id_equipo']) {
                            if ($partido[0]['ptos'] == 0 && $partido[1]['ptos'] == 0) {
                                if ($partido[1]['goles'] == $partido[0]['goles']) 
                                    $pe++;//empate
                                elseif ($partido[1]['goles'] > $partido[0]['goles']) 
                                    $pg++;//gano
                                elseif ($partido[1]['goles'] < $partido[0]['goles']) 
                                    $pp++;//perdio
                                $gf += $partido[1]['goles'];
                                $gc += $partido[0]['goles'];
                            }
                            else
                                $ptos += $partido[1]['ptos'];
                            $pj++;
                        }
                    }
                    $list_equipos_s[$e][$key]->pj = $pj;// ver si funciona esta variable correctamente
                    $list_equipos_s[$e][$key]->pg = $pg;
                    $list_equipos_s[$e][$key]->pe = $pe;
                    $list_equipos_s[$e][$key]->pp = $pp;
                    $list_equipos_s[$e][$key]->gf = $gf;
                    $list_equipos_s[$e][$key]->gc = $gc;
                    $list_equipos_s[$e][$key]->dif = $gf - $gc;
                    $list_equipos_s[$e][$key]->ptos = ($pg * 3) + ($pe * 1) + $ptos;// contar los puntajes de los walkover
                }
            }
            $partidos_s = collect([]);
            $list_equipos_s = collect($list_equipos_s);
            foreach ($list_equipos_s as $key => $value) {
                $value = collect($value);
                $sorted = $value->sortByDesc('ptos');
                $sorted = $sorted->values();
                $partidos_s->push($sorted);
            }
            // AQUI VEMOS SI YA SE GENERO LA FASE FINAL
            $fase_f = Fase::where('id_temporada', $id_temporada)
                ->where('fase', 'Final')
                ->first();
            if ($fase_f != NULL) {
                $grupos_f = Grupo::where('id_fase', $fase_f->id_fase)->first();
                $list_equipos_f = DB::select('
                    select c.nombre, e.id_equipo
                    from grupo_equipo as ge, grupo as g, equipo as e, club as c
                    where g.id_grupo = '.$grupos_f->id_grupo.'
                    and g.id_grupo = ge.id_grupo
                    and ge.id_equipo = e.id_equipo
                    and e.id_club = c.id_club
                    ');
                // POSIBLEMENTE DE ERRORES POR CULTA DE NO TENER ID LA TABLA GRUPO_EQUIPO
                // LOS 2 PRIMEROS EQUIPOS SON PARA PRIMER Y SEGUNDO LUGAR
                $list_fechas = Fecha::where('id_grupo', $grupos_f->id_grupo)->get();
                // PARTIDO PARA 3 Y 4 LUGAR
                $partido_1 = Partido::where('id_fecha', $list_fechas[0]->id_fecha)->first();
                // PARTIDO PARA 1 Y 2 LUGAR
                $partido_2 = Partido::where('id_fecha', $list_fechas[1]->id_fecha)->first();
                $posiciones_f = collect([]);
                if ($partido_1->ptos_equi1 == 0 && $partido_1->ptos_equi2 == 0) {
                    if ($partido_1->goles_equi1 > $partido_1->goles_equi2) {
                        $posiciones_f->push([
                            ['id_equipo' =>$partido_1->id_equipo1,'pos' =>1,],
                            ['id_equipo' =>$partido_1->id_equipo2,'pos' =>2,],
                        ]);
                    }
                    else{
                        $posiciones_f->push([
                            ['id_equipo' =>$partido_1->id_equipo2,'pos' =>1,],
                            ['id_equipo' =>$partido_1->id_equipo1,'pos' =>2,],
                        ]);
                    }
                }
                if ($partido_2->ptos_equi1 == 0 && $partido_2->ptos_equi2 == 0) {
                    if ($partido_2->goles_equi1 > $partido_2->goles_equi2) {
                        $posiciones_f->push([
                            ['id_equipo' =>$partido_2->id_equipo1,'pos' =>3,],
                            ['id_equipo' =>$partido_2->id_equipo2,'pos' =>4,],
                        ]);
                    }
                    else{
                        $posiciones_f->push([
                            ['id_equipo' =>$partido_2->id_equipo2,'pos' =>3,],
                            ['id_equipo' =>$partido_2->id_equipo1,'pos' =>4,],
                        ]);
                    }
                }
                $partidos_f = collect([]);
                $list_equipos_f = collect($list_equipos_f);
                foreach ($list_equipos_f as $key => $value) {
                    $value = collect($value);
                    foreach ($posiciones_f as $p => $pos) {
                        if ($pos[0]['id_equipo'] == $value['id_equipo']) {
                            $list_equipos_f[$key]->pos = $pos[0]['pos'];
                        }
                        elseif ($pos[1]['id_equipo'] == $value['id_equipo']) {
                            $list_equipos_f[$key]->pos = $pos[1]['pos'];
                        }
                    }
                }
                $partidos_f = ($list_equipos_f->sortBy('pos'))->values();
            }
            else{$grupos_f = NULL; $partidos_f = NULL;}
        }
        else { $grupos_s = NULL; $partidos_s = NULL; $fase_f = NULL; $grupos_f = NULL; $partidos_f = NULL;}
        return view('temporada.posiciones')->with([
            'fase_p' => $fase_p,
            'grupos_p' => $grupos_p,
            'partidos_p' => $partidos_p,
            'fase_s' => $fase_s,
            'grupos_s' => $grupos_s,
            'partidos_s' => $partidos_s,
            'fase_f' => $fase_f,
            'grupos_f' => $grupos_f,
            'partidos_f' => $partidos_f,
        ]);
    }
    public function update_partido_ptos(Request $request){
        // HACER UN UPDATE EN PARTIDO CUANDO SEA EL EQUIPO 1
        Partido::where('id_partido', $request->id_partido)
            ->where('id_equipo1', $request->id_equipo)
            ->update(['ptos_equi1' => $request->ptos]);
        // HACER UN UPDATE EN PARTIDO CUANDO SEA EL EQUIPO 2
        Partido::where('id_partido', $request->id_partido)
            ->where('id_equipo2', $request->id_equipo)
            ->update(['ptos_equi2' => $request->ptos]);
        return response()->json([
            'message' => 'Todo salio okey!',
        ]);
    }
    public function createSegundaFase(Request $request){
        DB::beginTransaction();
        try {
            // insertar la Primera Fase
            $id_fase = DB::table('fase')->insertGetId([
                'id_temporada' => $request->id_temporada,
                'fase' => $request->fase,
            ]);
            $id_equipos = collect([
                $request->equi1,
                $request->equi2,
                $request->equi3,
                $request->equi4,
            ]);
            //inserta un grupo y saca el id del ultimo insertado
            $id_grupo = DB::table('grupo')->insertGetId([
                'id_fase' => $id_fase,
                'grupo' => 'Grupo 1',
            ]);
            for ($i=0; $i < $id_equipos->count(); $i++) {
                // PONER PUNTOS EXTRA A UN EQUIPO (solucion con solo poner en la tabla grupo_equipoen orden)
                DB::table('grupo_equipo')->insert([
                    'id_grupo' => $id_grupo, 
                    'id_equipo' => $id_equipos[$i],
                ]);
            }
            if ($request->fase == 'Segunda Fase') {
                //la colleccion es aleatorio con shuffle
                $id_equipos = $id_equipos->shuffle();
                if ($request->checked == 'true') {
                    $this->equiposPar_SF($id_grupo, $id_equipos->values());
                    $this->equiposPar_SF($id_grupo, $id_equipos->values());
                }
                else{
                    $this->equiposPar_SF($id_grupo, $id_equipos->values());
                }
            }
            else if ($request->fase == 'Semi Final') {
                $id_fecha = DB::table('fecha')->insertGetId(['id_grupo' => $id_grupo, 'fecha' => today()]);
                for ($i=0; $i < ($id_equipos->count())/2; $i++) { 
                    Partido::create([
                        'id_equipo1' => $id_equipos[$i], 
                        'id_equipo2' => $id_equipos[$id_equipos->count()-$i-1], 
                        'id_cancha' => NULL, 
                        'id_fecha' => $id_fecha, 
                        'hora' => NULL,
                        'ptos_equi1' => 0,
                        'ptos_equi2' => 0,
                        'goles_equi1' => 0,
                        'goles_equi2' => 0,
                        'jugado' => 0,
                    ]);
                }
            }

            DB::commit();
            return response()->json([
                'message' =>$request->fase.' generada correctamente!',
            ]);
        } catch (Exception $e) {
            DB::rollback();
        }
    }
    public function createFinalFase(Request $request){
        DB::beginTransaction();
        try {
            $id_fase = DB::table('fase')->insertGetId([
                'id_temporada' => $request->id_temporada,
                'fase' => 'Final',
            ]);
            $id_equipos = collect([
                $request->equi1,
                $request->equi2,
                $request->equi3,
                $request->equi4,
            ]);
            //inserta un grupo y saca el id del ultimo insertado
            $id_grupo = DB::table('grupo')->insertGetId([
                'id_fase' => $id_fase,
                'grupo' => 'Grupo 1',
            ]);
            // VER LUEGO SI ES NECESARIO PONER OTRA VEZ EN LA TABLA GRUPO_EQUIPO
            for ($i=0; $i < $id_equipos->count(); $i++) {
                DB::table('grupo_equipo')->insert([
                    'id_grupo' => $id_grupo, 
                    'id_equipo' => $id_equipos[$i],
                ]);
            }
            // VOLCAMOS LA LISTA PORQUE LOS ULTIMOS SON PARA TERCEROS Y CUARTO LUGAR Y LOS PRIMEROS SON PARA PRIMER Y SEGUNDO LUGAR
            $id_equipos = ($id_equipos->reverse())->values();
            for ($i=0; $i < $id_equipos->count(); $i+=2) { 
                $id_fecha = DB::table('fecha')->insertGetId(['id_grupo' => $id_grupo, 'fecha' => today()]);
                Partido::create([
                    'id_equipo1' => $id_equipos[$i], 
                    'id_equipo2' => $id_equipos[$i+1], 
                    'id_cancha' => NULL, 
                    'id_fecha' => $id_fecha, 
                    'hora' => NULL,
                    'ptos_equi1' => 0,
                    'ptos_equi2' => 0,
                    'goles_equi1' => 0,
                    'goles_equi2' => 0,
                    'jugado' => 0,
                ]);
            }
            DB::commit();
            return response()->json([
                'message' =>'Fase Final generada correctamente!',
            ]);
        } catch (Exception $e) {
            DB::rollback();
        }
    }
    private function equiposPar_SF($id_grupo, $id_equipos){
        // ciclo para agregar fechas de ida
        for ($i=0; $i < ($id_equipos->count())-1; $i++) { 
            $id_fecha = DB::table('fecha')->insertGetId(['id_grupo' => $id_grupo, 'fecha' => today()]);
            // inserta el total de partidos por fecha
            $id_equipo1 = $id_equipos[0];
            $id_equipo2 = $id_equipos[1];
            $e1 = 2;
            $e2 = $id_equipos->count()-1;
            for ($j=0; $j < ($id_equipos->count())/2; $j++) { 
                Partido::create([
                    'id_equipo1' => $id_equipo1, 
                    'id_equipo2' => $id_equipo2, 
                    'id_cancha' => NULL, 
                    'id_fecha' => $id_fecha, 
                    'hora' => NULL,
                    'ptos_equi1' => 0,
                    'ptos_equi2' => 0,
                    'goles_equi1' => 0,
                    'goles_equi2' => 0,
                    'jugado' => 0,
                ]);
                $id_equipo1 = $id_equipos[$e1];
                $id_equipo2 = $id_equipos[$e2];
                $e1 += 1;
                $e2 -= 1;
            }
            //organizar el orden de los equipos
            $aux = $id_equipos[1];
            for ($k=1; $k < $id_equipos->count()-1; $k++) { 
                $id_equipos[$k] = $id_equipos[$k+1];
            }
            $id_equipos[$id_equipos->count()-1] = $aux;
        }
    }
    public function clubestemporada($id_temporada){
        $clubes = DB::select('SELECT club.*, equipo.id_equipo 
        FROM grupo_equipo, equipo, categoria, temporada, fase, grupo, club
        WHERE temporada.id_categoria= categoria.id_categoria 
        and fase.id_temporada=temporada.id_temporada
        and fase.id_fase=grupo.id_fase
        and grupo.id_grupo=grupo_equipo.id_grupo
        and grupo_equipo.id_equipo=equipo.id_equipo
        and equipo.id_club=club.id_club
        and temporada.id_temporada='.$id_temporada
        );

        $id_temp=$id_temporada;
        return view('temporada.clubstemporada', compact('clubes', 'id_temp'));
    }
    public function equipo_jugador($id_club, Request $request )
    { 
        $idEquipo = 0;
        $equipos = DB::select('SELECT categoria.*, club.id_club, equipo.id_equipo     
            FROM equipo,club, categoria  
            WHERE equipo.id_club=club.id_club 
            and equipo.id_categoria= categoria.id_categoria 
            and club.id_club='.$id_club
        );

        if ($request->id_equipo){
            $idEquipo = $request->id_equipo;
        }else{
            $idEquipo = $equipos[0]->id_equipo;//controlar si en el resultado anterior no existe elementos
        }
        
        $jugadores = DB::select('SELECT jugador.*, equipo_jugador.id_equipo 
            from jugador 
            INNER JOIN equipo_jugador on jugador.id_jugador = equipo_jugador.id_jugador 
            WHERE equipo_jugador.id_equipo='.$idEquipo
        );
        
        return view('equipo_jugador.index', compact('equipo_jugadores','equipos','jugadores', 'id_club'));
    }
     public function jugadoreswithoutequipo($id_club){
        $jugadores = DB::select('SELECT jugador.*, equipo_jugador.id_jugador as equipo_jugador_id_jugador
            from jugador
            left OUTER join equipo_jugador on jugador.id_jugador = equipo_jugador.id_jugador 
            inner JOIN club on jugador.id_club = club.id_club 
            where equipo_jugador.id_jugador is null
            and club.id_club='.$id_club
        );

        foreach ($jugadores as $key => $value) {
            $value->fecha_nacimiento = $this->calculaedad($value->fecha_nacimiento);
            $value->tipo_jugador = $value->tipo_jugador?'Aficionado':'No aficionado';
            $value->nacionalidad = $value->nacionalidad?'Nacional':'Extranjero';
        }
        return $jugadores;
    }
    public function guardarequipojugador(Request $request){
        $contRes = 0;
        foreach ($request->jugadores as $key => $jugador) {
             $res = Equipo_jugador::create([
                'id_equipo' => intval($request->id_equipo),
                'id_jugador' => intval($jugador['id_jugador']),
                ]);
            if ($res) {
                $contRes++;
            }
        }
        if ($contRes == count($request->jugadores) ) {
            return response()->json([
                'message'=>'Jugadores agregados correctamente',
            ], 200);
        }else{
            return response()->json([
                'message'=>'No agregados correctamente',
            ], 500);
        }

    }
    public function mostrarjugadores($id_equipo){
        $jugadores= DB::select('SELECT jugador.* 
        FROM equipo_jugador, jugador 
        WHERE equipo_jugador.id_jugador=jugador.id_jugador 
        and equipo_jugador.id_equipo='.$id_equipo);
        foreach ($jugadores as $key => $value) {
            $value->fecha_nacimiento = $this->calculaedad($value->fecha_nacimiento);
            $value->tipo_jugador = $value->tipo_jugador?'Aficionado':'No aficionado';
            $value->nacionalidad = $value->nacionalidad?'Nacional':'Extranjero';
        }
        return $jugadores;
    }

    public function borrarequipojugador(Request $request){
        $contRes = 0;
        foreach ($request->jugadores as $key => $jugador) {
            $model = equipo_jugador::where([
                'id_equipo'=>intval($request->id_equipo),
                'id_jugador'=>intval($jugador['id_jugador'])
            ]);
            $res = $model->delete();
            if ($res) {
                $contRes++;
            }
        }
        if ($contRes == count($request->jugadores) ) {
            return response()->json([
                'message'=>'Jugadores eliminaron correctamente',
            ], 200);
        }else{
            return response()->json([
                'message'=>'No eliminaron correctamente',
            ], 500);
        }

    }

    private function calculaedad($fechanacimiento){
        list($ano,$mes,$dia) = explode("-",$fechanacimiento);
        $ano_diferencia  = date("Y") - $ano;
        $mes_diferencia = date("m") - $mes;
        $dia_diferencia   = date("d") - $dia;
        if ($dia_diferencia < 0 || $mes_diferencia < 0){
            $ano_diferencia--;
        }
        return $ano_diferencia;
    }
}