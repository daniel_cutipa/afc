<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $deudas = DB::select('
                select de.*, ca.categoria, cl.nombre as club
                from deuda_equipo as de, equipo as e, categoria as ca, club as cl
                where de.estado = 1
                and de.id_equipo = e.id_equipo
                and e.id_club = cl.id_club
                and e.id_categoria = ca.id_categoria
                order by de.id_deuda_equipo desc
                ');
        $sanciones = \App\Sancion::where('estado', 1)->orderBy('id_sancion', 'desc')->get();
        $goleadores = DB::select('
            select j.matricula, concat_ws(" ", j.ap_paterno, j.ap_materno, j.nombre) as nombre, sum(o.observacion) as goles, c.nombre as club
            from observacion as o, jugador as j, club as c
            where o.id_tipo_observacion = 1
            and o.id_jugador = j.id_jugador
            and j.id_club = c.id_club
            group by j.id_jugador
            order by goles desc
            ');
        $rojas = DB::select('
            select j.matricula, concat_ws(" ", j.ap_paterno, j.ap_materno, j.nombre) as nombre, count(o.id_observacion) as roja, c.nombre as club
            from observacion as o, jugador as j, club as c
            where o.id_tipo_observacion = 2
            and o.id_jugador = j.id_jugador
            and j.id_club = c.id_club
            group by j.id_jugador
            order by roja desc
            ');
        $amarillas = DB::select('
            select j.matricula, concat_ws(" ", j.ap_paterno, j.ap_materno, j.nombre) as nombre, count(o.id_observacion) as amarilla, c.nombre as club
            from observacion as o, jugador as j, club as c
            where o.id_tipo_observacion = 3
            and o.id_jugador = j.id_jugador
            and j.id_club = c.id_club
            group by j.id_jugador
            ');
        return view('home', compact('deudas', 'sanciones', 'goleadores', 'rojas', 'amarillas'));
    }
}
