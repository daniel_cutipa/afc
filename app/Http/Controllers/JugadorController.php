<?php

namespace App\Http\Controllers;

use App\Jugador;
use App\Club;
use DB;
use Illuminate\Http\Request;
use Redirect;

class JugadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jugadores=Jugador::all();
        return view('jugador.index', compact ('jugadores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_club)
    {
        $clubs = DB::select('select c.*, c.nombre as club, cat.categoria as categoria
           from club as c, equipo as e, categoria as cat
           where e.id_club = c.id_club
           and e.id_categoria = cat.id_categoria'
        );
        $jugadores=Jugador::all();
        return view('jugador.create', compact ('jugadores','clubs', 'id_club'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'nombre' => 'required|string|max:20',
            'ap_paterno' => 'required|string|max:20',
            'ap_materno' => 'required|string|max:20',
            'matricula' => 'required|int|',
            'fecha_nacimiento' => 'required|date',
            'direccion' => 'required|string|max:30',
            'telefono' => 'required|string|max:30',
            'celular' => 'required|string|max:30',
            'tipo_jugador' => 'required|string|max:30'


        ]);
        Jugador::create([
            'id_club'=> $request['id_club'],
             'nombre' => $request['nombre'],
             'ap_paterno' => $request['ap_paterno'],
             'ap_materno' => $request['ap_materno'],
             'matricula' => $request['matricula'],
             'fecha_nacimiento' => $request['fecha_nacimiento'],
             'sexo' => $request['sexo'],
             'nacionalidad' => $request['nacionalidad'],
             'direccion' => $request['direccion'],
             'telefono' => $request['telefono'],
             'celular' => $request['celular'],
             'tipo_jugador' => $request['tipo_jugador'],
             'estado' => 1,
        ]);
        return redirect('/club/jugador/'.$request['id_club'])->with('message','Jugador registrado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Jugador  $jugador
     * @return \Illuminate\Http\Response
     */
    public function show(Jugador $jugador)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Jugador  $jugador
     * @return \Illuminate\Http\Response
     */
    public function edit(Jugador $jugador )
    {
       $clubs = DB::select('
           select jug.*, c.*, c.nombre as club, cat.categoria as categoria
           from jugador as jug, club as c, equipo as e, categoria as cat
           where e.id_club = c.id_club
           and e.id_categoria = cat.id_categoria'
       );
        return view('jugador.edit', compact('jugador', 'clubs' ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Jugador  $jugador
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Jugador $jugador)
    {
        $this->validate($request, [
            'nombre' => 'required|string|max:30',
            'ap_paterno' => 'required|string|max:30',
            'ap_materno' => 'required|string|max:30',
            // 'matricula' => 'required|int|',
            // 'fecha_nacimiento' => 'required|date',
            'direccion' => 'required|string|max:30',
            'telefono' => 'required|string|max:30',
            'celular' => 'required|string|max:30',
            'tipo_jugador' => 'required|string|max:30',
        ]);
        $jugador->nombre = $request->nombre;
        $jugador->ap_paterno = $request->ap_paterno;
        $jugador->ap_materno = $request->ap_materno;
        $jugador->nacionalidad = $request->nacionalidad;
        // $jugador->matricula = $request->matricula;
        // $jugador->fecha_nacimiento = $request->fecha_nacimiento;
        $jugador->direccion = $request->direccion;
        $jugador->telefono = $request->telefono;
        $jugador->celular = $request->celular;
        $jugador->tipo_jugador = $request->tipo_jugador;
        $jugador->save();

        return redirect('/club/jugador/'.$jugador->id_club)-> with('message','Datos guardados');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Jugador  $jugador
     * @return \Illuminate\Http\Response
     */
    public function destroy($jugador)
    {
         //$jugador->estado = 0;
         Jugador::where('id_jugador', $jugador)->update(
            array(
                'estado'=>0,
            )
        );
         return redirect('/club');
    }




}
