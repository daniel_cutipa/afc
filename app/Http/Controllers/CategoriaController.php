<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Division;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias = Categoria::where('estado', 1)->orderBy('id_categoria', 'desc')->get();
        return view('categoria.index', compact('categorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $divisiones =Division::where('estado', 1)->get();        
        return view('categoria.create', compact('divisiones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'id_division' => 'required|integer',
            'categoria' => 'required|string|max:20',
        ]);
        Categoria::create([
            'id_division' => $request['id_division'],
            'categoria' => $request['categoria'],
            'edad_min' => $request['edad_min'],
            'edad_max' => $request['edad_max'],
            'estado' => 1,
        ]);
        return redirect('/categoria');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function show(Categoria $categoria)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function edit($id)//Categoria $categoria)
    {
        //<------------
        $categoria = Categoria::where('id_categoria', $id)->firstOrFail();
        //------------>
        $categoria->divisiones = Division::where('estado', 1)->get();
        return view('categoria.edit', compact('categoria'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)//Categoria $categoria)
    {
        $this->validate($request, [
            'id_division' => 'required|integer',
            'categoria' => 'required|string|max:20',
            'edad_min' => 'required|integer|min:5|max:100',
            'edad_max' => 'required|integer|min:5|max:100',
        ]);
        //<----------se utiliza el $id porque no funciona de otra forma Categoria $categoria
        Categoria::where('id_categoria', $id)->update(
            array(
                'id_division'=>$request->id_division,
                'categoria'=>$request->categoria,
                'edad_min'=>$request->edad_min,
                'edad_max'=>$request->edad_max,
            )
        );
        //---------->esta parte esta comentada porque existe lo de arriba 
        // $categoria->id_division = $request->id_division;
        // $categoria->categoria = $request->categoria;
        // $categoria->edad_min = $request->edad_min;
        // $categoria->edad_max = $request->edad_max;
        // $categoria->save();
        return redirect('/categoria');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)//Categoria $categoria)
    {
        //<---------
        Categoria::where('id_categoria', $id)->update(
            array(
                'estado'=>0,
            )
        );
        //---------->
        // $categoria->estado = 0;
        // $categoria->save();
        return redirect('/categoria');
    }

    
}
