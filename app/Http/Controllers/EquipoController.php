<?php

namespace App\Http\Controllers;

use App\Equipo;
use App\Club;
use App\Categoria;
use DB;
use Illuminate\Http\Request;

class EquipoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $equipos = Equipo::where('estado', 1)->orderBy('id_equipo', 'desc')->get();
        return view('equipo.index', compact('equipos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clubes = Club::where('estado', 1)->get();
        $categorias = Categoria::where('estado', 1)->get();
        return view('equipo.create', compact(['clubes', 'categorias']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'id_categoria' => 'required|integer',
            'id_club' => 'required|integer',
            'director_tecnico' => 'required|string|max:30',
        ]);
        Equipo::create([
            'id_categoria' => $request['id_categoria'],
            'id_club' => $request['id_club'],
            'licencia' => 0,
            'director_tecnico' => $request['director_tecnico'],
            'estado' => 1,
        ]);
        return redirect('/equipo');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Equipo  $equipo
     * @return \Illuminate\Http\Response
     */
    public function show(Equipo $equipo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Equipo  $equipo
     * @return \Illuminate\Http\Response
     */
    public function edit(Equipo $equipo)
    {
        $equipo->clubes = Club::where('estado', 1)->get();
        $equipo->categorias = Categoria::where('estado', 1)->get();
        return view('equipo.edit', compact('equipo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Equipo  $equipo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Equipo $equipo)
    {
        $this->validate($request, [
            'id_categoria' => 'required|integer',
            'id_club' => 'required|integer',
            'director_tecnico' => 'required|string|max:45',
        ]);
        $equipo->id_categoria = $request->id_categoria;
        $equipo->id_club = $request->id_club;
        $equipo->director_tecnico = $request->director_tecnico;
        $equipo->save();
        return redirect('/equipo');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Equipo  $equipo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Equipo $equipo)
    {
        $equipo->estado = 0;
        $equipo->save();
        return redirect('/equipo');
    }

    /**EN ESTA PARTE VAMOS HACER EL CRUD DE LA RELACION ENTRE CATEGORIA Y EQUIPO**/


    // EN ESTA PARTE VAMOS A GESTIONAR LAS DEUDAS
    public function deuda(){
        $deudas = DB::select('
                select de.*, ca.categoria, cl.nombre as club
                from deuda_equipo as de, equipo as e, categoria as ca, club as cl
                where de.estado = 1
                and de.id_equipo = e.id_equipo
                and e.id_club = cl.id_club
                and e.id_categoria = ca.id_categoria
                order by de.id_deuda_equipo desc
                ');
        return view('equipo.deuda_index', compact('deudas'));
    }
    public function deudaCreate(){
        $temporadas = \App\Temporada::orderBy('id_temporada', 'desc')->where('estado', 1)->get();
        return view('equipo.deuda', compact('temporadas'));
    }
    public function getEquipos($id_temporada){
        $equipos = DB::select('
            select distinct eq.id_equipo, c.nombre as club, eq.director_tecnico as dt
            from fase as f, grupo as g, grupo_equipo as ge, equipo as eq, club as c
            where f.id_temporada = '.$id_temporada.'
            and f.id_fase = g.id_fase
            and g.id_grupo = ge.id_grupo
            and ge.id_equipo = eq.id_equipo
            and eq.id_club = c.id_club
            order by c.nombre
            ');
        return response()->json([
            'equipos' => $equipos,
        ]);
    }

    public function deudaStore(Request $request)
    {
        $this->validate($request, [
            'descripcion' => 'required|string',
            'fecha_amonestacion' => 'required|date',
        ]);
        DB::table('deuda_equipo')->insert([
            'id_equipo' => $request->id_equipo,
            'fecha_amonestacion' => $request->fecha_amonestacion,
            'descripcion' => $request->descripcion,
            'monto' => $request->monto,
            'estado' => 1,
        ]);
        return response()->json([
            'message' => 'Se guardo correactamente!!!',
        ]);
    }
    public function deudaDelete($id_deuda)
    {
        DB::table('deuda_equipo')->where('id_deuda_equipo', $id_deuda)->update(['estado' => 0]);
        // return redirect('/deuda');
        return redirect()->back();
    }
}
