<?php

namespace App\Http\Controllers;

use App\Cancha;
use Illuminate\Http\Request;

class CanchaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $canchas = Cancha::where('estado', 1)->get();
        return view('cancha.index', compact('canchas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cancha.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'nombre' => 'required|string|max:45',
            // 'ubicacion' => 'required|string|max:45',

    ]);
        Cancha::create([
           'nombre' => $request['nombre'],
            'ubicacion' => $request['ubicacion'],
            'tipo_cancha' => $request['tipo_cancha'],
            'estado' => 1,
        ]);
        return redirect('/cancha');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Division  $division
     * @return \Illuminate\Http\Response
     */
    public function show(Cancha $cancha)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Division  $division
     * @return \Illuminate\Http\Response
     */
    public function edit(Cancha $cancha)
    {
        return view('cancha.edit', compact('cancha'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Division  $division
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cancha $cancha)
    {
        $this->validate($request, [
            'nombre' => 'required|string|max:45',
            // 'ubicacion' => 'required|string|max:45',

        ]); 
        $cancha->nombre = $request->nombre;
        $cancha->ubicacion = $request->ubicacion;
        $cancha->tipo_cancha = $request->tipo_cancha;

        $cancha->save();
        return redirect('/cancha');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Division  $division
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cancha $cancha)
    {
        $cancha->estado = 0;
        $cancha->save();
        return redirect('/cancha');
    }
}