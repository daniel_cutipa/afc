<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fase extends Model
{
    //le damos el nombre a la tabla
    protected $table = 'fase';
    protected $primaryKey = 'id_fase';
    protected $fillable = ['id_temporada', 'fase', 'estado'];
    //ya no guarda el created_at, updated_at 
    public $timestamps = false;

    public function temporada(){
    	return $this->hasOne(Temporada::class, 'id_temporada', 'id_temporada');
    }

    // estado [
    // 	0 => 'fase completada',
    // 	1 => 'fase no completada',
    // ]
}
