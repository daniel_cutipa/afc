<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipo_jugador extends Model
{
    protected $table = 'equipo_jugador';
    protected $fillable = ['id_equipo', 'id_jugador', 'en_espera'];
    //ya no guarda el created_at, updated_at 
    public $timestamps = false;
}
