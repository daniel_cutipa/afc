<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cancha extends Model
{

	//le damos el nombre a la tabla
    protected $table = 'cancha';
    protected $primaryKey = 'id_cancha';
    protected $fillable = ['nombre','ubicacion','tipo_cancha','estado'];
    //ya no guarda el created_at, updated_at 
    public $timestamps = false;
}
