<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partido extends Model
{
    //le damos el nombre a la tabla
    protected $table = 'partido';
    protected $primaryKey = 'id_partido';
    protected $fillable = ['id_equipo1', 'id_equipo2', 'id_cancha', 'id_fecha', 'hora', 'jugado' ];
    //ya no guarda el created_at, updated_at 
    public $timestamps = false;

    public function cancha(){
    	return $this->hasOne(Cancha::class, 'id_cancha', 'id_cancha');
    }
    public function fecha(){
    	return $this->hasOne(Fecha::class, 'id_fecha', 'id_fecha');
    }
    public function equipo(){
    	return $this->hasOne(Equipo::class, 'id_equipo', 'id_equipo');
    }
}
