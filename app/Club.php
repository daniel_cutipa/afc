<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    //le damos el nombre a la tabla
    protected $table = 'club';
    protected $primaryKey = 'id_club';
    protected $fillable = ['nombre', 'fecha_fundacion', 'logo', 'presidente', 'tipo_club', 'estado'];
    //ya no guarda el created_at, updated_at 
    public $timestamps = false;
}

// tipo_club = [
// 	0 => no aficionado
// 	1 => aficionado
// ]


