<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Planillero extends Model
{
  //le damos el nombre a la tabla
  protected $table = 'planillero';
  protected $primaryKey = 'id_planillero';
  protected $fillable = ['id_partido','nombre', 'informe'];
  //ya no guarda el created_at, updated_at
  public $timestamps = false;
}
