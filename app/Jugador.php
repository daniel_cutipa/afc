<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jugador extends Model
{
    //le damos el nombre a la tabla
    protected $table = 'jugador';
    protected $primaryKey = 'id_jugador';
    protected $fillable = ['id_club', 'nombre','ap_paterno', 'ap_materno', 'matricula',  'fecha_nacimiento', 'sexo', 'direccion', 'telefono', 'celular', 'tipo_jugador', 'nacionalidad', 'estado'];
    //ya no guarda el created_at, updated_at 
    public $timestamps = false;

    

    // tipo_jugador
    // 0 => no aficionado
    // 1 => aficionado

    // nacionalidad
    // 0 => extranjero
    // 1 => boliviano
}