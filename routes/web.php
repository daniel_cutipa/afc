<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::group(['middleware' => 'auth'], function(){
	Route::get('/', 'HomeController@index')->name('home');
	
	Route::group(['middleware' => 'admin'], function(){
		
		Route::resource('division', 'DivisionController');

		Route::resource('categoria', 'CategoriaController');
		Route::resource('equipo', 'EquipoController');

		Route::resource('cancha', 'CanchaController');

		Route::resource('jugador', 'JugadorController');
		/**
		 * for create jugador with id club 
		 */
		Route::get('jugador/create/{id_club}', 'JugadorController@create');

		Route::resource('sancion','SancionController');
		Route::get('sancion/getJugadores/{id}', 'SancionController@getJugadores');
		Route::get('sancion/searchJugadores/{id}/{txt}', 'SancionController@searchJugadores');
		Route::post('sancion/addSancion', 'SancionController@addSancion');

		Route::resource('club', 'ClubController');

		Route::get('club/jugador/{id}','ClubController@jugador');

		Route::resource('reporte','ReporteController');

		Route::resource('user','UserController');

		// Route::get('equipo_jugador/libres/{id_club}', 'ClubController@jugadoreswithoutequipo');

		Route::get('equipo_jugador/{id}', 'ClubController@equipo_jugador');

		Route::get('equipo_jugador/{id}/{id_equipo}', 'ClubController@equipo_jugador');

		Route::resource('equipo_jugador', 'ClubController');

		Route::post('equipo_jugador/agregarequipojugador/', 'ClubController@guardarequipojugador');

		Route::get('temporada/clubes/libres/{id_club}', 'TemporadaController@jugadoreswithoutequipo');
		Route::get('equipo_jugador/{id}/{id_equipo}', 'TemporadaController@equipo_jugador');
		Route::get('temporada/clubes/{id_club}','TemporadaController@clubestemporada');
		Route::post('temporada/clubes/agregarequipojugador/', 'TemporadaController@guardarequipojugador');
		Route::get('temporada/clubes/equipo/{id_equipo}', 'TemporadaController@mostrarjugadores');
		Route::post('temporada/clubes/borrarequipojugador/', 'TemporadaController@borrarequipojugador');
		
		Route::get('temporada/countEquipos/{id}', 'TemporadaController@countEquipos');
		Route::get('temporada/{id}/posiciones', 'TemporadaController@posiciones');
		Route::post('temporada/getFases', 'TemporadaController@getFases');
		Route::post('temporada/createSegundaFase', 'TemporadaController@createSegundaFase');
		Route::post('temporada/createFinalFase', 'TemporadaController@createFinalFase');

		Route::post('temporada/updateGrupo', 'TemporadaController@updateGrupo');
		Route::post('temporada/updateFecha', 'TemporadaController@updateFecha');
		Route::post('temporada/updateHora', 'TemporadaController@updateHora');
		Route::post('temporada/updateCancha', 'TemporadaController@updateCancha');

	});

	Route::get('deuda', 'EquipoController@deuda');
	Route::get('deuda/create', 'EquipoController@deudaCreate');
	Route::get('deuda/getEquipos/{id}', 'EquipoController@getEquipos');
	Route::post('deuda/deudaStore', 'EquipoController@deudaStore');
	Route::delete('deuda/{id}', 'EquipoController@deudaDelete');


	Route::resource('temporada', 'TemporadaController');

	Route::get('temporada/planilla/{id}', 'TemporadaController@planilla');
	Route::post('temporada/planilla/updateJugado', 'TemporadaController@updateJugado');
	Route::post('temporada/planilla/addLogPartido', 'TemporadaController@addLogPartido');
	Route::post('temporada/planilla/deleteLogPartido', 'TemporadaController@deleteLogPartido');
	Route::post('temporada/planilla/update_num', 'TemporadaController@update_num');
	Route::post('temporada/planilla/update_num_s', 'TemporadaController@update_num_s');
	Route::post('temporada/planilla/update_partido_ptos', 'TemporadaController@update_partido_ptos');
	Route::get('temporada/planilla/getJugadores/{id_partido}/{id_equipo}', 'TemporadaController@getJugadores');

	Route::post('temporada/planilla/addObservacion', 'TemporadaController@addObservacion');
	Route::post('temporada/planilla/add_informe_arbitro', 'TemporadaController@add_informe_arbitro');
	Route::post('temporada/planilla/add_informe_delegado', 'TemporadaController@add_informe_delegado');
	Route::post('temporada/planilla/add_informe_planillero', 'TemporadaController@add_informe_planillero');
	Route::post('temporada/planilla/add_informe_veedor', 'TemporadaController@add_informe_veedor');

	Route::get('graficos', 'ReporteController@graficos');
	Route::get('datosGraficos/{id}', 'ReporteController@datosGraficos');

});
